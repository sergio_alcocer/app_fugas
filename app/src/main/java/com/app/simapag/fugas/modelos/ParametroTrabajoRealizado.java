package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ParametroTrabajoRealizado {

    @SerializedName("ListaDiametroFuga")
    private  List<Cls_Combo> ListaDiametroFuga;
    @SerializedName("ListaMaterialesFuga")
    private  List<Cls_Combo> ListaMaterialesFuga;
    @SerializedName("ListaAreas")
    private  List<Cls_Combo> ListaAreas;
    @SerializedName("ListaEmpleadosDistritos")
    private  List<Cls_Combo> ListaEmpleadosDistritos;
    @SerializedName("ListaFallas")
    private  List<Cls_Combo>  ListaFallas;

    public List<Cls_Combo> getListaDiametroFuga() {
        return ListaDiametroFuga;
    }

    public void setListaDiametroFuga(List<Cls_Combo> listaDiametroFuga) {
        ListaDiametroFuga = listaDiametroFuga;
    }

    public List<Cls_Combo> getListaMaterialesFuga() {
        return ListaMaterialesFuga;
    }

    public void setListaMaterialesFuga(List<Cls_Combo> listaMaterialesFuga) {
        ListaMaterialesFuga = listaMaterialesFuga;
    }

    public List<Cls_Combo> getListaAreas() {
        return ListaAreas;
    }

    public void setListaAreas(List<Cls_Combo> listaAreas) {
        ListaAreas = listaAreas;
    }

    public List<Cls_Combo> getListaEmpleadosDistritos() {
        return ListaEmpleadosDistritos;
    }

    public void setListaEmpleadosDistritos(List<Cls_Combo> listaEmpleadosDistritos) {
        ListaEmpleadosDistritos = listaEmpleadosDistritos;
    }

    public List<Cls_Combo> getListaFallas() {
        return ListaFallas;
    }

    public void setListaFallas(List<Cls_Combo> listaFallas) {
        ListaFallas = listaFallas;
    }
}
