package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentoMenuOrdenes;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActividadMenuOrdenes extends SingleFragmentoActividad {

    @Override
    protected Fragment crearFragmento() {
        return FragmentoMenuOrdenes.NuevaInstancia();
    }

    public static Intent nuevaInstancia(Context context){
        Intent intent = new Intent(context,ActividadMenuOrdenes.class);
        return  intent;
    }


    @Override
    public void onBackPressed() {
        return;
    }
}
