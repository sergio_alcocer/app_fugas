package com.app.simapag.fugas.util;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class TimePickerWrapper implements View.OnClickListener, View.OnFocusChangeListener ,TimePickerDialog.OnTimeSetListener {

    private TextView mTextViewDisplay;
    private TimePickerDialog mDialog= null;
    private  Calendar mCalendarCurrent;
    private  int mHora;
    private  int mMinuto;
    private  String mFormat;
    private Activity mActivity;

    public TimePickerWrapper(TextView textViewDisplay, Activity activity) {
        mTextViewDisplay = textViewDisplay;
        mTextViewDisplay.setFocusable(true);
        mTextViewDisplay.setClickable(true);
        mTextViewDisplay.setOnClickListener(this);
        mTextViewDisplay.setOnFocusChangeListener(this);
        mActivity = activity;

        mCalendarCurrent = Calendar.getInstance();
        mHora = mCalendarCurrent.get(Calendar.HOUR_OF_DAY);
        mMinuto = mCalendarCurrent.get(Calendar.MINUTE);

        this.setTime(mHora,mMinuto);
    }

    @Override
    public void onClick(View view) {
        openTimePickerDialog();
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if(b){
            Util.hideKeyboard(mActivity);
            openTimePickerDialog();
        }
    }


    public  void setTime(int hour, int min){

        mCalendarCurrent.set(Calendar.HOUR_OF_DAY, hour);
        mCalendarCurrent.set(Calendar.MINUTE, min);
        mHora = hour;
        mMinuto =min;


        this.mTextViewDisplay.setText(getTime());

        if(this.mDialog != null){
            this.mDialog.updateTime(hour,min);
        }
    }

    public  String getTime(){
        String respuesta = "";

        // if (mHora == 0) {
        //     mHora += 12;
        //     mFormat = "AM";
        // } else if (mHora == 12) {
        //     mFormat = "PM";
        // } else if (mHora > 12) {
        //     mHora -= 12;
        //     mFormat = "PM";
        // } else {
        //     mFormat = "AM";
        // }
        String strHora = String.format("%02d", mHora);
        String strMinuto = String.format("%02d", mMinuto);
        respuesta =  strHora + " H " + ":" + " min " + strMinuto;

        //return  new StringBuilder().append(mHora).append(" : ").append(mMinuto)
        //        .append(" ").append(mFormat).toString();

        return  respuesta;
    }


    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        setTime(hourOfDay,minute);
    }

    public  void  openTimePickerDialog(){

        if(mDialog == null){
            //Calendar calendar = Calendar.getInstance();
            mDialog = new TimePickerDialog(mTextViewDisplay.getContext(),
                    this,
                    mCalendarCurrent.get(Calendar.HOUR_OF_DAY),
                    mCalendarCurrent.get(Calendar.MINUTE),
                    true);
        }
        mDialog.show();
    }

    public Calendar getCalendarCurrent() {
        return mCalendarCurrent;
    }

    public  void bloquearCajas(){
        mTextViewDisplay.setEnabled(false);
    }
}
