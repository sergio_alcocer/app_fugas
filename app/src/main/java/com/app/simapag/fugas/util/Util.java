package com.app.simapag.fugas.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.fragment.app.FragmentActivity;

import com.app.simapag.fugas.modelos.Cls_Combo;
import com.google.android.material.snackbar.Snackbar;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class Util {

    public  static String CARACTERES_PERMITIDOS = ".,_";

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public  static  void mostarAlerta2(FragmentActivity fragmentActivity, String mensaje){
        View view = fragmentActivity.findViewById(android.R.id.content);
        Snackbar.make(view,mensaje ,Snackbar.LENGTH_LONG).show();
    }

    public  static void  mostrarAlerta(View view, String mensaje){
        Snackbar.make(view,mensaje ,Snackbar.LENGTH_LONG).show();
    }


    public  static  List<Cls_Combo> Obtener_Item_SI_NO(){
        List<Cls_Combo> Lista_Combo = new ArrayList<>();

        Lista_Combo.add(obtenerElementoInicial());

        Cls_Combo cls_combo = new Cls_Combo();
        cls_combo.setID("SI");
        cls_combo.setName("SI");
        Lista_Combo.add(cls_combo);

         cls_combo = new Cls_Combo();
        cls_combo.setID("NO");
        cls_combo.setName("NO");
        Lista_Combo.add(cls_combo);

        return  Lista_Combo;
    }

    public  static  List<Cls_Combo> Obtener_Item_Aprobacion_Orden_Investigacion(){

        List<Cls_Combo> Lista_Combo = new ArrayList<>();

        Lista_Combo.add(obtenerElementoInicial());

        Cls_Combo cls_combo = new Cls_Combo();
        cls_combo.setID("SIN DEFINIR");
        cls_combo.setName("SIN DEFINIR");
        Lista_Combo.add(cls_combo);

        cls_combo = new Cls_Combo();
        cls_combo.setID("NO");
        cls_combo.setName("NO");
        Lista_Combo.add(cls_combo);

        cls_combo = new Cls_Combo();
        cls_combo.setID("SI");
        cls_combo.setName("SI");
        Lista_Combo.add(cls_combo);

        return  Lista_Combo;
    }

    public  static  List<Cls_Combo> Obtener_Estatus_Orden_Investigacion(){
        List<Cls_Combo> Lista_Combo = new ArrayList<>();

        Lista_Combo.add(obtenerElementoInicial());

        Cls_Combo cls_combo = new Cls_Combo();
        cls_combo.setID("GENERADO");
        cls_combo.setName("GENERADO");
        Lista_Combo.add(cls_combo);

        cls_combo = new Cls_Combo();
        cls_combo.setID("CONTRATO");
        cls_combo.setName("CONTRATO");
        Lista_Combo.add(cls_combo);

        return  Lista_Combo;

    }

    public static Cls_Combo obtenerElementoInicial(){
        Cls_Combo cls_combo = new Cls_Combo();
        cls_combo.setID("-1");
        cls_combo.setName("Seleccione");
        return  cls_combo;
    }

    public  static  int obtenerIndex(String id, List<Cls_Combo> listaElementos){
      int repuesta = -1;
      int contador;

      for(contador = 0; contador < listaElementos.size(); contador ++){

          if(listaElementos.get(contador).getID().equals(id)){
              repuesta = contador;
              break;
          }
      }

      return  repuesta;
    }

    public  static  int obtenerIndexNombre(String name, List<Cls_Combo> listaElementos){
        int repuesta = -1;
        int contador;

        for(contador = 0; contador < listaElementos.size(); contador ++){

            if(listaElementos.get(contador).getName().equals(name)){
                repuesta = contador;
                break;
            }
        }

        return  repuesta;
    }

    public  static  int obtenerMinuto(String strCadena){

        int minuto =0;
        int longitudCadena = strCadena.length();
        String strMinuto;

        strMinuto =strCadena.substring(longitudCadena -2,longitudCadena);
        minuto = Integer.valueOf(strMinuto);
        return  minuto;

    }

    public  static  int obtenerHora(String strCadena){
        int hora =0;
        String strHora;

        strHora =strCadena.substring(0,2);
        hora = Integer.valueOf(strHora);
        return  hora;
    }

    public  static Date configurarHorasMinutosAFecha(Date date, int horas, int minutos){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, horas);
        calendar.set(Calendar.MINUTE, minutos);
        calendar.set(Calendar.SECOND,0);
        return calendar.getTime();
    }

    public static double round(final double value, final int frac) {
        return Math.round(Math.pow(10.0, frac) * value) / Math.pow(10.0, frac);
    }

    public  static  int convertirEntero(String strNumero){
        int respuesta = 0;

        if(strNumero == null || strNumero.isEmpty()){
            respuesta = 0;
        }else {

            try {
                respuesta = Integer.parseInt(strNumero);
            }catch (Exception ex){
                respuesta = 0;
            }
        }

        return  respuesta;
    }

    public  static double convertirDouble(String strNumero){
        double respuesta = 0.0;

        if (strNumero == null || strNumero.isEmpty()){
            respuesta = 0.0;
        }  else {
            try {
                respuesta = Double.parseDouble(strNumero);
            }catch (Exception ex){
                respuesta = 0.0;
            }
        }

        return respuesta;
    }

    public  static  boolean tieneCarateresPermitidos(String cadena){

        String cadenaPattern = "^[a-zñ A-ZÑ0-9/.,_áéíóú]+$";
        Pattern CADENA_VALIDAR = Pattern.compile(cadenaPattern);

        if (CADENA_VALIDAR.matcher(cadena).matches() == false) {
            return  false;
        }

        return  true;
    }

    public static File obtenerArchivoFoto(Context context, String nombre){
        File file = context.getFilesDir();
        if(file == null){
            return  null;
        }

        return  new File(file, nombre);
    }

    public static  void  eliminarFoto(Context context, String nombreFoto){
        File file;
        file = obtenerArchivoFoto(context,nombreFoto);

        if(file != null && file.exists()){
            file.delete();
        }
    }

    // estos métodos son  de los primeros que use muy efectectivos
    public static Bitmap getScaledBitmap(Bitmap b, int reqWidth, int reqHeight)
    {
        int bWidth = b.getWidth();
        int bHeight = b.getHeight();

        int nWidth = bWidth;
        int nHeight = bHeight;

        if(nWidth > reqWidth)
        {
            int ratio = bWidth / reqWidth;
            if(ratio > 0)
            {
                nWidth = reqWidth;
                nHeight = bHeight / ratio;
            }
        }

        if(nHeight > reqHeight)
        {
            int ratio = bHeight / reqHeight;
            if(ratio > 0)
            {
                nHeight = reqHeight;
                nWidth = bWidth / ratio;
            }
        }

        return Bitmap.createScaledBitmap(b, nWidth, nHeight, true);
    }

    // estos métodos son  de los primeros que use muy efectectivos
    public static  Bitmap escalarBitmap(String uri, Integer factor) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = factor;
        //bmOptions.inPurgeable = true;
        return rotarBitmap(uri, BitmapFactory.decodeFile(uri, bmOptions));
    }

    /**
     * Hace la Rotación de un Bitmap
     *
     * @param Url
     * @param bitmap
     * @return Bitmap
     */
    // estos métodos son  de los primeros que use muy efectectivos
    private static Bitmap rotarBitmap(String Url, Bitmap bitmap) {
        try {
            ExifInterface exifInterface = new ExifInterface(Url);
            int orientacion = exifInterface.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 1);
            Matrix matrix = new Matrix();

            if (orientacion == 6) {
                matrix.postRotate(90);
            } else if (orientacion == 3) {
                matrix.postRotate(180);
            } else if (orientacion == 8) {
                matrix.postRotate(270);
            }

            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true); // rotating bitmap
        } catch (Exception e) {
            // TODO:
        }
        return bitmap;
    }


    // estos métodos son del google los baje de sus páginas
    /**
     * calcular  El factor de calidad mantien el width
     * @param options debe estar en true  inJustDecodeBounds
     * @param reqWidth
     * @param reqHeight
     * @return
     */

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /**
     *  escala la imagen y cacula su factor de acuerdo a los dimenciones de google esta madrola
     * @param path
     * @param destWidth
     * @param destHeight
     * @return
     */
    public static Bitmap getScaledBitmap(String path, int destWidth, int destHeight){
        BitmapFactory.Options options= new BitmapFactory.Options();
        options.inJustDecodeBounds=true;
        BitmapFactory.decodeFile(path,options);

        int inSampleSize= calculateInSampleSize(options, destWidth, destHeight);

        options= new BitmapFactory.Options();
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        options.inSampleSize=inSampleSize;

        return BitmapFactory.decodeFile(path,options);
    }

    /**
     * Rotate an image if required.
     *
     * @param img           The image bitmap
     * @param selectedImage Image URI
     * @return The resulted Bitmap after manipulation
     */
    public static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public static String getBase64String(Bitmap bitmap)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] imageBytes = baos.toByteArray();

        String base64String = Base64.encodeToString(imageBytes, Base64.NO_WRAP);

        return base64String;
    }
}
