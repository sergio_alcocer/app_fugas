package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class OrdenesMaterialViewModel {

    @SerializedName("Ordenes_Trabajo_Detalles_Materiales_ID")
    private  String  Ordenes_Trabajo_Detalles_Materiales_ID;
    @SerializedName("No_Orden_trabajo")
    private  String No_Orden_trabajo;
    @SerializedName("Producto_ID")
    private  String Producto_ID;
    @SerializedName("Cantidad")
    private  double Cantidad;
    @SerializedName("CostoPorUnidad")
    private  double CostoPorUnidad;
    @SerializedName("CostoTotal")
    private  double CostoTotal;
    @SerializedName("Nombre_Usuario")
    private  String Nombre_Usuario;
    @SerializedName("Nombre_Material")
    private  String Nombre_Material;
    @SerializedName("Unidad")
    private  String Unidad;

    public String getOrdenes_Trabajo_Detalles_Materiales_ID() {
        return Ordenes_Trabajo_Detalles_Materiales_ID;
    }

    public void setOrdenes_Trabajo_Detalles_Materiales_ID(String ordenes_Trabajo_Detalles_Materiales_ID) {
        Ordenes_Trabajo_Detalles_Materiales_ID = ordenes_Trabajo_Detalles_Materiales_ID;
    }

    public String getNo_Orden_trabajo() {
        return No_Orden_trabajo;
    }

    public void setNo_Orden_trabajo(String no_Orden_trabajo) {
        No_Orden_trabajo = no_Orden_trabajo;
    }

    public String getProducto_ID() {
        return Producto_ID;
    }

    public void setProducto_ID(String producto_ID) {
        Producto_ID = producto_ID;
    }

    public double getCantidad() {
        return Cantidad;
    }

    public void setCantidad(double cantidad) {
        Cantidad = cantidad;
    }

    public double getCostoPorUnidad() {
        return CostoPorUnidad;
    }

    public void setCostoPorUnidad(double costoPorUnidad) {
        CostoPorUnidad = costoPorUnidad;
    }

    public double getCostoTotal() {
        return CostoTotal;
    }

    public void setCostoTotal(double costoTotal) {
        CostoTotal = costoTotal;
    }

    public String getNombre_Usuario() {
        return Nombre_Usuario;
    }

    public void setNombre_Usuario(String nombre_Usuario) {
        Nombre_Usuario = nombre_Usuario;
    }

    public String getNombre_Material() {
        return Nombre_Material;
    }

    public void setNombre_Material(String nombre_Material) {
        Nombre_Material = nombre_Material;
    }

    public String getUnidad() {
        return Unidad;
    }

    public void setUnidad(String unidad) {
        Unidad = unidad;
    }
}
