package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class OrderAsingadaModel {

    @SerializedName("No_Orden_Trabajo")
    private  String No_Orden_Trabajo;

    @SerializedName("Empleado_ID")
    private  String Empleado_ID;

    @SerializedName("Nombre_Empleado")
    private  String Nombre_Empleado;

    @SerializedName("Usuario_Creo")
    private String Usuario_Creo;

    public String getNo_Orden_Trabajo() {
        return No_Orden_Trabajo;
    }

    public void setNo_Orden_Trabajo(String no_Orden_Trabajo) {
        No_Orden_Trabajo = no_Orden_Trabajo;
    }

    public String getEmpleado_ID() {
        return Empleado_ID;
    }

    public void setEmpleado_ID(String empleado_ID) {
        Empleado_ID = empleado_ID;
    }

    public String getNombre_Empleado() {
        return Nombre_Empleado;
    }

    public void setNombre_Empleado(String nombre_Empleado) {
        Nombre_Empleado = nombre_Empleado;
    }

    public String getUsuario_Creo() {
        return Usuario_Creo;
    }

    public void setUsuario_Creo(String usuario_Creo) {
        Usuario_Creo = usuario_Creo;
    }
}
