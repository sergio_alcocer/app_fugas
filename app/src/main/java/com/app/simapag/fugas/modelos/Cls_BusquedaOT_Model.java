package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class Cls_BusquedaOT_Model {

    @SerializedName("Query")
    private  String Query ;
    @SerializedName("Distrito_ID")
    private  String Distrito_ID;
    @SerializedName("Brigada_ID")
    private  String Brigada_ID;
    @SerializedName("Pagina")
    private  int Pagina;
    @SerializedName("Empleado_ID")
    private  String Empleado_ID;
    @SerializedName("Falla_ID")
    private  String  Falla_ID;
    @SerializedName("Empleado_Asignado_ID")
    private  String Empleado_Asignado_ID;
    @SerializedName("Estatus")
    private  String Estatus;
    @SerializedName("Fecha_Inicio")
    private  String Fecha_Inicio;
    @SerializedName("Fecha_Fin")
    private  String Fecha_Fin;

    public String getQuery() {
        return Query;
    }

    public void setQuery(String query) {
        Query = query;
    }

    public String getDistrito_ID() {
        return Distrito_ID;
    }

    public void setDistrito_ID(String distrito_ID) {
        Distrito_ID = distrito_ID;
    }

    public String getBrigada_ID() {
        return Brigada_ID;
    }

    public void setBrigada_ID(String brigada_ID) {
        Brigada_ID = brigada_ID;
    }

    public int getPagina() {
        return Pagina;
    }

    public void setPagina(int pagina) {
        Pagina = pagina;
    }

    public String getEmpleado_ID() {
        return Empleado_ID;
    }

    public void setEmpleado_ID(String empleado_ID) {
        Empleado_ID = empleado_ID;
    }

    public String getFalla_ID() {
        return Falla_ID;
    }

    public void setFalla_ID(String falla_ID) {
        Falla_ID = falla_ID;
    }

    public String getEmpleado_Asignado_ID() {
        return Empleado_Asignado_ID;
    }

    public void setEmpleado_Asignado_ID(String empleado_Asignado_ID) {
        Empleado_Asignado_ID = empleado_Asignado_ID;
    }

    public String getEstatus() {
        return Estatus;
    }

    public void setEstatus(String estatus) {
        Estatus = estatus;
    }

    public String getFecha_Inicio() {
        return Fecha_Inicio;
    }

    public void setFecha_Inicio(String fecha_Inicio) {
        Fecha_Inicio = fecha_Inicio;
    }

    public String getFecha_Fin() {
        return Fecha_Fin;
    }

    public void setFecha_Fin(String fecha_Fin) {
        Fecha_Fin = fecha_Fin;
    }
}
