package com.app.simapag.fugas.parciable;

import android.os.Parcel;
import android.os.Parcelable;

public class VisitaInvestigacionParciable implements Parcelable {

    private int No_Visita;
    private  String No_Solicitud;
    private  String Fecha_Visita;
    private  String Hora_Visita;
    private  String Observaciones;
    private  String Usuario;


    public VisitaInvestigacionParciable(int no_Visita, String no_Solicitud, String fecha_Visita, String hora_Visita, String observaciones, String usuario) {
        No_Visita = no_Visita;
        No_Solicitud = no_Solicitud;
        Fecha_Visita = fecha_Visita;
        Hora_Visita = hora_Visita;
        Observaciones = observaciones;
        Usuario = usuario;
    }

    public int getNo_Visita() {
        return No_Visita;
    }

    public void setNo_Visita(int no_Visita) {
        No_Visita = no_Visita;
    }

    public String getNo_Solicitud() {
        return No_Solicitud;
    }

    public void setNo_Solicitud(String no_Solicitud) {
        No_Solicitud = no_Solicitud;
    }

    public String getFecha_Visita() {
        return Fecha_Visita;
    }

    public void setFecha_Visita(String fecha_Visita) {
        Fecha_Visita = fecha_Visita;
    }

    public String getHora_Visita() {
        return Hora_Visita;
    }

    public void setHora_Visita(String hora_Visita) {
        Hora_Visita = hora_Visita;
    }

    public String getObservaciones() {
        return Observaciones;
    }

    public void setObservaciones(String observaciones) {
        Observaciones = observaciones;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        Usuario = usuario;
    }

    protected VisitaInvestigacionParciable(Parcel in) {
        No_Visita = in.readInt();
        No_Solicitud = in.readString();
        Fecha_Visita = in.readString();
        Hora_Visita = in.readString();
        Observaciones = in.readString();
        Usuario = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

      dest.writeInt(No_Visita);
      dest.writeString(No_Solicitud);
      dest.writeString(Fecha_Visita);
      dest.writeString(Hora_Visita);
      dest.writeString(Observaciones);
      dest.writeString(Usuario);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VisitaInvestigacionParciable> CREATOR = new Creator<VisitaInvestigacionParciable>() {
        @Override
        public VisitaInvestigacionParciable createFromParcel(Parcel in) {
            return new VisitaInvestigacionParciable(in);
        }

        @Override
        public VisitaInvestigacionParciable[] newArray(int size) {
            return new VisitaInvestigacionParciable[size];
        }
    };
}
