package com.app.simapag.fugas.fragmentos;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadListadoVisitasInvestigacion;
import com.app.simapag.fugas.actividades.ActividadOrdenesInvestigacion;
import com.app.simapag.fugas.actividades.ActividadVisitaInvestigacion;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.modelos.OrdenesInvestigacionViewModel;
import com.app.simapag.fugas.modelos.VisitaInvestigacionModel;
import com.app.simapag.fugas.parciable.VisitaInvestigacionParciable;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.singlenton.RepositorioVisita;
import com.app.simapag.fugas.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FragmentoListadoVisitasInvestigacion extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;

    @BindView(R.id.text_view_titulo_pantalla)
    TextView textViewTituloPantalla;
     OrdenesInvestigacionViewModel ordenesInvestigacionViewModel;
    List<VisitaInvestigacionModel> Lista_Visita_Investigacion = new ArrayList<>();

    @BindView(R.id.recyclerview_visita_orden_investigacion)
    RecyclerView recyclerVisitaOrdenInvestigacion;
    LinearLayoutManager linearLayoutManager;

    DelayedProgressDialog delayedProgressDialog;
    private Unbinder unbinder;

    public static  FragmentoListadoVisitasInvestigacion nuevaInstancia(){
        return  new FragmentoListadoVisitasInvestigacion();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);
        setHasOptionsMenu(true);
        ordenesInvestigacionViewModel = RepositorioVisita.get().getOrdenesInvestigacionViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_listado_visitas_investigacion, container, false);
        unbinder = ButterKnife.bind(this, view);

        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerVisitaOrdenInvestigacion.setLayoutManager(linearLayoutManager);

        textViewTitulo.setText("RPU: " + ordenesInvestigacionViewModel.getRpu());
        textViewTituloPantalla.setText("Visitas Hechas");

        return  view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        obtenerVisitasOrdenesInvestigacion(ordenesInvestigacionViewModel.getNo_solicitud());
    }


    // métodos

     public void configurarAdpater(){

        if(isAdded()){
            recyclerVisitaOrdenInvestigacion.setAdapter(new ListadoVisitaInvestigacionAdapter(Lista_Visita_Investigacion));
        }
     }

    // peticiones
    public void obtenerVisitasOrdenesInvestigacion(String No_Solicitud){

        delayedProgressDialog.show(getParentFragmentManager(), "paginado");
        FactoryPeticiones.getFactoryPeticiones()
                .ObtenerVisitasOrdenInvestigacion(No_Solicitud)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<VisitaInvestigacionModel>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<VisitaInvestigacionModel> visitaInvestigacionModels) {
                          Lista_Visita_Investigacion = visitaInvestigacionModels;
                          configurarAdpater();
                          if(visitaInvestigacionModels.size() == 0){
                              Util.mostarAlerta2(getActivity(), "No hay visitas Agregadas");
                          }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });
    }

    // sobre escribir métodos
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_mas, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_item_agregar:

                 Intent intent1 = ActividadVisitaInvestigacion.nuevaInstancia(getContext(),
                         new VisitaInvestigacionParciable(0,ordenesInvestigacionViewModel.getNo_solicitud(),"","","",""));
                 startActivity(intent1);
                return  true;
            case  android.R.id.home:
                Intent intent = ActividadOrdenesInvestigacion.nuevaInstancia(getContext());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return  true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    // clases anidadas

    public class  ListadoVisitaInvestigacionAdapter extends RecyclerView.Adapter<ListadoVisitaInvestigacionHolder>{

        List<VisitaInvestigacionModel> Lista;

        public ListadoVisitaInvestigacionAdapter(List<VisitaInvestigacionModel> lista) {
            Lista = lista;
        }

        @NonNull
        @Override
        public ListadoVisitaInvestigacionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_visita_investigacion, parent, false);
            return new ListadoVisitaInvestigacionHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ListadoVisitaInvestigacionHolder holder, int position) {
            VisitaInvestigacionModel model = Lista.get(position);
            holder.enlazarDatos(model);
        }

        @Override
        public int getItemCount() {
            return Lista.size();
        }
    }

    public  class  ListadoVisitaInvestigacionHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{

        VisitaInvestigacionModel visitaInvestigacionModel;

        @BindView(R.id.text_view_vista_investigacion_fecha_visita)
        TextView textViewFechaVisita;

        @BindView(R.id.text_view_vista_investigacion_hora_visita)
        TextView textViewHoraVisita;

        @BindView(R.id.text_view_vista_investigacion_observacion)
        TextView textViewObservacion;

        @BindView(R.id.text_view_vista_investigacion_usuario)
        TextView textViewUsuario;


        public ListadoVisitaInvestigacionHolder(@NonNull View itemView){
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);

        }

        public void enlazarDatos(VisitaInvestigacionModel model){
             visitaInvestigacionModel = model;
            textViewFechaVisita.setText(visitaInvestigacionModel.getFecha_Visita());
            textViewHoraVisita.setText(visitaInvestigacionModel.getHora_Visita());
            textViewObservacion.setText(visitaInvestigacionModel.getObservaciones());
            textViewUsuario.setText(visitaInvestigacionModel.getUsuario());
        }

        @Override
        public void onClick(View view) {


        }
    }

}
