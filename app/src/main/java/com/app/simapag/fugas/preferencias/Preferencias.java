package com.app.simapag.fugas.preferencias;

import android.content.Context;

import androidx.preference.PreferenceManager;

public class Preferencias {
    public  static  final String PREF_EMPLEADO_ID= "empleado_id";
    public  static  final String PREF_NOMBRE_COMPLETO= "nombre_completo";
    public  static  final  String PREF_NOMBRE_ROL = "nombre_rol";


    public  static  void setNombreRol(Context context, String nombreRol){

        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(PREF_NOMBRE_ROL, nombreRol)
                .apply();
    }

    public  static  String   getNombreRol(Context context){

       return   PreferenceManager.getDefaultSharedPreferences(context)
             .getString(PREF_NOMBRE_ROL, "");
    }

    public  static  void  setEmpleadoID(Context context, String empleadoID){
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(PREF_EMPLEADO_ID, empleadoID)
                .apply();

    }

    public  static  String getEmpleadoID(Context context){
        return  PreferenceManager.getDefaultSharedPreferences(context)
                .getString(PREF_EMPLEADO_ID, "");
    }

    public  static  void  setNombreUsuario(Context context, String nombreUsuario){
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(PREF_NOMBRE_COMPLETO,nombreUsuario)
                .apply();
    }

    public  static  String getNombreUsuario(Context context){
        return  PreferenceManager.getDefaultSharedPreferences(context)
                .getString(PREF_NOMBRE_COMPLETO,"");
    }

}
