package com.app.simapag.fugas.util;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.app.simapag.fugas.R;

public abstract class SingleFragmentoActividad extends AppCompatActivity {

    protected  abstract Fragment crearFragmento();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragmento_generico);

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.fragmento_contender);

        if(fragment == null) {
            fragment = crearFragmento();
            fragmentManager.beginTransaction()
                    .add(R.id.fragmento_contender, fragment)
                    .commit();
        }

    }


}
