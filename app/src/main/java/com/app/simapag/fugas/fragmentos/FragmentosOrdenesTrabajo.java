package com.app.simapag.fugas.fragmentos;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.PreferenceManager;

import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadAgregarVisitas;
import com.app.simapag.fugas.actividades.ActividadListadoVisitas;
import com.app.simapag.fugas.actividades.ActividadMenu;
import com.app.simapag.fugas.actividades.ActividadMenuOrdenes;
import com.app.simapag.fugas.adapter.AutoCompleteCombos;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.dialogos.DialogoBuscarCuenta;
import com.app.simapag.fugas.modelos.Cls_Combo;
import com.app.simapag.fugas.modelos.MensajeModel;
import com.app.simapag.fugas.modelos.OrdenCreateModel;
import com.app.simapag.fugas.modelos.ParametroCrearOT;
import com.app.simapag.fugas.modelos.PrediosViewModel;
import com.app.simapag.fugas.preferencias.Preferencias;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.util.Util;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.internal.bind.JsonTreeReader;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FragmentosOrdenesTrabajo extends Fragment implements DialogoBuscarCuenta.BusquedaCuentaListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;

    @BindView(R.id.autoComplete_actividad)
    AutoCompleteTextView autoCompleteTextActividad;

    @BindView(R.id.boton_borrar_autocompletado_actividad)
    Button buttonBorrarAutompleadoActividad;

    List<Cls_Combo> ListaActividad = new ArrayList<>();
    Cls_Combo itemSelectedActividad;


    @BindView(R.id.autoComplete_Colonia)
    AutoCompleteTextView autoCompleteTextColonias;

    @BindView(R.id.boton_borrar_autocompletado_colonia)
    Button buttonBorrarAutompleadoColonias;

    List<Cls_Combo> ListaColonias = new ArrayList<>();
    Cls_Combo itemSelectedColonias;

    @BindView(R.id.autoComplete_Calle)
    AutoCompleteTextView autoCompleteTextCalles;

    @BindView(R.id.boton_borrar_autocompletado_calle)
    Button buttonBorrarAutompleadoCalles;

    List<Cls_Combo> ListaCalles = new ArrayList<>();
    Cls_Combo itemSelectedCalle;

    @BindView(R.id.autoComplete_Calle_referencia)
    AutoCompleteTextView autoCompleteTextCallesReferencia;

    @BindView(R.id.boton_borrar_autocompletado_calle_referencia)
    Button buttonBorrarAutompleadoCallesReferencia;

    Cls_Combo itemSelectedCalleReferencia;

    @BindView(R.id.autoComplete_Calle_referencia2)
    AutoCompleteTextView autoCompleteTextCallesReferencia2;

    @BindView(R.id.boton_borrar_autocompletado_calle_referencia2)
    Button buttonBorrarAutompleadoCallesReferencia2;

    Cls_Combo itemSelectedCalleReferencia2;

    @BindView(R.id.radio_ot_cuenta_cero)
    RadioButton radioButtonCuentaCero;

    @BindView(R.id.radio_ot_cuenta_simapag)
    RadioButton radioButtonCuentaSimapag;

    @BindView(R.id.button_ot_buscar)
    Button buttonBuscarRPU;

    @BindView(R.id.text_input_layout_ot_rpu)
    TextInputLayout textInputLayoutRPU;

    @BindView(R.id.text_input_edit_ot_rpu)
    TextInputEditText textInputEditTextRPU;

    DelayedProgressDialog delayedProgressDialog;
    private Unbinder unbinder;

    @BindView(R.id.text_input_layout_domicilio_referencia)
    TextInputLayout textInputLayoutDomiciloReferencia;
    @BindView(R.id.text_input_edit_domicilio_referencia)
    TextInputEditText textInputEditTextDomicilioReferencia;

    @BindView(R.id.text_input_layout_referencia)
    TextInputLayout textInputLayoutReferencia;
    @BindView(R.id.text_input_edit_referencia)
    TextInputEditText textInputEditTextReferencia;

    @BindView(R.id.text_input_layout_reporto)
    TextInputLayout textInputLayoutReporte;
    @BindView(R.id.text_input_edit_reporto)
    TextInputEditText textInputEditTextReporte;

    @BindView(R.id.text_input_layout_comentario)
    TextInputLayout textInputLayoutObservaciones;
    @BindView(R.id.text_input_edit_comentario)
    TextInputEditText textInputEditTextObservaciones;

    @BindView(R.id.text_input_layout_telefono)
    TextInputLayout textInputLayoutTelefono;
    @BindView(R.id.text_input_edit_telefono)
    TextInputEditText textInputEditTextTelefono;

    PrediosViewModel ObjPrediosViewModel=null;

    OrdenCreateModel ObjOrdenCreatOT = new OrdenCreateModel();

    public static FragmentosOrdenesTrabajo nuevaInstancia(){
        return  new FragmentosOrdenesTrabajo();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_orden_trabajo, container, false);
        unbinder = ButterKnife.bind(this, view);

        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");

        textViewTitulo.setText("Crear Ordenes");

        view.findViewById(R.id.linear_layout_principal).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                View focusedView = getActivity().getCurrentFocus();
                /*
                 * If no view is focused, an NPE will be thrown
                 *
                 * Maxim Dmitriev
                 */
                if (focusedView != null) {
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }


                return true;
            }
        });


        // auto completados ********************************************

        buttonBorrarAutompleadoCallesReferencia2.setVisibility(View.GONE);
        autoCompleteTextCallesReferencia2.setImeOptions(EditorInfo.IME_ACTION_DONE);
        autoCompleteTextCallesReferencia2.setRawInputType(InputType.TYPE_CLASS_TEXT);

        autoCompleteTextCallesReferencia2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                if(s.length() != 0) {
                    buttonBorrarAutompleadoCallesReferencia2.setVisibility(View.VISIBLE);
                } else {
                    buttonBorrarAutompleadoCallesReferencia2.setVisibility(View.GONE);
                    itemSelectedCalleReferencia2 = null;
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        autoCompleteTextCallesReferencia.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                itemSelectedCalleReferencia2 = (Cls_Combo) adapterView.getItemAtPosition(i);
                buttonBorrarAutompleadoCallesReferencia2.setVisibility(View.VISIBLE);
            }
        });


        buttonBorrarAutompleadoCallesReferencia.setVisibility(View.GONE);
        autoCompleteTextCallesReferencia.setImeOptions(EditorInfo.IME_ACTION_DONE);
        autoCompleteTextCallesReferencia.setRawInputType(InputType.TYPE_CLASS_TEXT);

        autoCompleteTextCallesReferencia.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                if(s.length() != 0) {
                    buttonBorrarAutompleadoCallesReferencia.setVisibility(View.VISIBLE);
                } else {
                    buttonBorrarAutompleadoCallesReferencia.setVisibility(View.GONE);
                    itemSelectedCalleReferencia = null;
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        autoCompleteTextCallesReferencia.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                itemSelectedCalleReferencia = (Cls_Combo) adapterView.getItemAtPosition(i);
                buttonBorrarAutompleadoCallesReferencia.setVisibility(View.VISIBLE);
            }
        });


        buttonBorrarAutompleadoCalles.setVisibility(View.GONE);
        autoCompleteTextCalles.setImeOptions(EditorInfo.IME_ACTION_DONE);
        autoCompleteTextCalles.setRawInputType(InputType.TYPE_CLASS_TEXT);

        autoCompleteTextCalles.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                if(s.length() != 0) {
                    buttonBorrarAutompleadoCalles.setVisibility(View.VISIBLE);
                } else {
                    buttonBorrarAutompleadoCalles.setVisibility(View.GONE);
                    itemSelectedCalle = null;
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        autoCompleteTextCalles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                itemSelectedCalle = (Cls_Combo) adapterView.getItemAtPosition(i);
                buttonBorrarAutompleadoCalles.setVisibility(View.VISIBLE);
            }
        });

        buttonBorrarAutompleadoColonias.setVisibility(View.GONE);
        autoCompleteTextColonias.setImeOptions(EditorInfo.IME_ACTION_DONE);
        autoCompleteTextColonias.setRawInputType(InputType.TYPE_CLASS_TEXT);

        autoCompleteTextColonias.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                if(s.length() != 0) {
                    buttonBorrarAutompleadoColonias.setVisibility(View.VISIBLE);
                } else {
                    buttonBorrarAutompleadoColonias.setVisibility(View.GONE);
                    itemSelectedColonias = null;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        autoCompleteTextColonias.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                itemSelectedColonias = (Cls_Combo) adapterView.getItemAtPosition(i);
                buttonBorrarAutompleadoColonias.setVisibility(View.VISIBLE);
                obtenerCallerColonias(itemSelectedColonias.getID());
            }
        });

        buttonBorrarAutompleadoActividad.setVisibility(View.GONE);
        autoCompleteTextActividad.setImeOptions(EditorInfo.IME_ACTION_DONE);
        autoCompleteTextActividad.setRawInputType(InputType.TYPE_CLASS_TEXT);

        autoCompleteTextActividad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                if(s.length() != 0) {
                    buttonBorrarAutompleadoActividad.setVisibility(View.VISIBLE);
                } else {
                    buttonBorrarAutompleadoActividad.setVisibility(View.GONE);
                    itemSelectedActividad = null;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        autoCompleteTextActividad.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                itemSelectedActividad = (Cls_Combo) adapterView.getItemAtPosition(i);
                buttonBorrarAutompleadoActividad.setVisibility(View.VISIBLE);
            }
        });

       textInputEditTextRPU.setEnabled(false);
       bloquearControlesUbicacion();

        textInputEditTextObservaciones.setImeOptions(EditorInfo.IME_ACTION_DONE);
        textInputEditTextObservaciones.setRawInputType(InputType.TYPE_CLASS_TEXT);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        obtenerParametrosCrearOT();
    }


    //métodos **********************************************************


    public void desbloquearControlesUbicacion(){
        autoCompleteTextColonias.setEnabled(true);
        autoCompleteTextCalles.setEnabled(true);
        autoCompleteTextCallesReferencia2.setEnabled(true);
        autoCompleteTextCallesReferencia.setEnabled(true);

        buttonBorrarAutompleadoColonias.setEnabled(true);
        buttonBorrarAutompleadoCalles.setEnabled(true);
        buttonBorrarAutompleadoCallesReferencia.setEnabled(true);
        buttonBorrarAutompleadoCallesReferencia2.setEnabled(true);
    }

    public  void  bloquearControlesUbicacion(){

        autoCompleteTextColonias.setEnabled(false);
        autoCompleteTextCalles.setEnabled(false);
        autoCompleteTextCallesReferencia2.setEnabled(false);
        autoCompleteTextCallesReferencia.setEnabled(false);

        buttonBorrarAutompleadoColonias.setEnabled(false);
        buttonBorrarAutompleadoCalles.setEnabled(false);
        buttonBorrarAutompleadoCallesReferencia.setEnabled(false);
        buttonBorrarAutompleadoCallesReferencia2.setEnabled(false);

    }

    public  void  configurarAdapterAutoCompleatados(){

        if(isAdded()){
             autoCompleteTextActividad.setAdapter(new AutoCompleteCombos(getContext(),ListaActividad));
             autoCompleteTextColonias.setAdapter(new AutoCompleteCombos(getContext(), ListaColonias));
        }
    }

    public  void  configurarAdapterCalles(){

        autoCompleteTextCalles.setAdapter(new AutoCompleteCombos(getContext(),ListaCalles));
        autoCompleteTextCallesReferencia.setAdapter(new AutoCompleteCombos(getContext(),ListaCalles));
        autoCompleteTextCallesReferencia2.setAdapter(new AutoCompleteCombos(getContext(), ListaCalles));
    }

    public void limpiarAutocompletadoColonias(){
        autoCompleteTextColonias.setText("");
        buttonBorrarAutompleadoColonias.setVisibility(View.GONE);
        itemSelectedColonias = null;

    }

    public  void  limpiarAutocompletadoCalle(){
        autoCompleteTextCalles.setText("");
        buttonBorrarAutompleadoCalles.setVisibility(View.GONE);
        itemSelectedCalle = null;
    }

    public  void  limpiarAutocompletadoCalleReferencia(){

        autoCompleteTextCallesReferencia.setText("");
        buttonBorrarAutompleadoCallesReferencia.setVisibility(View.GONE);
        itemSelectedCalleReferencia = null;
    }

    public  void  limpiarAutompleadoCalleReferencia2(){
        autoCompleteTextCallesReferencia2.setText("");
        buttonBorrarAutompleadoCallesReferencia2.setVisibility(View.GONE);
        itemSelectedCalleReferencia2 = null;
    }

    public  boolean validarInterfaz(){

        ObjOrdenCreatOT.setEmpleado_ID(Preferencias.getEmpleadoID(getContext()));
        ObjOrdenCreatOT.setNombre_Empleado(Preferencias.getNombreUsuario(getContext()));
        ObjOrdenCreatOT.setDomicilio_Referencia(textInputEditTextDomicilioReferencia.getText().toString().trim());
        ObjOrdenCreatOT.setReferencia(textInputEditTextReferencia.getText().toString().trim());
        ObjOrdenCreatOT.setReporto(textInputEditTextReporte.getText().toString().trim());
        ObjOrdenCreatOT.setObservaciones(textInputEditTextObservaciones.getText().toString().trim());
        ObjOrdenCreatOT.setTelefono(textInputEditTextTelefono.getText().toString().trim());

        if(radioButtonCuentaSimapag.isChecked()){

            if(ObjPrediosViewModel == null){
                 Util.mostarAlerta2(getActivity(), "Seleccione una cuenta");
                 return  false;
            }else {
                ObjOrdenCreatOT.setNo_Medidor(ObjPrediosViewModel.getNo_Medidor().trim());
                ObjOrdenCreatOT.setPredio_ID(ObjPrediosViewModel.getPredio_ID());
                ObjOrdenCreatOT.setNo_Cuenta(ObjPrediosViewModel.getNo_Cuenta());
                ObjOrdenCreatOT.setNumero(ObjPrediosViewModel.getNumero_Exterior().trim() + " " + ObjPrediosViewModel.getNumero_Interior().trim());
            }
        }else {
            ObjOrdenCreatOT.setPredio_ID("");
            ObjOrdenCreatOT.setNo_Cuenta("");
            ObjOrdenCreatOT.setNumero("");
            ObjOrdenCreatOT.setNo_Medidor("");
        }

        if(itemSelectedActividad == null){
            Util.mostarAlerta2(getActivity(), "Seleccione una actividad");
            return  false;
        }

        ObjOrdenCreatOT.setTipo_Falla_ID(itemSelectedActividad.getID());

        if(itemSelectedColonias == null){
            Util.mostarAlerta2(getActivity(), "Seleccione una colonia");
            return  false;
        }


        ObjOrdenCreatOT.setColonia_ID(itemSelectedColonias.getID());

        if(itemSelectedCalle == null){
            Util.mostarAlerta2(getActivity(), "Seleccione una calle");
            return  false;
        }

        ObjOrdenCreatOT.setCalle_ID(itemSelectedCalle.getID());

        if(itemSelectedCalleReferencia == null)
              ObjOrdenCreatOT.setCalle_Referencia_ID("");
        else
            ObjOrdenCreatOT.setCalle_Referencia_ID(itemSelectedCalleReferencia.getID());

        if(itemSelectedCalleReferencia2 == null)
             ObjOrdenCreatOT.setCalle_Referencia_ID2("");
        else
            ObjOrdenCreatOT.setCalle_Referencia_ID2(itemSelectedCalleReferencia2.getID());


        if(ObjOrdenCreatOT.getDomicilio_Referencia().length() > 1){

            if(Util.tieneCarateresPermitidos(ObjOrdenCreatOT.getDomicilio_Referencia()) == false){
                textInputLayoutDomiciloReferencia.setError("Contiene caracteres no permitidos");
                Util.mostarAlerta2(getActivity(),"Permitidos letras,números y estos caracteres: " + Util.CARACTERES_PERMITIDOS);
                return  false;
            }
        }

        textInputLayoutDomiciloReferencia.setError(null);

        if(ObjOrdenCreatOT.getReferencia().length() > 1){

            if(Util.tieneCarateresPermitidos(ObjOrdenCreatOT.getReferencia()) == false){
                textInputLayoutReferencia.setError("Contiene caracteres no permitidos");
                Util.mostarAlerta2(getActivity(),"Permitidos letras,números y estos caracteres: " + Util.CARACTERES_PERMITIDOS);
                return  false;
            }
        }

        textInputLayoutReferencia.setError(null);

        if(ObjOrdenCreatOT.getReporto().length() > 1){
            if(Util.tieneCarateresPermitidos(ObjOrdenCreatOT.getReporto()) == false){
                textInputLayoutReporte.setError("Contiene caracteres no permitidos");
                Util.mostarAlerta2(getActivity(),"Permitidos letras,números y estos caracteres: " + Util.CARACTERES_PERMITIDOS);
                return  false;
            }
        }

        textInputLayoutReporte.setError(null);

        if(ObjOrdenCreatOT.getObservaciones().length() > 1){

            if(Util.tieneCarateresPermitidos(ObjOrdenCreatOT.getObservaciones()) == false){
                textInputLayoutObservaciones.setError("Contiene caracteres no permitidos");
                Util.mostarAlerta2(getActivity(),"Permitidos letras,números y estos caracteres: " + Util.CARACTERES_PERMITIDOS);
                return  false;
            }
        }

        textInputLayoutObservaciones.setError(null);


        return  true;
    }


    public  void  mostrarAlerta(String folio){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Proceso Correcto con Folio: " + folio);
        builder.setTitle("Crear Ordenes de Trabajo");
        builder.setCancelable(false);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = ActividadMenu.nuevaInstancia(getContext());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public  void  guardarOrden(){

        if(validarInterfaz()== false){
            return;
        }


        Gson gson = new Gson();
        String json = gson.toJson(ObjOrdenCreatOT);
        Log.d("DEBUG", json);

        guadarOT();
    }

    // eventos ***********************************************************

    @OnClick(R.id.boton_borrar_autocompletado_actividad)
    public void  onEventoBorrarAutompleadoFallas(){
        autoCompleteTextActividad.setText("");
        buttonBorrarAutompleadoActividad.setVisibility(View.GONE);
        itemSelectedActividad = null;
    }

    @OnClick(R.id.boton_borrar_autocompletado_colonia)
    public void  onEventoBorrarAutompleadoColonias(){

        limpiarAutocompletadoColonias();

        ListaCalles = new ArrayList<>();
        configurarAdapterCalles();
        limpiarAutocompletadoCalle();
        limpiarAutocompletadoCalleReferencia();
        limpiarAutompleadoCalleReferencia2();
    }

    @OnClick(R.id.boton_borrar_autocompletado_calle)
    public void  onEventoBorrarAutompleadoCalles(){
        limpiarAutocompletadoCalle();
    }

    @OnClick(R.id.boton_borrar_autocompletado_calle_referencia)
    public void  onEventoBorrarAutompleadoCallesReferencia(){
        limpiarAutocompletadoCalleReferencia();
    }

    @OnClick(R.id.boton_borrar_autocompletado_calle_referencia2)
    public void  onEventoBorrarAutompleadoCallesReferencia2(){
        limpiarAutompleadoCalleReferencia2();
    }

    @OnClick(R.id.button_ot_buscar)
    public  void  onBuscarCuenta(){
      /*  FragmentManager fragmentManager = getParentFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        fragmentTransaction.add(android.R.id.content, DialogoBuscarCuenta.nuevaInstancia())
                .addToBackStack(null).commit();
        */

        DialogoBuscarCuenta dialogoBuscarCuenta = DialogoBuscarCuenta.nuevaInstancia();
        dialogoBuscarCuenta.setTargetFragment(FragmentosOrdenesTrabajo.this, 8);
        dialogoBuscarCuenta.show(getParentFragmentManager(),"buscar");
    }

    @OnClick(R.id.radio_ot_cuenta_cero)
    public void  onRadioCuentaCero(){
        desbloquearControlesUbicacion();
        ObjPrediosViewModel = null;
        limpiarAutocompletadoColonias();
        limpiarAutocompletadoCalleReferencia();
        limpiarAutocompletadoCalle();
        limpiarAutompleadoCalleReferencia2();
        buttonBuscarRPU.setEnabled(false);
        textInputEditTextRPU.setText("");
    }

    @OnClick(R.id.radio_ot_cuenta_simapag)
    public  void  onRadioCuentaSimapag(){
        ObjPrediosViewModel = null;
         limpiarAutocompletadoColonias();
         limpiarAutocompletadoCalleReferencia();
         limpiarAutocompletadoCalle();
         limpiarAutompleadoCalleReferencia2();
        buttonBuscarRPU.setEnabled(true);
        textInputEditTextRPU.setText("");
         bloquearControlesUbicacion();
    }

    //peticiones *****************************************************************


    public  void  guadarOT(){
        delayedProgressDialog.show(getParentFragmentManager(), "paginado");

        FactoryPeticiones.getFactoryPeticiones()
                .guardarOrdenTrabajo(ObjOrdenCreatOT)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<MensajeModel>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull MensajeModel mensajeModel) {

                        if(mensajeModel.getEstatus().equals("bien")){
                             mostrarAlerta(mensajeModel.getDato1());
                        }else {
                            Util.mostarAlerta2(getActivity(), " " + mensajeModel.getEstatus());
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });

    }

    public  void  obtenerCallerColonias(String pColoniaID){
        delayedProgressDialog.show(getParentFragmentManager(), "paginado");
        FactoryPeticiones.getFactoryPeticiones()
                .obtenerCallesPorColonia(pColoniaID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Cls_Combo>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<Cls_Combo> cls_combos) {
                        ListaCalles = cls_combos;
                        configurarAdapterCalles();
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });
    }

    public void  obtenerParametrosCrearOT(){

        delayedProgressDialog.show(getParentFragmentManager(), "paginado");
        FactoryPeticiones.getFactoryPeticiones()
                .obtenerParametrosCrearOT()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ParametroCrearOT>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull ParametroCrearOT parametroCrearOT) {
                         ListaActividad = parametroCrearOT.getListaFallas();
                         ListaColonias = parametroCrearOT.getListaColonias();
                         configurarAdapterAutoCompleatados();
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });

    }

    // sobre escribir métodos ****************************************************


    @Override
    public void selectedCuenta(PrediosViewModel prediosViewModel) {
            ObjPrediosViewModel = prediosViewModel;

            textInputEditTextRPU.setText(prediosViewModel.getRPU());


            itemSelectedColonias = new Cls_Combo();
            itemSelectedColonias.setID(prediosViewModel.getColonia_ID());
            itemSelectedColonias.setName(prediosViewModel.getColonia());
            autoCompleteTextColonias.setText(itemSelectedColonias.getName());

            itemSelectedCalle = new Cls_Combo();
            itemSelectedCalle.setID(prediosViewModel.getCalle_ID());
            itemSelectedCalle.setName(prediosViewModel.getTipo() + " " + prediosViewModel.getCalle());
            autoCompleteTextCalles.setText(itemSelectedCalle.getName());

            if(prediosViewModel.getCalle_Referencia1_ID().trim().length() == 5){
                itemSelectedCalleReferencia = new Cls_Combo();
                itemSelectedCalleReferencia.setID(prediosViewModel.getCalle_Referencia1_ID());
                itemSelectedCalleReferencia.setName(prediosViewModel.getCalle_Referencia());
                autoCompleteTextCallesReferencia.setText(itemSelectedCalleReferencia.getName());
            }

        if(prediosViewModel.getCalle_Referencia2().trim().length() == 5){
            itemSelectedCalleReferencia2 = new Cls_Combo();
            itemSelectedCalleReferencia2.setID(prediosViewModel.getCalle_Referencia2_ID());
            itemSelectedCalleReferencia2.setName(prediosViewModel.getCalle_Referencia2());
            autoCompleteTextCallesReferencia2.setText(itemSelectedCalleReferencia2.getName());
        }

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_guardar, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.menu_item_aceptar:
                guardarOrden();
                return  true;
            case android.R.id.home:
                Intent intent = ActividadMenu.nuevaInstancia(getContext());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    // clases anidades ***********************************************************
}
