package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ParametrosVisitasInvestigacion {

    @SerializedName("ListaTomas")
    private List<Cls_Combo> ListaTomas;
    @SerializedName("ListaTipoDescarga")
    private List<Cls_Combo> ListaTipoDescarga;
    @SerializedName("OrdenesInvestigacion")
    private  OrdenesInvestigacionViewModel ordenesInvestigacionViewModel;

    public List<Cls_Combo> getListaTomas() {
        return ListaTomas;
    }

    public void setListaTomas(List<Cls_Combo> listaTomas) {
        ListaTomas = listaTomas;
    }

    public List<Cls_Combo> getListaTipoDescarga() {
        return ListaTipoDescarga;
    }

    public void setListaTipoDescarga(List<Cls_Combo> listaTipoDescarga) {
        ListaTipoDescarga = listaTipoDescarga;
    }

    public OrdenesInvestigacionViewModel getOrdenesInvestigacionViewModel() {
        return ordenesInvestigacionViewModel;
    }

    public void setOrdenesInvestigacionViewModel(OrdenesInvestigacionViewModel ordenesInvestigacionViewModel) {
        this.ordenesInvestigacionViewModel = ordenesInvestigacionViewModel;
    }
}
