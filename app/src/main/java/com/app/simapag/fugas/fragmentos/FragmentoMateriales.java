package com.app.simapag.fugas.fragmentos;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadListadoVisitas;
import com.app.simapag.fugas.actividades.ActividadMenuOrdenes;
import com.app.simapag.fugas.actividades.ActvidadListadoMaterial;
import com.app.simapag.fugas.adapter.AutoCompleteMaterialAdapter;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.modelos.MaterialesViewModel;
import com.app.simapag.fugas.modelos.MensajeModel;
import com.app.simapag.fugas.modelos.OrdenTrabajoViewModel;
import com.app.simapag.fugas.modelos.OrdenesMaterialViewModel;
import com.app.simapag.fugas.preferencias.Preferencias;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.singlenton.RepositorioOT;
import com.app.simapag.fugas.util.Util;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FragmentoMateriales extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;

    @BindView(R.id.text_view_titulo_pantalla)
    TextView textViewTituloPantalla;


    @BindView(R.id.autoComplete_materiales)
    AutoCompleteTextView autoCompleteTextViewMateriales;

    @BindView(R.id.text_input_layout_material_cantidad)
    TextInputLayout textInputLayoutCantidad;
    @BindView(R.id.text_input_edit_material_cantidad)
    TextInputEditText textInputEditTextCantidad;

    @BindView(R.id.recyclerview_material_orden)
    RecyclerView recyclerMaterialOrden;
    LinearLayoutManager linearLayoutManager;

    OrdenesMaterialViewModel ordenesMaterialViewModel = new OrdenesMaterialViewModel();
    List<OrdenesMaterialViewModel> ListaMaterialOrdenes = new ArrayList<>();
    MaterialOrdenAdapter ObjMaterialAdapter;

    List<MaterialesViewModel> ListaMateriales = new ArrayList<>();
     MaterialesViewModel materialesViewModelSelected;
    OrdenTrabajoViewModel ordenTrabajoViewModel;
    DelayedProgressDialog delayedProgressDialog;
    private Unbinder unbinder;

    public  static  FragmentoMateriales nuevaInstacia(){
        return  new FragmentoMateriales();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);
        setHasOptionsMenu(true);
        ordenTrabajoViewModel = RepositorioOT.get().getObjTrabajViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_materiales, container, false);
        unbinder = ButterKnife.bind(this, view);
        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");

        textViewTitulo.setText("Folio: " + ordenTrabajoViewModel.getFolio());
        textViewTituloPantalla.setText("Agregar Material a la Orden");

        view.findViewById(R.id.linear_layout_principal).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                View focusedView = getActivity().getCurrentFocus();
                /*
                 * If no view is focused, an NPE will be thrown
                 *
                 * Maxim Dmitriev
                 */
                if (focusedView != null) {
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }


                return true;
            }
        });

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerMaterialOrden.setLayoutManager(linearLayoutManager);


        // pone un división en la parte de abajo
        //recyclerMaterialOrden.addItemDecoration(new DividerItemDecoration(getContext(),
        //        DividerItemDecoration.VERTICAL));

        // customiza la división
       // DividerItemDecoration itemDecorator = new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
       // itemDecorator.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.dividir));
       // recyclerMaterialOrden.addItemDecoration(itemDecorator);

        autoCompleteTextViewMateriales.setImeOptions(EditorInfo.IME_ACTION_DONE);
        autoCompleteTextViewMateriales.setRawInputType(InputType.TYPE_CLASS_TEXT);

        autoCompleteTextViewMateriales.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                materialesViewModelSelected = (MaterialesViewModel) adapterView.getItemAtPosition(i);
                Util.hideKeyboard(getActivity());

                textInputEditTextCantidad.requestFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(textInputEditTextCantidad, InputMethodManager.SHOW_IMPLICIT);



            }
        });

        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ObtenerTodosLosMateriales();
    }

    // métodos

    public  void  configurarAdapterAutoCompletado(){

        if(isAdded()){
            autoCompleteTextViewMateriales.setAdapter(new AutoCompleteMaterialAdapter(getContext(), ListaMateriales));
        }
    }


    public  void  configurarAdapter(){
        ListaMaterialOrdenes.add(ordenesMaterialViewModel);

        if( ObjMaterialAdapter == null){
            ObjMaterialAdapter = new MaterialOrdenAdapter(ListaMaterialOrdenes);
            recyclerMaterialOrden.setAdapter(ObjMaterialAdapter);
        }else {
            ObjMaterialAdapter.notifyDataSetChanged();
        }
    }

    public  void  limpiarAutocompletado(){
        autoCompleteTextViewMateriales.setText("");
        materialesViewModelSelected = null;
        autoCompleteTextViewMateriales.requestFocus();
        textInputEditTextCantidad.setText("");
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(textInputEditTextCantidad, InputMethodManager.SHOW_IMPLICIT);
    }

    public  boolean validarInterfaz(){
        double cantidad = Util.convertirEntero( textInputEditTextCantidad.getText().toString());


        if( materialesViewModelSelected == null ){
            Util.mostarAlerta2(getActivity(), "Se requiere seleccionar un material");
            return false;
        }

        if(cantidad <= 0){
            Util.mostarAlerta2(getActivity(), "La cantidad no puede ser cero");
            return  false;
        }

        ordenesMaterialViewModel.setCantidad(cantidad);
        ordenesMaterialViewModel.setCostoPorUnidad( materialesViewModelSelected.getCosto());
        ordenesMaterialViewModel.setCostoTotal( cantidad * materialesViewModelSelected.getCosto());
        ordenesMaterialViewModel.setNo_Orden_trabajo(ordenTrabajoViewModel.getNo_Orden_Trabajo());
        ordenesMaterialViewModel.setNombre_Material(materialesViewModelSelected.getNombre());
        ordenesMaterialViewModel.setOrdenes_Trabajo_Detalles_Materiales_ID("");
        ordenesMaterialViewModel.setNombre_Usuario(Preferencias.getNombreUsuario(getContext()));
        ordenesMaterialViewModel.setProducto_ID(materialesViewModelSelected.getProducto_ID());
        ordenesMaterialViewModel.setUnidad(materialesViewModelSelected.getUnidad());

        return  true;
    }


    // peticiones

    public void  guardarMaterialesOrden(){
        delayedProgressDialog.show(getParentFragmentManager(), "paginado");
        FactoryPeticiones.getFactoryPeticiones()
                .agregarMaterialOrden(ordenesMaterialViewModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<MensajeModel>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull MensajeModel mensajeModel) {

                        if(mensajeModel.getEstatus().equals("bien")){
                            Util.mostarAlerta2(getActivity(), "Proceso Correcto");
                            configurarAdapter();
                            limpiarAutocompletado();

                        }else {
                            Util.mostarAlerta2(getActivity(), " " + mensajeModel.getEstatus());
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });
    }

    public  void  ObtenerTodosLosMateriales(){
        delayedProgressDialog.show(getParentFragmentManager(), "paginado");

        FactoryPeticiones.getFactoryPeticiones()
                .obtenerMateriales()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<MaterialesViewModel>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<MaterialesViewModel> materialesViewModels) {
                           ListaMateriales = materialesViewModels;
                           configurarAdapterAutoCompletado();
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });
    }



    // eventos

    @OnClick(R.id.button_material_limpiar)
    public  void  onLimpiarMaterial(){
         limpiarAutocompletado();
    }

    @OnClick(R.id.button_material_guardar)
    public  void  onGuardarMaterial(){

        if(validarInterfaz() == false){
            return;
        }

        guardarMaterialesOrden();
    }

    // sobre escribrír métodos

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case android.R.id.home:
                Intent intent = ActvidadListadoMaterial.nuevaInstancia(getContext());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    // clase anidados

    public  class MaterialOrdenAdapter extends  RecyclerView.Adapter<MaterialOrdenHolder>{

        List<OrdenesMaterialViewModel> listaOrdens;

        public MaterialOrdenAdapter(List<OrdenesMaterialViewModel> listaOrdens) {
            this.listaOrdens = listaOrdens;
        }

        @NonNull
        @Override
        public MaterialOrdenHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_material_orden_1, parent, false);
            return new MaterialOrdenHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MaterialOrdenHolder holder, int position) {
            OrdenesMaterialViewModel model = listaOrdens.get(position);
            holder.enlazarDatos(model);


        }

        @Override
        public int getItemCount() {
            return listaOrdens.size();
        }
    }

    public  class  MaterialOrdenHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        OrdenesMaterialViewModel ordenesM;

        @BindView(R.id.text_view_material_nombre)
        TextView textViewNombre;

        @BindView(R.id.text_view_material_cantidad)
        TextView textViewCantidad;

        @BindView(R.id.text_view_material_unidad)
        TextView textViewUnidad;

        @BindView(R.id.text_view_material_costo)
        TextView textViewCosto;

        @BindView(R.id.text_view_material_costo_total)
        TextView textViewCostoTotal;

        public MaterialOrdenHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        public  void  enlazarDatos(OrdenesMaterialViewModel model){
            ordenesM = model;
            textViewNombre.setText(model.getNombre_Material());
            textViewCantidad.setText("Cantidad: " + String.valueOf(model.getCantidad()));
            textViewUnidad.setText("Unidad: " + model.getUnidad());
            textViewCosto.setText("Costo Unidad: " + String.valueOf(model.getCostoPorUnidad()));
            textViewCostoTotal.setText("Costo Total: " + String.valueOf(model.getCostoTotal()));
        }

        @Override
        public void onClick(View view) {

        }
    }
}
