package com.app.simapag.fugas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.simapag.fugas.R;
import com.app.simapag.fugas.modelos.MaterialesViewModel;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteMaterialAdapter extends ArrayAdapter<MaterialesViewModel> {
   private   List<MaterialesViewModel> materialListFull;

    public AutoCompleteMaterialAdapter(@NonNull Context context, @NonNull List<MaterialesViewModel> materialesList) {
        super(context, 0, materialesList);
        materialListFull = new ArrayList<>(materialesList);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return materialesFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null){
             convertView = LayoutInflater.from(getContext()).inflate(R.layout.material_autocomplete_row ,parent, false);
        }

        TextView textViewNombreMaterial = convertView.findViewById(R.id.text_view_material_nombre);

        MaterialesViewModel  materialesViewModel = getItem(position);

        if(materialesViewModel != null){
            textViewNombreMaterial.setText(materialesViewModel.getNombre());
        }

        return  convertView;
    }

    private Filter materialesFilter =  new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<MaterialesViewModel> suggestions = new ArrayList<>();

            if( constraint == null || constraint.length() == 0){
                 suggestions.addAll(materialListFull);
            } else {
                String  filterPattern = constraint.toString().toLowerCase().trim();

                for(MaterialesViewModel item: materialListFull){

                    if(item.getNombre().toLowerCase().contains(filterPattern)){
                        suggestions.add(item);
                    }
                }
            }
            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults results) {
           clear();
           addAll((List) results.values);
           notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((MaterialesViewModel) resultValue).getNombre();
        }
    };
}
