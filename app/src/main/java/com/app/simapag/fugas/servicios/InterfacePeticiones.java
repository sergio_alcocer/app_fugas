package com.app.simapag.fugas.servicios;

import com.app.simapag.fugas.modelos.BusquedaPredioModel;
import com.app.simapag.fugas.modelos.Cls_BusquedaOT_Model;
import com.app.simapag.fugas.modelos.Cls_Busqueda_Orden_Investigacion;
import com.app.simapag.fugas.modelos.Cls_Combo;
import com.app.simapag.fugas.modelos.CoordenadasOrdenesModel;
import com.app.simapag.fugas.modelos.FotoOTModel;
import com.app.simapag.fugas.modelos.LoginModel;
import com.app.simapag.fugas.modelos.MaterialesViewModel;
import com.app.simapag.fugas.modelos.MensajeModel;
import com.app.simapag.fugas.modelos.OrdenCreateModel;
import com.app.simapag.fugas.modelos.OrdenInvestigacionPaginado;
import com.app.simapag.fugas.modelos.OrdenOTUpdate;
import com.app.simapag.fugas.modelos.OrdenesMaterialViewModel;
import com.app.simapag.fugas.modelos.OrdenesTrabajoPaginado;
import com.app.simapag.fugas.modelos.OrderAsingadaModel;
import com.app.simapag.fugas.modelos.ParametroCrearOT;
import com.app.simapag.fugas.modelos.ParametroTrabajoRealizado;
import com.app.simapag.fugas.modelos.ParametrosBusquedaOT;
import com.app.simapag.fugas.modelos.ParametrosVisitasInvestigacion;
import com.app.simapag.fugas.modelos.PrediosPaginado;
import com.app.simapag.fugas.modelos.PrediosViewModel;
import com.app.simapag.fugas.modelos.VisitaInvestigacionModel;
import com.app.simapag.fugas.modelos.VisitasViewModel;


import java.util.List;
import java.util.SplittableRandom;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface InterfacePeticiones {

    @POST("Login/verificarUsuario")
    public Observable<MensajeModel> verificarUsuario(@Body LoginModel loginModel);

    @GET("Busquedas/ObtenerParametrosBusquedaOT")
    public Observable<ParametrosBusquedaOT> ObtenerParametrosBusquedaOT(@Query("empleadoID") String empleadoID);

    @POST("OrdenesTrabajo/obtenerOrdenesTrabajo")
    public  Observable<OrdenesTrabajoPaginado> obtenerOrdenesTrabajo(@Body Cls_BusquedaOT_Model ModelBusqueda);

    @GET("Brigadas/ObtenerBrigadasPorDistritos")
    public Observable<List<Cls_Combo>> ObtenerBrigadasPorDistritos(@Query("distrito_id") String distrito_id);

    @POST("Visitas/agregarVisita")
    public Observable<MensajeModel> agregarVisita(@Body VisitasViewModel visitasViewModel);

    @GET("Visitas/eliminarVisita")
    public  Observable<MensajeModel> eliminarVisita(@Query("pNoVisitaTrabajo") String pNoVisitaTrabajo, @Query("pUsuario") String pUsuario);

    @GET("Visitas/obtenerVisita")
    public  Observable<List<VisitasViewModel>> obtenerVisita(@Query("pNoOrdenTrabajo")  String pNoOrdenTrabajo );

    @GET("Materiales/obtenerMateriales")
    public  Observable<List<MaterialesViewModel>> obtenerMateriales();

    @GET("OrdenesMaterial/obtenerMaterial")
    public  Observable<List<OrdenesMaterialViewModel>> obtenerMaterialOrdenes(@Query("NoOrdenTrabajo") String NoOrdenTrabajo);
    @POST("OrdenesMaterial/agregarMaterial")
    public  Observable<MensajeModel> agregarMaterialOrden(@Body OrdenesMaterialViewModel ordenesMaterialViewModel);
    @GET("OrdenesMaterial/eliminarMaterial")
    public  Observable<MensajeModel> eliminarMaterialOrden(@Query("Ordenes_Trabajo_Detalles_Materiales_ID") String Ordenes_Trabajo_Detalles_Materiales_ID);

    @POST("OrdenesTrabajo/actualizarCoordenadas")
    public Observable<MensajeModel>  actualizarCoordenadas(@Body CoordenadasOrdenesModel coordenadasOrdenesModel);
    @GET("OrdenesTrabajo/obtenerCoordenadasTrabajo")
    public  Observable<CoordenadasOrdenesModel> obtenerCoordenadasTrabajo(@Query("No_Orden_Trabajo") String No_Orden_Trabajo);

    @GET("Busquedas/ObtenerParametroTrabajoRealizado")
    public Observable<ParametroTrabajoRealizado> obtenerParametrosTrabajoRealizado(@Query("Empleado_ID") String Empleado_ID, @Query("No_Orden_Trabajo")  String No_Orden_Trabajo);

    @GET("Busquedas/ObtenerEmpleadosBrigada")
    public  Observable< List<Cls_Combo>> ObtenerEmpleadosBrigada(@Query("Brigada_ID") String Brigada_ID);

    @POST("OrdenesTrabajo/modificarOrdenTrabajo")
    public  Observable<MensajeModel>  modificarOrdenTrabajo(@Body OrdenOTUpdate ordenOTUpdate);

    @POST("Imagenes/guardarImagen")
    Call<MensajeModel> subirImagen(@Body FotoOTModel fotoOTModel);

    @GET("Imagenes/obtenerImagen")
    public  Observable<List<FotoOTModel>> obtenerFotos(@Query("No_Orden_Trabajo") String No_Orden_Trabajo);

    @GET("Busquedas/obtenerParametrosCrearOT")
    public  Observable<ParametroCrearOT>  obtenerParametrosCrearOT();

    @GET("Busquedas/obtenerCallesPorColonia")
    public  Observable<List<Cls_Combo>> obtenerCallesPorColonia(@Query("Colonia_ID") String Colonia_ID);

    @POST("Predios/obtenerPredios")
    public  Observable<PrediosPaginado> obtenerPredios(@Body BusquedaPredioModel busquedaPredioModel);
    @POST("OrdenesTrabajo/guardarOrdenTrabajo")
    public Observable<MensajeModel> guardarOrdenTrabajo(@Body OrdenCreateModel ordenCreateModel);

    @POST("OrdenesTrabajo/asignarOrdenesTrabajo")
    public  Observable<MensajeModel> asignarOrdenesTrabajo(@Body List<OrderAsingadaModel> listaAsignacion);

    @POST("OrdenInvestigacion/obtenerOrdenesInvestigacion")
    public  Observable<OrdenInvestigacionPaginado> obtenerOrdenInvestigacion(@Body Cls_Busqueda_Orden_Investigacion cls_busqueda_orden_investigacion);

    @GET("OrdenInvestigacion/obtenerVisitaInvestigacion")
    public Observable<List<VisitaInvestigacionModel>> ObtenerVisitasOrdenInvestigacion(@Query("No_Solicitud") String No_Solicitud);

    @GET("OrdenInvestigacion/obtenerParametroVisitasInvestigacion")
    public  Observable<ParametrosVisitasInvestigacion> ObtenerParametrosVisitasInvestigacion(@Query("No_Solicitud") String No_Solicitud);
}
