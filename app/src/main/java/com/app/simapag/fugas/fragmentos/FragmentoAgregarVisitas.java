package com.app.simapag.fugas.fragmentos;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.R;

import com.app.simapag.fugas.actividades.ActividadListadoVisitas;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.modelos.MensajeModel;
import com.app.simapag.fugas.modelos.OrdenTrabajoViewModel;
import com.app.simapag.fugas.modelos.VisitasViewModel;
import com.app.simapag.fugas.preferencias.Preferencias;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.singlenton.RepositorioOT;
import com.app.simapag.fugas.util.DatePickerWrapper;
import com.app.simapag.fugas.util.Util;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FragmentoAgregarVisitas extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;

    @BindView(R.id.text_view_titulo_pantalla)
    TextView textViewTituloPantalla;

    @BindView(R.id.text_input_layout_visita_comentario)
    TextInputLayout textInputLayoutComentario;

    @BindView(R.id.text_input_edit_visita_comentario)
    TextInputEditText textInputEditTextComentario;

    OrdenTrabajoViewModel ordenTrabajoViewModel;
    DelayedProgressDialog delayedProgressDialog;

    DatePickerWrapper datePickerWrapperFechaInicio;
    SimpleDateFormat formatFecha = new SimpleDateFormat("yyyy-MM-dd");

    VisitasViewModel ObjVisitasViewModel = new VisitasViewModel();
    private Unbinder unbinder;


    public static FragmentoAgregarVisitas nuevaInstancia(){
        return new FragmentoAgregarVisitas();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);
        setHasOptionsMenu(true);
        ordenTrabajoViewModel = RepositorioOT.get().getObjTrabajViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_agregar_visita, container, false);
        unbinder = ButterKnife.bind(this, view);

        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");

        textViewTitulo.setText("Folio: " + ordenTrabajoViewModel.getFolio());
        textViewTituloPantalla.setText("Agregar Visitas");

        view.findViewById(R.id.linear_layout_principal).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                View focusedView = getActivity().getCurrentFocus();
                /*
                 * If no view is focused, an NPE will be thrown
                 *
                 * Maxim Dmitriev
                 */
                if (focusedView != null) {
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }


                return true;
            }
        });

        datePickerWrapperFechaInicio = new DatePickerWrapper((TextView) view.findViewById(R.id.text_view_visita_fecha),getActivity());
        textInputEditTextComentario.setImeOptions(EditorInfo.IME_ACTION_DONE);
        textInputEditTextComentario.setRawInputType(InputType.TYPE_CLASS_TEXT);
        return  view;
    }


    // métodos


    public  boolean validarInterfaz(){
        Date dateFecha;

        dateFecha = datePickerWrapperFechaInicio.getDate();
        ObjVisitasViewModel.setFecha(formatFecha.format(dateFecha));
        ObjVisitasViewModel.setEstatus("ACTIVO");
        ObjVisitasViewModel.setDetalles(textInputEditTextComentario.getText().toString().trim());
        ObjVisitasViewModel.setNo_Orden_Trabajo(ordenTrabajoViewModel.getNo_Orden_Trabajo());
        ObjVisitasViewModel.setNombre_Usuario(Preferencias.getNombreUsuario(getContext()));

        if(ObjVisitasViewModel.getDetalles().length() < 2){
            textInputLayoutComentario.setError("Se require un comentario");
            return  false;
        }

        if(Util.tieneCarateresPermitidos(ObjVisitasViewModel.getDetalles()) == false){
            textInputLayoutComentario.setError("Contiene caracteres no permitidos");
            Util.mostarAlerta2(getActivity(),"Permitidos letras,números y estos caracteres: " + Util.CARACTERES_PERMITIDOS);
            return  false;
        }
        textInputLayoutComentario.setError(null);

        return  true;
    }

    public  void  guardarVisita(){

        if(validarInterfaz() == false){
            return;
        }

        enviarVisita();
    }

    public  void  mostrarAlerta(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Proceso Correcto");
        builder.setTitle("Visitas");
        builder.setCancelable(false);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                  Intent intent = ActividadListadoVisitas.nuevaInstancia(getContext());
                  startActivity(intent);

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    // eventos

    // peticiones

    public  void  enviarVisita(){
        delayedProgressDialog.show(getParentFragmentManager(), "paginado");

        Gson gson = new Gson();
        String json = gson.toJson(ObjVisitasViewModel);
        Log.d("DEBUG", json);

        FactoryPeticiones.getFactoryPeticiones()
                .agregarVisita(ObjVisitasViewModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<MensajeModel>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull MensajeModel mensajeModel) {

                        if(mensajeModel.getEstatus().equals("bien")){
                             mostrarAlerta();
                        }else {
                            Util.mostarAlerta2(getActivity(), " " + mensajeModel.getEstatus());
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });
    }

    // sobre escribir métodos

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_guardar, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.menu_item_aceptar:
                guardarVisita();
                return  true;
            case android.R.id.home:
                Intent intent = ActividadListadoVisitas.nuevaInstancia(getContext());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    // clases anidadas

}
