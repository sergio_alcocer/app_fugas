package com.app.simapag.fugas.fragmentos;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.BuildConfig;
import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadMenu;
import com.app.simapag.fugas.actividades.ActividadMenuOrdenes;
import com.app.simapag.fugas.actividades.ActvidadListadoMaterial;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.modelos.OrdenTrabajoViewModel;
import com.app.simapag.fugas.singlenton.RepositorioOT;
import com.app.simapag.fugas.util.Util;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FragmentoInformacionOT extends Fragment {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;

    @BindView(R.id.text_input_edit_informacion_folio)
    TextInputEditText textInputEditTextFolio;
    @BindView(R.id.text_input_edit_informacion_fecha)
    TextInputEditText textInputEditTextFecha;
    @BindView(R.id.text_input_edit_informacion_No_Cuenta)
    TextInputEditText textInputEditTextNoCuenta;
    @BindView(R.id.text_input_edit_informacion_RPU)
    TextInputEditText textInputEditTextRpu;
    @BindView(R.id.text_input_edit_informacion_domicilio)
    TextInputEditText textInputEditTextDomicilio;
    @BindView(R.id.text_input_edit_informacion_colonia)
    TextInputEditText textInputEditTextColonia;
    @BindView(R.id.text_input_edit_informacion_comentario)
    TextInputEditText textInputEditTextComentario;

    @BindView(R.id.text_input_edit_informacion_usuario)
    TextInputEditText textInputEditTextUsuario;
    @BindView(R.id.text_input_edit_informacion_falla)
    TextInputEditText textInputEditTextFalla;

    @BindView(R.id.text_input_edit_informacion_estatus)
    TextInputEditText textInputEditTextEstatus;
    @BindView(R.id.text_input_edit_informacion_no_medidor)
    TextInputEditText textInputEditTextNoMedidor;


    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;
    LocationRequest mLocationRequests = LocationRequest.create();
    private  static  final  long UPDATE_INTERVAL = 10000, FASTER_INTERVAL = 5000 ; // = 5 seconds
    boolean blnPermisos = false;
    double latitud = 0;
    double longitud = 0;


    OrdenTrabajoViewModel ordenTrabajoViewModel;
    DelayedProgressDialog delayedProgressDialog;
    private Unbinder unbinder;
    private boolean blnSepuedeRegresar = false;

    public static  FragmentoInformacionOT nuevaInstancia(){
        return  new FragmentoInformacionOT();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);
        setHasOptionsMenu(true);
        ordenTrabajoViewModel = RepositorioOT.get().getObjTrabajViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_informacion_ot, container, false);
        unbinder = ButterKnife.bind(this, view);

        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");

        textViewTitulo.setText("Información OT");

        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                Location location = locationResult.getLastLocation();
                if (location != null){
                    stopGPS();
                    delayedProgressDialog.cancel();
                    latitud = location.getLatitude();
                    longitud = location.getLongitude();

                    abrirMapaGoogle();
                }
            }
        };

        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        visuzualizarInformacion();

    }

    public boolean isBlnSepuedeRegresar() {
        return blnSepuedeRegresar;
    }

    public  void  visuzualizarInformacion(){

        if(isAdded()){
            textInputEditTextColonia.setText(ordenTrabajoViewModel.getColonia());
            textInputEditTextComentario.setText(ordenTrabajoViewModel.getObservaciones());
            textInputEditTextDomicilio.setText(ordenTrabajoViewModel.getDomicilio());
            textInputEditTextEstatus.setText(ordenTrabajoViewModel.getEstatus());
            textInputEditTextFalla.setText(ordenTrabajoViewModel.getFalla());
            textInputEditTextFecha.setText(ordenTrabajoViewModel.getFecha_Elaboro());
            textInputEditTextFolio.setText(ordenTrabajoViewModel.getFolio());
            textInputEditTextNoCuenta.setText(ordenTrabajoViewModel.getNo_Cuenta());
            textInputEditTextNoMedidor.setText(ordenTrabajoViewModel.getNo_Medidor());
            textInputEditTextRpu.setText(ordenTrabajoViewModel.getRPU());
            textInputEditTextUsuario.setText(ordenTrabajoViewModel.getUsuario());
            textInputEditTextFalla.setText(ordenTrabajoViewModel.getFalla());

            bloquearCajas();
        }

    }

    public  void  bloquearCajas(){

        textInputEditTextColonia.setEnabled(false);
        textInputEditTextComentario.setEnabled(false);
        textInputEditTextDomicilio.setEnabled(false);
        textInputEditTextEstatus.setEnabled(false);
        textInputEditTextFalla.setEnabled(false);
        textInputEditTextFecha.setEnabled(false);
        textInputEditTextFolio.setEnabled(false);
        textInputEditTextNoCuenta.setEnabled(false);
        textInputEditTextNoMedidor.setEnabled(false);
        textInputEditTextRpu.setEnabled(false);
        textInputEditTextUsuario.setEnabled(false);
        textInputEditTextFalla.setEnabled(false);
    }

    public  void abrirMapaGoogle(){

        String uri = "http://maps.google.com/maps?saddr=" + latitud + "," + longitud + "&daddr=" + ordenTrabajoViewModel.getLatitud() + "," + ordenTrabajoViewModel.getLongitud() + "&mode=d";

        Uri gmmIntentUri = Uri.parse(uri);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(mapIntent);
        }else {
            Util.mostarAlerta2(getActivity(), "Posiblemente no tiene instaldo la app de Mapas de Google");
        }
    }

    public void  abrirMapa(){

        if(ordenTrabajoViewModel.getLatitud() != 0 && ordenTrabajoViewModel.getLongitud() != 0){
               obtenerCoordenasGeograficas();
        }else {
            Util.mostarAlerta2(getActivity(), "No tiene coordenadas geográficas");
        }

    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction( Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public  void  stopGPS(){

        if(locationCallback != null){
            if(fusedLocationClient != null){
                if(blnPermisos){
                    fusedLocationClient.removeLocationUpdates(locationCallback);
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        fusedLocationClient.requestLocationUpdates(mLocationRequests,
                locationCallback,
                Looper.getMainLooper());
    }


    public  void obtenerCoordenasGeograficas(){

        Dexter.withContext(getActivity())
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                if(report.areAllPermissionsGranted()){
                    // si todos los permisos son concedidos

                    fusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
                    mLocationRequests.setInterval(UPDATE_INTERVAL);
                    mLocationRequests.setFastestInterval(FASTER_INTERVAL);
                    mLocationRequests.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

                    // Create LocationSettingsRequest object using location request
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
                    builder.addLocationRequest(mLocationRequests);
                    LocationSettingsRequest locationSettingsRequest = builder.build();

                    delayedProgressDialog.show(getParentFragmentManager(),"gps");
                    SettingsClient settingsClient = LocationServices.getSettingsClient(getActivity());
                    Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(locationSettingsRequest);

                    task.addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            // All location settings are satisfied. The client can initialize
                            // location requests here.
                            // ...
                            startLocationUpdates();
                            blnPermisos = true;
                        }
                    });

                    task.addOnFailureListener(getActivity(), new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            delayedProgressDialog.cancel();
                            Util.mostarAlerta2(getActivity(), "No se pudo conectar al gps del dispositivo");
                            if (e instanceof ResolvableApiException) {
                                // Location settings are not satisfied, but this can be fixed
                                // by showing the user a dialog.
                                try {
                                    // Show the dialog by calling startResolutionForResult(),
                                    // and check the result in onActivityResult().
                                    ResolvableApiException resolvable = (ResolvableApiException) e;
                                    resolvable.startResolutionForResult(getActivity(),
                                            1);
                                } catch (IntentSender.SendIntentException sendEx) {
                                    // Ignore the error.
                                }
                            }
                        }
                    });

                }else {
                    openSettings();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();

    }


    // sobre escribir métodos

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_mapa, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.menu_item_mapita:
                abrirMapa();
             return  true;
            case android.R.id.home:
                Intent intent = ActividadMenuOrdenes.nuevaInstancia(getContext());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        stopGPS();
    }

}
