package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class Cls_Busqueda_Orden_Investigacion {

    @SerializedName("Pagina")
    private  int Pagina;
    @SerializedName("RPU")
    private  String RPU;
    @SerializedName("Aprobada")
    private  String Aprobada;
    @SerializedName("Estatus")
    private  String Estatus;
    @SerializedName("Fecha_Inicio")
    private  String Fecha_Inicio;
    @SerializedName("Fecha_Fin")
    private  String Fecha_Fin;


    public int getPagina() {
        return Pagina;
    }

    public void setPagina(int pagina) {
        Pagina = pagina;
    }

    public String getRPU() {
        return RPU;
    }

    public void setRPU(String RPU) {
        this.RPU = RPU;
    }

    public String getAprobada() {
        return Aprobada;
    }

    public void setAprobada(String aprobada) {
        Aprobada = aprobada;
    }

    public String getEstatus() {
        return Estatus;
    }

    public void setEstatus(String estatus) {
        Estatus = estatus;
    }

    public String getFecha_Inicio() {
        return Fecha_Inicio;
    }

    public void setFecha_Inicio(String fecha_Inicio) {
        Fecha_Inicio = fecha_Inicio;
    }

    public String getFecha_Fin() {
        return Fecha_Fin;
    }

    public void setFecha_Fin(String fecha_Fin) {
        Fecha_Fin = fecha_Fin;
    }
}
