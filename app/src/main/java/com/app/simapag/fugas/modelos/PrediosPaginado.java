package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PrediosPaginado {

    @SerializedName("TotalElementos")
    private  int TotalElementos;
    @SerializedName("ListaPredios")
    private List<PrediosViewModel> ListaPredios;

    public int getTotalElementos() {
        return TotalElementos;
    }

    public void setTotalElementos(int totalElementos) {
        TotalElementos = totalElementos;
    }

    public List<PrediosViewModel> getListaPredios() {
        return ListaPredios;
    }

    public void setListaPredios(List<PrediosViewModel> listaPredios) {
        ListaPredios = listaPredios;
    }
}
