package com.app.simapag.fugas.singlenton;

import com.app.simapag.fugas.modelos.OrdenesInvestigacionViewModel;

public class RepositorioVisita {
    private static  RepositorioVisita sRepoVisita;
    private OrdenesInvestigacionViewModel ordenesInvestigacionViewModel;


    public  static RepositorioVisita  get(){

        if(sRepoVisita == null){
             sRepoVisita = new RepositorioVisita();
        }

        return  sRepoVisita;
    }

    public OrdenesInvestigacionViewModel getOrdenesInvestigacionViewModel() {
        return ordenesInvestigacionViewModel;
    }

    public void setOrdenesInvestigacionViewModel(OrdenesInvestigacionViewModel ordenesInvestigacionViewModel) {
        this.ordenesInvestigacionViewModel = ordenesInvestigacionViewModel;
    }

    public void clearInstancia(){ sRepoVisita = null;}
}
