package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ParametroCrearOT {

    @SerializedName("ListaFallas")
     private List<Cls_Combo> ListaFallas;
    @SerializedName("ListaColonias")
     private  List<Cls_Combo> ListaColonias;

    public List<Cls_Combo> getListaFallas() {
        return ListaFallas;
    }

    public void setListaFallas(List<Cls_Combo> listaFallas) {
        ListaFallas = listaFallas;
    }

    public List<Cls_Combo> getListaColonias() {
        return ListaColonias;
    }

    public void setListaColonias(List<Cls_Combo> listaColonias) {
        ListaColonias = listaColonias;
    }
}
