package com.app.simapag.fugas.fragmentos;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadListadoOrdenes;
import com.app.simapag.fugas.actividades.ActividadListadoOrdenesRegistradas;
import com.app.simapag.fugas.actividades.ActividadMenu;
import com.app.simapag.fugas.actividades.ActvidadListadoMaterial;
import com.app.simapag.fugas.adapter.AutoCompleteCombos;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.dialogos.DialogoBuscarCuenta;
import com.app.simapag.fugas.dialogos.DialogoPregunta;
import com.app.simapag.fugas.modelos.Cls_Combo;
import com.app.simapag.fugas.modelos.MensajeModel;
import com.app.simapag.fugas.modelos.OrdenTrabajoViewModel;
import com.app.simapag.fugas.modelos.OrderAsingadaModel;
import com.app.simapag.fugas.preferencias.Preferencias;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.singlenton.RepositorioOT;
import com.app.simapag.fugas.util.Util;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FragmentoAsignacionOT extends Fragment implements  DialogoPregunta.RespuestaListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;
    private  static final int REQUEST_CODIGO = 1;

    @BindView(R.id.text_view_ordenes_colonia)
    TextView textViewColonia;

    @BindView(R.id.text_view_ordenes_domicilio)
    TextView textViewDomicilio;

    @BindView(R.id.text_view_ordenes_falla)
    TextView textViewFalla;

    @BindView(R.id.text_view_ordenes_folio)
    TextView textViewFolio;

    @BindView(R.id.text_view_fecha_ordenes_registro)
    TextView textViewFechaRegistro;

    @BindView(R.id.text_view_ordenes_no_cuenta)
    TextView textViewCuenta;

    @BindView(R.id.text_view_ordenes_rpu)
    TextView textViewRPU;

    @BindView(R.id.text_view_ordenes_usuario)
    TextView textViewUsuario;

    @BindView(R.id.text_view_ordenes_estatus)
    TextView textViewEstatus;

    @BindView(R.id.layout_datos_ordenes_cuenta)
    View viewDatosCuenta;

    @BindView(R.id.autoComplete_empleado)
    AutoCompleteTextView autoCompleteTextEmpleado;

    @BindView(R.id.boton_borrar_autocompletado_empleado)
    Button buttonBorrarAutompleadoEmpleado;

    List<Cls_Combo> ListaEmpledo = new ArrayList<>();
    Cls_Combo itemSelectedEmpleado;

    @BindView(R.id.recyclerview_empleados_asignados)
    RecyclerView recyclerEmpleadosAsignado;
    LinearLayoutManager linearLayoutManager;

    List<OrderAsingadaModel> ListaAsignados = new ArrayList<>();
    AsignacionAdapter ObjAsignacionAdapter;

    OrdenTrabajoViewModel ordenTrabajoViewModel;
    DelayedProgressDialog delayedProgressDialog;
    private Unbinder unbinder;

    public  static  FragmentoAsignacionOT nuevaInstancia(){
        return  new FragmentoAsignacionOT();
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);
        setHasOptionsMenu(true);
        ordenTrabajoViewModel = RepositorioOT.get().getObjTrabajViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_asignacion_ot, container, false);
        unbinder = ButterKnife.bind(this, view);
        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");

        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerEmpleadosAsignado.setLayoutManager(linearLayoutManager);

        textViewTitulo.setText("Asingar OT");

        buttonBorrarAutompleadoEmpleado.setVisibility(View.GONE);
        autoCompleteTextEmpleado.setImeOptions(EditorInfo.IME_ACTION_DONE);
        autoCompleteTextEmpleado.setRawInputType(InputType.TYPE_CLASS_TEXT);

        autoCompleteTextEmpleado.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                if(s.length() != 0) {
                    buttonBorrarAutompleadoEmpleado.setVisibility(View.VISIBLE);
                } else {
                    buttonBorrarAutompleadoEmpleado.setVisibility(View.GONE);
                    itemSelectedEmpleado = null;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        autoCompleteTextEmpleado.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                itemSelectedEmpleado = (Cls_Combo) adapterView.getItemAtPosition(i);
                buttonBorrarAutompleadoEmpleado.setVisibility(View.VISIBLE);
                Util.hideKeyboard(getActivity());
            }
        });


        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        visualizarDatos(ordenTrabajoViewModel);
        obtenerEmpleadosAsignar(ordenTrabajoViewModel.getBrigada_ID());
    }

    //**************************** métodos

    public void configurarAdapter(){

        if(isAdded()){
            if(ObjAsignacionAdapter == null){
                ObjAsignacionAdapter = new AsignacionAdapter(ListaAsignados);
                recyclerEmpleadosAsignado.setAdapter(ObjAsignacionAdapter);
            }else {
                ObjAsignacionAdapter.setOrderAsingadaModels(ListaAsignados);
                ObjAsignacionAdapter.notifyDataSetChanged();
            }
        }
    }

    public  void  configurarAdapterAutocompletados(){

        if(isAdded()){
            autoCompleteTextEmpleado.setAdapter(new AutoCompleteCombos(getContext(), ListaEmpledo));
        }
    }

    public  void  visualizarDatos(OrdenTrabajoViewModel model){


        textViewFolio.setText("Folio: " + model.getFolio());
        textViewFechaRegistro.setText(model.getFecha_Elaboro());
        textViewFalla.setText(model.getFalla());
        textViewColonia.setText(model.getColonia());
        textViewDomicilio.setText(model.getDomicilio());
        textViewEstatus.setText(model.getEstatus());

        if(model.getRPU().trim().length() == 0){
            viewDatosCuenta.setVisibility(View.GONE);
        } else {
            viewDatosCuenta.setVisibility(View.VISIBLE);
        }

        textViewCuenta.setText("No.Cuenta: " + model.getNo_Cuenta());
        textViewRPU.setText("RPU:" + model.getRPU());
        textViewUsuario.setText(model.getUsuario());



    }


    public  void  mostrarAlerta(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Proceso Correcto");
        builder.setTitle("Asignar Ordenes de Trabajo");
        builder.setCancelable(false);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = ActividadMenu.nuevaInstancia(getContext());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }


    public int obtenerIndex(String pEmpleadoID){
        int index = -1;
        int contador = 0;

        for (contador = 0; contador < ListaAsignados.size(); contador++){

            if(ListaAsignados.get(contador).getEmpleado_ID().equals(pEmpleadoID)){
                index = contador;
                break;
            }
        }

        return  index;
    }

    //********************************************* eventos


    public void  limpiarEmpleado(){
        autoCompleteTextEmpleado.setText("");
        buttonBorrarAutompleadoEmpleado.setVisibility(View.GONE);
        itemSelectedEmpleado = null;
    }

    @OnClick(R.id.boton_borrar_autocompletado_empleado)
    public void  onEventoBorrarAutompleadoFallas(){
         limpiarEmpleado();
    }

    @OnClick(R.id.button_empleado_agregar)
    public  void  onEventoEmpleadoAgregar(){
      int index= -1;
        if(itemSelectedEmpleado == null){
            return;
        }

        OrderAsingadaModel orderAsingadaModel = new OrderAsingadaModel();
        orderAsingadaModel.setEmpleado_ID(itemSelectedEmpleado.getID());
        orderAsingadaModel.setNombre_Empleado(itemSelectedEmpleado.getName());
        orderAsingadaModel.setNo_Orden_Trabajo(ordenTrabajoViewModel.getNo_Orden_Trabajo());
        orderAsingadaModel.setUsuario_Creo(Preferencias.getNombreUsuario(getContext()));


        index = obtenerIndex(orderAsingadaModel.getEmpleado_ID());

        if(index != -1){
            Util.mostarAlerta2(getActivity(), "El empleado seleccionado ya fue agregado");
            return;
        }

        ListaAsignados.add(orderAsingadaModel);
        configurarAdapter();
        limpiarEmpleado();

    }

    //******************************************* peticiones

    public  void  asignarOT(){
        delayedProgressDialog.show(getParentFragmentManager(), "paginado");

        Gson gson = new Gson();
        String json = gson.toJson(ListaAsignados);
        Log.d("DEBUG",json);

        FactoryPeticiones.getFactoryPeticiones()
                .asignarOrdenesTrabajo(ListaAsignados)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<MensajeModel>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull MensajeModel mensajeModel) {

                        if(mensajeModel.getEstatus().equals("bien")){
                             mostrarAlerta();
                        }else {
                             Util.mostarAlerta2(getActivity(), "" + mensajeModel.getEstatus());
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });

    }

    public  void  obtenerEmpleadosAsignar(String pBrigadaID){

        delayedProgressDialog.show(getParentFragmentManager(), "paginado");

        FactoryPeticiones.getFactoryPeticiones()
                .ObtenerEmpleadosBrigada(pBrigadaID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Cls_Combo>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<Cls_Combo> cls_combos) {

                        ListaEmpledo = cls_combos;
                        configurarAdapterAutocompletados();
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });
    }

    //**************************** sobre escribir métodos


    @Override
    public void aceptarRespuestaSI(String identificador) {

        int index = obtenerIndex(identificador);
        ListaAsignados.remove(index);
        configurarAdapter();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_guardar, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case  R.id.menu_item_aceptar:

                if( ListaAsignados.size() > 0){
                    asignarOT();
                }else {
                    Util.mostarAlerta2(getActivity(),"Agregue un empleado");
                }

                return  true;
            case android.R.id.home:
                Intent intent1 = ActividadListadoOrdenesRegistradas.nuevaInstancia(getContext());
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    //*************************************************** clases anidadas


    public  class  AsignacionAdapter extends RecyclerView.Adapter<AsignacionHolder> {

        List<OrderAsingadaModel> orderAsingadaModels;

        public AsignacionAdapter(List<OrderAsingadaModel> orderAsingadaModels) {
            this.orderAsingadaModels = orderAsingadaModels;
        }

        public void setOrderAsingadaModels(List<OrderAsingadaModel> orderAsingadaModels) {
            this.orderAsingadaModels = orderAsingadaModels;
        }

        @NonNull
        @Override
        public AsignacionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_empleado, parent, false);
            return new AsignacionHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull AsignacionHolder holder, int position) {
            OrderAsingadaModel orderAsingadaModel = orderAsingadaModels.get(position);
            holder.enlazarDatos(orderAsingadaModel);

            holder.buttonEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String mensaje = " " + orderAsingadaModel.getNombre_Empleado();
                    DialogoPregunta dialogoPregunta = DialogoPregunta.nuevaInstancia(orderAsingadaModel.getEmpleado_ID(),mensaje,"Asignación OT");
                    dialogoPregunta.setTargetFragment(FragmentoAsignacionOT.this, REQUEST_CODIGO);
                    dialogoPregunta.show(getParentFragmentManager(), "codigo");
                }
            });
        }

        @Override
        public int getItemCount() {
            return orderAsingadaModels.size();
        }
    }

    public  class  AsignacionHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{

        @BindView(R.id.text_view_empleado)
        TextView textViewNombreEmpleado;
        @BindView(R.id.button_empleado_eliminar)
        Button buttonEliminar;

        OrderAsingadaModel orderAsingadaModel;

        public AsignacionHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

       public  void  enlazarDatos(OrderAsingadaModel model){
            orderAsingadaModel = model;
            textViewNombreEmpleado.setText(model.getNombre_Empleado());
       }

        @Override
        public void onClick(View view) {

        }
    }


}
