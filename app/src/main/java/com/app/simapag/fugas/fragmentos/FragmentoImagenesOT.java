package com.app.simapag.fugas.fragmentos;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.simapag.fugas.BuildConfig;
import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadListadoVisitas;
import com.app.simapag.fugas.actividades.ActividadMenuOrdenes;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.dialogos.DialogoVisualizarFoto;
import com.app.simapag.fugas.modelos.FotoOTModel;
import com.app.simapag.fugas.modelos.MensajeModel;
import com.app.simapag.fugas.modelos.OrdenTrabajoViewModel;
import com.app.simapag.fugas.preferencias.Preferencias;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.singlenton.RepositorioOT;
import com.app.simapag.fugas.util.Util;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;

public class FragmentoImagenesOT extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;

    @BindView(R.id.text_view_titulo_pantalla)
    TextView textViewTituloPantalla;

    @BindView(R.id.recyclerview_fotos_ot)
    RecyclerView recyclerFotos;
    LinearLayoutManager linearLayoutManager;


    private  static  final  int REQUEST_PHOTO = 1;
    File fileFoto;

    List<FotoOTModel> ListaFotoModel = new ArrayList<>();
    FotoOTModel ObjFotoModel = new FotoOTModel();

    OrdenTrabajoViewModel ordenTrabajoViewModel;
    DelayedProgressDialog delayedProgressDialog;
    private Unbinder unbinder;
    private boolean blnSepuedeRegresar = false;

    public  static  FragmentoImagenesOT nuevaInstancia(){
        return  new FragmentoImagenesOT();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);
        setHasOptionsMenu(true);
        ordenTrabajoViewModel = RepositorioOT.get().getObjTrabajViewModel();

        ObjFotoModel.setNombre_Usuario(Preferencias.getNombreUsuario(getContext()));
        ObjFotoModel.setRPU(ordenTrabajoViewModel.getRPU());
        ObjFotoModel.setNo_Orden_Trabajo(ordenTrabajoViewModel.getNo_Orden_Trabajo());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_listado_fotografias_ot, container, false);
        unbinder = ButterKnife.bind(this, view);

        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");

        textViewTitulo.setText("Folio: " + ordenTrabajoViewModel.getFolio());
        textViewTituloPantalla.setText("Listado de Fotografías");

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerFotos.setLayoutManager(linearLayoutManager);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        obtenerListadoFotos(ordenTrabajoViewModel.getNo_Orden_Trabajo());
    }

    // métodos


    public boolean isBlnSepuedeRegresar() {
        return blnSepuedeRegresar;
    }

    public  void  configurarAdapter(){

        if(isAdded()){
              recyclerFotos.setAdapter(new ImagenOTAdapter(ListaFotoModel));
        }
   }

    public  void  eliminarFoto(){

        if(fileFoto != null && fileFoto.exists()){
            fileFoto.delete();
        }
    }

    public  void  ejecutarProcesoCamara(){
        fileFoto = null;
        final  Intent capturarImagen = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String ID = UUID.randomUUID().toString();
        fileFoto = Util.obtenerArchivoFoto(getContext(), ID+".jpg");

        Uri uri = FileProvider.getUriForFile(getActivity(),"com.app.simapag.fugas.fileprovider",fileFoto);
        capturarImagen.putExtra(MediaStore.EXTRA_OUTPUT, uri);

        List<ResolveInfo> camaraActividades = getActivity()
                .getPackageManager().queryIntentActivities(capturarImagen, PackageManager.MATCH_DEFAULT_ONLY);

        for(ResolveInfo actividad: camaraActividades){
            getActivity().grantUriPermission(actividad.activityInfo.packageName, uri, Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        }

        startActivityForResult(capturarImagen, REQUEST_PHOTO);

    }



    // métodos para subir fotos

    public Observable<MensajeModel> subirImagenesObservable(){

        return Observable.create(new ObservableOnSubscribe<MensajeModel>() {
            @Override
            public void subscribe(@io.reactivex.annotations.NonNull ObservableEmitter<MensajeModel> emitter) throws Exception {
                emitter.onNext( new Cls_Subir_Foto(fileFoto,ObjFotoModel).ejecutarProceso());
                emitter.onComplete();
            }
        });
    }

    public  void ejecutarUpload(){

        delayedProgressDialog.show(getParentFragmentManager(), "foto");

          subirImagenesObservable()
                  .subscribeOn(Schedulers.io())
                  .observeOn(AndroidSchedulers.mainThread())
                  .subscribe(new Observer<MensajeModel>() {
                      @Override
                      public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                      }

                      @Override
                      public void onNext(@io.reactivex.annotations.NonNull MensajeModel mensajeModel) {

                          if(mensajeModel.getEstatus().equals("bien")){
                            eliminarFoto();
                            Util.mostarAlerta2(getActivity(),"Proceso Correcto");
                            obtenerListadoFotos(ordenTrabajoViewModel.getNo_Orden_Trabajo());
                          }else {
                              Util.mostarAlerta2(getActivity(),mensajeModel.getEstatus());
                          }
                      }

                      @Override
                      public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                          delayedProgressDialog.cancel();
                          Util.mostarAlerta2(getActivity(),"Error de Conectividad");
                      }

                      @Override
                      public void onComplete() {
                          delayedProgressDialog.cancel();
                      }
                  });
    }

    // peticiones


     public  void  obtenerListadoFotos(String pNoOrdenTrabajo){
         delayedProgressDialog.show(getParentFragmentManager(), "foto");

         FactoryPeticiones.getFactoryPeticiones()
                 .obtenerFotos(pNoOrdenTrabajo)
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe(new Observer<List<FotoOTModel>>() {
                     @Override
                     public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                     }

                     @Override
                     public void onNext(@io.reactivex.annotations.NonNull List<FotoOTModel> fotoOTModels) {
                          ListaFotoModel = fotoOTModels;
                          configurarAdapter();
                          if(fotoOTModels.size() == 0){
                              Util.mostarAlerta2(getActivity(),"No se encontro información");
                          }

                     }

                     @Override
                     public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                         delayedProgressDialog.cancel();
                         Util.mostarAlerta2(getActivity(),"Error de Conectividad");
                     }

                     @Override
                     public void onComplete() {
                         delayedProgressDialog.cancel();
                     }
                 });

     }

    // permisos

    private  void openSettings(){
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }


    public  void  obtenerPermisosCamara(){

        Dexter.withContext(getActivity())
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                         ejecutarProcesoCamara();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if(response.isPermanentlyDenied()){
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    // sobre escribrír clases

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if(resultCode != Activity.RESULT_OK){
            return;
        }

        if(requestCode == REQUEST_PHOTO){
            Uri uri = FileProvider.getUriForFile(getActivity(),"com.app.simapag.fugas.fileprovider",fileFoto);
            getActivity().revokeUriPermission(uri,Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            ejecutarUpload();

        }

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_foto, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.menu_item_tomar_foto:
                 obtenerPermisosCamara();
                return  true;
            case android.R.id.home:
                Intent intent = ActividadMenuOrdenes.nuevaInstancia(getContext());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    // clases anidadas

    public class ImagenOTAdapter extends RecyclerView.Adapter<ImageneOTHolder>{

        public  List<FotoOTModel> fotoOTModelList;

        public ImagenOTAdapter(List<FotoOTModel> fotoOTModelList) {
            this.fotoOTModelList = fotoOTModelList;
        }

        @NonNull
        @Override
        public ImageneOTHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_imagen_ot, parent, false);
            return new ImageneOTHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ImageneOTHolder holder, int position) {
               FotoOTModel foto = fotoOTModelList.get(position);
               holder.EnlazarDatos(foto);
        }

        @Override
        public int getItemCount() {
            return fotoOTModelList.size();
        }
    }

    public  class ImageneOTHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.text_view_imagenes_descripcion)
        TextView textViewDescripcion;
        @BindView(R.id.text_view_imagenes_rpu )
        TextView textViewRPU;
        @BindView(R.id.imagen_view_imagenes_ot)
        ImageView imageViewOT;

        FotoOTModel photoModel;

        public ImageneOTHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        public  void EnlazarDatos(FotoOTModel model){
             photoModel = model;

             textViewDescripcion.setText(model.getDescripcion());
             textViewRPU.setText(model.getRPU());

             if(model.getURL_Imagen().trim().length() >0){

                 Picasso mPicasso = Picasso.with(getContext());
                 mPicasso.setIndicatorsEnabled(true);
                 mPicasso.load(model.getURL_Imagen().trim())
                         .resize(200, 200)
                         .into(imageViewOT);
             }

             imageViewOT.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {

                     try {

                         Bitmap bitmap = ((BitmapDrawable) imageViewOT.getDrawable()).getBitmap();

                         if(bitmap != null) {
                             ByteArrayOutputStream stream = new ByteArrayOutputStream();
                             bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                             byte[] byteArray = stream.toByteArray();

                             DialogoVisualizarFoto dialogoVisualizarFoto = DialogoVisualizarFoto.nuevaInstancia(byteArray, ordenTrabajoViewModel.getNo_Orden_Trabajo());
                             dialogoVisualizarFoto.show(getParentFragmentManager(), "foto");

                         }else {
                             Util.mostarAlerta2(getActivity(), "No hay Foto que visualizar");
                         }

                     }catch (Exception ex){
                         Util.mostarAlerta2(getActivity(), "No hay Foto que visualizar");
                     }
                 }
             });
        }

        @Override
        public void onClick(View view) {

        }
    }

    // clase para subir informacion

    private class Cls_Subir_Foto{
        File fileFotito;
        FotoOTModel fotoOTModel;

        public Cls_Subir_Foto(File fileFotito, FotoOTModel fotoOTModel) {
            this.fileFotito = fileFotito;
            this.fotoOTModel = fotoOTModel;
        }


        public MensajeModel ejecutarProceso(){
            MensajeModel mensaje = new MensajeModel();
            String resultado = "";
            try {

                fotoOTModel.setNombre_Foto(fileFoto.getName());
                fotoOTModel.setCadena_Foto(obtenerImagenCadena(fileFotito.getName()));
                resultado = uploadImagen(fotoOTModel);
                mensaje.setEstatus(resultado);
                //Gson gson = new Gson();
               // String json = gson.toJson(fotoOTModel);
                //Log.d("DEBUG", json);

            }catch (Exception ex){
                mensaje.setEstatus(ex.getMessage());
            }

            return  mensaje;
        }

        public String uploadImagen(FotoOTModel model){
            String resultado = "mal";
            Call<MensajeModel> mensajeModelCall= FactoryPeticiones.getFactoryPeticiones().subirImagen(model);

            try {

                MensajeModel mensajeModel = mensajeModelCall.execute().body();
               resultado = mensajeModel.getEstatus();

            }catch (Exception ex){

            }
           return  resultado;
        }


        public  String obtenerImagenCadena(String nombreImagen){
            String respuesta = "";
            File file;

            file = Util.obtenerArchivoFoto(getContext(), nombreImagen);

            if(file.exists()){

                try {

                    Uri uri = FileProvider.getUriForFile(getActivity(),"com.app.simapag.fugas.fileprovider",file);
                    Bitmap bitmap = Util.getScaledBitmap(file.getPath(), 600,600);
                    Bitmap bitmapRotate = Util.rotateImageIfRequired(getActivity(), bitmap, uri);

                    //Bitmap bitmap = Util.escalarBitmap(file.getPath(),4);
                    respuesta = Util.getBase64String(Util.getScaledBitmap(bitmapRotate,600,600));
                    //respuesta = Util.getBase64String(bitmapRotate);

                }catch (Exception ex){
                    respuesta ="";
                }
            }

            return  respuesta;
        }
    }
}
