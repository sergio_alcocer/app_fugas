package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class LoginModel {

    @SerializedName("Usuario")
    private String Usuario;
    @SerializedName("Password")
    private  String Password;
    @SerializedName("Token")
    private  String Token;

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        Usuario = usuario;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }
}
