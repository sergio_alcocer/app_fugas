package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentoListadoVisitasInvestigacion;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActividadListadoVisitasInvestigacion extends SingleFragmentoActividad {


    @Override
    protected Fragment crearFragmento() {
        return FragmentoListadoVisitasInvestigacion.nuevaInstancia();
    }


    public static Intent nuevaInstancia(Context context){
        Intent intent = new Intent(context,ActividadListadoVisitasInvestigacion.class);
        return  intent;

    }

    @Override
    public void onBackPressed() {
        return;
    }
}
