package com.app.simapag.fugas.fragmentos;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadImagenesOT;
import com.app.simapag.fugas.actividades.ActividadInformacionOT;
import com.app.simapag.fugas.actividades.ActividadListadoOrdenes;
import com.app.simapag.fugas.actividades.ActividadListadoVisitas;
import com.app.simapag.fugas.actividades.ActividadMateriales;
import com.app.simapag.fugas.actividades.ActividadMenu;
import com.app.simapag.fugas.actividades.ActividadMenuOrdenes;
import com.app.simapag.fugas.actividades.ActividadTrabajoRealizado;
import com.app.simapag.fugas.actividades.ActividadUbicacion;
import com.app.simapag.fugas.actividades.ActvidadListadoMaterial;
import com.app.simapag.fugas.modelos.Cls_Menu;
import com.app.simapag.fugas.modelos.OrdenTrabajoViewModel;
import com.app.simapag.fugas.singlenton.RepositorioOT;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FragmentoMenuOrdenes extends Fragment {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;

    @BindView(R.id.recyclerview_menu)
    RecyclerView recyclerMenu;
    LinearLayoutManager linearLayoutManager;

    OrdenTrabajoViewModel ordenTrabajoViewModel;

    List<Cls_Menu> ListaMenu = new ArrayList<>();
    Cls_Menu ObjMenu;

    private boolean blnSepuedeRegresar = false;
    private Unbinder unbinder;

    public  static  FragmentoMenuOrdenes NuevaInstancia(){
        FragmentoMenuOrdenes fragmentoMenuOrdenes = new FragmentoMenuOrdenes();
        return fragmentoMenuOrdenes;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        hacerMenu();
        ordenTrabajoViewModel = RepositorioOT.get().getObjTrabajViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_menu, container, false);
        unbinder = ButterKnife.bind(this, view);

        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerMenu.setLayoutManager(linearLayoutManager);

        textViewTitulo.setText("Folio: " + ordenTrabajoViewModel.getFolio());

        configurarAdaptador();
        return  view;
    }


    // métodos

    public void configurarAdaptador(){

        if(isAdded()){
            recyclerMenu.setAdapter(new MenuAdapter(ListaMenu));
        }

    }

    public boolean isBlnSepuedeRegresar() {
        return blnSepuedeRegresar;
    }

    public  void  hacerMenu(){

        ObjMenu = new Cls_Menu();
        ObjMenu.setID(1);
        ObjMenu.setNombre("Información OT");
        ListaMenu.add(ObjMenu);

        ObjMenu = new Cls_Menu();
        ObjMenu.setID(2);
        ObjMenu.setNombre("Material Utilizado");
        ListaMenu.add(ObjMenu);

        ObjMenu = new Cls_Menu();
        ObjMenu.setID(3);
        ObjMenu.setNombre("Visitas");
        ListaMenu.add(ObjMenu);

        ObjMenu = new Cls_Menu();
        ObjMenu.setID(4);
        ObjMenu.setNombre("Ubicación del Trabajo");
        ListaMenu.add(ObjMenu);

        ObjMenu = new Cls_Menu();
        ObjMenu.setID(5);
        ObjMenu.setNombre("Agregar Fotografías");
        ListaMenu.add(ObjMenu);

        ObjMenu = new Cls_Menu();
        ObjMenu.setID(6);
        ObjMenu.setNombre("Trabajo Realizado");
        ListaMenu.add(ObjMenu);
    }


    // eventos

    // sobre escribir métodos

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent intent1 = ActividadListadoOrdenes.nuevaInstancia(getContext());
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                return  true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    // clases anidadas

    public  class  MenuAdapter extends RecyclerView.Adapter<MenuHolder>{

        List<Cls_Menu> listaMenu;

        public MenuAdapter(List<Cls_Menu> listaMenu) {
            this.listaMenu = listaMenu;
        }

        @NonNull
        @Override
        public MenuHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_menu, parent, false);
            return new MenuHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MenuHolder holder, int position) {
            Cls_Menu menu = listaMenu.get(position);
            holder.enlazarDatos(menu);
        }

        @Override
        public int getItemCount() {
            return listaMenu.size();
        }
    }

    public class MenuHolder extends  RecyclerView.ViewHolder  implements  View.OnClickListener{


        @BindView(R.id.image_view_icono)
        ImageView imageView;

        @BindView(R.id.text_view_nombre_menu)
        TextView textViewNombreMenu;

        Cls_Menu clsMenu;

        public MenuHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        public  void enlazarDatos(Cls_Menu model){
            clsMenu =model;
            textViewNombreMenu.setText(model.getNombre());

            switch (model.getID()){
                case 1:
                    imageView.setImageResource(R.drawable.ic_baseline_info_60);
                    break;
                case 2:
                    imageView.setImageResource(R.drawable.ic_baseline_handyman_60);
                    break;
                case 3:
                    imageView.setImageResource(R.drawable.ic_baseline_groups_60);
                    break;
                case 4:
                    imageView.setImageResource(R.drawable.ic_baseline_location_on_60);
                    break;
                case 5:
                    imageView.setImageResource(R.drawable.ic_baseline_image_60);
                    break;
                case 6:
                    imageView.setImageResource(R.drawable.ic_baseline_hardware_60);
                    break;

            }

        }

        @Override
        public void onClick(View view) {

             switch (clsMenu.getID()){

                 case 1:
                      Intent intInfo = ActividadInformacionOT.nuevaInstancia(getContext());
                      startActivity(intInfo);
                      break;
                 case 2:
                     Intent intMaterial = ActvidadListadoMaterial.nuevaInstancia(getContext());
                     startActivity(intMaterial);
                     break;
                 case 3:
                     Intent intVisitas = ActividadListadoVisitas.nuevaInstancia(getContext());
                     startActivity(intVisitas);
                     break;
                 case 4:
                     Intent intUbicacion = ActividadUbicacion.nuevaInstancia(getContext());
                     startActivity(intUbicacion);
                     break;
                 case 5:
                     Intent intFotos = ActividadImagenesOT.nuevaInstancia(getContext());
                     startActivity(intFotos);
                     break;
                 case 6:
                     Intent intTrabajoRealizado = ActividadTrabajoRealizado.nuevaInstancia(getContext());
                     startActivity(intTrabajoRealizado);
                     break;
             }

        }
    }
}
