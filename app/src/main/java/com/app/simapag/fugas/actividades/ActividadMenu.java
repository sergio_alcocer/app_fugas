package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.app.simapag.fugas.R;
import com.app.simapag.fugas.preferencias.Preferencias;
import com.google.android.material.navigation.NavigationView;

public class ActividadMenu extends AppCompatActivity {


    @BindView(R.id.mitoolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.text_view_nombre_usuario)
    TextView textViewNombreUsuario;

    public TextView textViewTituloBar;
    public  TextView textViewEncabezado;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.menu_principal);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        textViewTituloBar = (TextView) toolbar.findViewById(R.id.toolbar_titulo);

        textViewTituloBar.setText("Fugas");

        View header = navigationView.getHeaderView(0);
        textViewEncabezado = (TextView) header.findViewById(R.id.text_view_encabezado_menu_principal);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_clear_all_24);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        textViewNombreUsuario.setText(Preferencias.getNombreUsuario(getApplicationContext()));

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()){

                    case  R.id.nav_crear_orden_ot:
                        Intent intentOT = ActividadOrdenesTrabajo.nuevaInstancia(getApplicationContext());
                        startActivity(intentOT);
                        break;
                    case R.id.nav_asignar_orden_ot:
                        Intent intentAsignar = ActividadListadoOrdenesRegistradas.nuevaInstancia(getApplicationContext());
                        startActivity(intentAsignar);
                        break;
                    case  R.id.nav_ordenes_de_investigacion:
                         Intent intentOrdenesInvestigacion = ActividadOrdenesInvestigacion.nuevaInstancia(getApplicationContext());
                         startActivity(intentOrdenesInvestigacion);
                        break;
                    case  R.id.nav_ordenes_trabajo:
                        Intent intent1 = ActividadListadoOrdenes.nuevaInstancia(getApplicationContext());
                          startActivity(intent1);
                        break;
                    case R.id.nav_nuevas_cerrar:

                        borrarPreferencias();
                        Intent intent = ActividadLogin.nuevaInstancia(getApplicationContext());
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        break;
                }

                return false;
            }
        });

        drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return  true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        return;
    }

    // métodos
    public  static Intent nuevaInstancia(Context context){
        Intent intent = new Intent(context, ActividadMenu.class);
        return  intent;
    }

    public  void  borrarPreferencias(){
        Preferencias.setNombreRol(getApplicationContext(), "");
        Preferencias.setEmpleadoID(getApplicationContext(), "");
        Preferencias.setNombreUsuario(getApplicationContext(), "");
    }

}
