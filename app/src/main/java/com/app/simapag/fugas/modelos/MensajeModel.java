package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class MensajeModel {

    @SerializedName("Estatus")
     private String Estatus;
    @SerializedName("Dato1")
     private String Dato1;
    @SerializedName("Dato2")
     private  String Dato2;
    @SerializedName("Dato3")
     private  String Dato3;

    public String getEstatus() {
        return Estatus;
    }

    public void setEstatus(String estatus) {
        Estatus = estatus;
    }

    public String getDato1() {
        return Dato1;
    }

    public void setDato1(String dato1) {
        Dato1 = dato1;
    }

    public String getDato2() {
        return Dato2;
    }

    public void setDato2(String dato2) {
        Dato2 = dato2;
    }

    public String getDato3() {
        return Dato3;
    }

    public void setDato3(String dato3) {
        Dato3 = dato3;
    }
}
