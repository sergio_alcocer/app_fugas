package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentoListadoOrdenes;
import com.app.simapag.fugas.fragmentos.FragmentoListadoVisitas;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActividadListadoVisitas extends SingleFragmentoActividad {

    @Override
    protected Fragment crearFragmento() {
        return FragmentoListadoVisitas.nuevaInstancia();
    }

    public static Intent nuevaInstancia(Context context){
        Intent intent =  new Intent(context,ActividadListadoVisitas.class);
        return intent;
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
