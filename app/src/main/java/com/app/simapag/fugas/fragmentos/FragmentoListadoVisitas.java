package com.app.simapag.fugas.fragmentos;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadAgregarVisitas;
import com.app.simapag.fugas.actividades.ActividadListadoVisitas;
import com.app.simapag.fugas.actividades.ActividadMenuOrdenes;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.dialogos.DialogoPregunta;
import com.app.simapag.fugas.modelos.MensajeModel;
import com.app.simapag.fugas.modelos.OrdenTrabajoViewModel;
import com.app.simapag.fugas.modelos.VisitasViewModel;
import com.app.simapag.fugas.preferencias.Preferencias;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.singlenton.RepositorioOT;
import com.app.simapag.fugas.util.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FragmentoListadoVisitas extends Fragment implements DialogoPregunta.RespuestaListener {

    private  static final int REQUEST_CODIGO = 1;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;

    @BindView(R.id.text_view_titulo_pantalla)
    TextView textViewTituloPantalla;

    @BindView(R.id.recyclerview_visita)
    RecyclerView recyclerVisita;
    LinearLayoutManager linearLayoutManager;

    OrdenTrabajoViewModel ordenTrabajoViewModel;

    List<VisitasViewModel> ListaVisita = new ArrayList<>();
    DelayedProgressDialog delayedProgressDialog;
    private Unbinder unbinder;

    public  static  FragmentoListadoVisitas nuevaInstancia(){

        return  new FragmentoListadoVisitas();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);
        setHasOptionsMenu(true);
        ordenTrabajoViewModel = RepositorioOT.get().getObjTrabajViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_listado_visitas, container, false);
        unbinder = ButterKnife.bind(this, view);

        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerVisita.setLayoutManager(linearLayoutManager);

        textViewTitulo.setText("Folio: " + ordenTrabajoViewModel.getFolio());
        textViewTituloPantalla.setText("Listado Visitas");

        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        obtenerListadoVisitas(ordenTrabajoViewModel.getNo_Orden_Trabajo());
    }

    // métodos

    public  void  configurarAdapter(){
        if(isAdded()){
            recyclerVisita.setAdapter( new VisitasAdapter(ListaVisita));
        }
    }

    public  void  mostrarAlerta(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Proceso Correcto");
        builder.setTitle("Visitas");
        builder.setCancelable(false);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                obtenerListadoVisitas(ordenTrabajoViewModel.getNo_Orden_Trabajo());

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }
    // eventos

    // peticiones


    public  void  eliminarVisita(String pNoVisitaTrabajo , String pNombreUsuario){
        delayedProgressDialog.show(getParentFragmentManager(), "paginado");
        FactoryPeticiones.getFactoryPeticiones()
                .eliminarVisita(pNoVisitaTrabajo,pNombreUsuario)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<MensajeModel>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull MensajeModel mensajeModel) {

                        if(mensajeModel.getEstatus().equals("bien")){
                            mostrarAlerta();
                        }else {
                            Util.mostarAlerta2(getActivity(), " " + mensajeModel.getEstatus());
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });
    }

    public  void  obtenerListadoVisitas(String pNoOrdenTrabajo){

        delayedProgressDialog.show(getParentFragmentManager(), "paginado");

        FactoryPeticiones.getFactoryPeticiones()
                .obtenerVisita(pNoOrdenTrabajo)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<VisitasViewModel>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<VisitasViewModel> visitasViewModels) {
                        ListaVisita = visitasViewModels;
                        configurarAdapter();
                        if(visitasViewModels.size() == 0){
                            Util.mostarAlerta2(getActivity(), "No se encontró ninguna visita");
                        }


                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });
    }

    // sobre escribír métodos


    @Override
    public void aceptarRespuestaSI(String identificador) {

        eliminarVisita(identificador, Preferencias.getNombreUsuario(getContext()));
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_mas, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.menu_item_agregar:

                Intent intentNuevo = ActividadAgregarVisitas.nuevaInstancia(getContext());
                startActivity(intentNuevo);

                return  true;
            case android.R.id.home:
                Intent intent = ActividadMenuOrdenes.nuevaInstancia(getContext());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    // clases anidados

    public  class  VisitasAdapter extends  RecyclerView.Adapter<VisitasHolder>{

        List<VisitasViewModel> listaVisita;

        public VisitasAdapter(List<VisitasViewModel> listaVisita) {
            this.listaVisita = listaVisita;
        }

        @NonNull
        @Override
        public VisitasHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_visita, parent, false);
            return new VisitasHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull VisitasHolder holder, int position) {
              VisitasViewModel viewModel = listaVisita.get(position);
              holder.enlazarDatos(viewModel);

              holder.buttonEliminar.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                      String mensaje = "Visita con Fecha: " + viewModel.getFecha();
                        DialogoPregunta dialogoPregunta = DialogoPregunta.nuevaInstancia(viewModel.getNo_Visita_Trabajo(),mensaje, "Visitas");
                        dialogoPregunta.setTargetFragment(FragmentoListadoVisitas.this,REQUEST_CODIGO);
                        dialogoPregunta.show(getParentFragmentManager(),"codigo");
                  }
              });
        }

        @Override
        public int getItemCount() {
            return listaVisita.size();
        }
    }

    public class  VisitasHolder extends  RecyclerView.ViewHolder implements  View.OnClickListener{

        VisitasViewModel visitasViewModel;


        @BindView(R.id.text_view_visita_fecha)
        TextView textViewFecha;

        @BindView(R.id.text_view_visita_detalle)
        TextView textViewDetalle;

        @BindView(R.id.button_visita_eliminar)
        Button buttonEliminar;

        public VisitasHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        public  void enlazarDatos(VisitasViewModel model){
             visitasViewModel = model;
             textViewFecha.setText(model.getFecha());
             textViewDetalle.setText(model.getDetalles());
        }

        @Override
        public void onClick(View view) {

        }
    }
}
