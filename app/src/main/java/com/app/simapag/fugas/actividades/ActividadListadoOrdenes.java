package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentoListadoOrdenes;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActividadListadoOrdenes extends SingleFragmentoActividad {

    @Override
    protected Fragment crearFragmento() {
        return FragmentoListadoOrdenes.nuevaInstancia();
    }

    public  static Intent nuevaInstancia(Context context){
        Intent intent = new Intent(context, ActividadListadoOrdenes.class);
        return  intent;
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
