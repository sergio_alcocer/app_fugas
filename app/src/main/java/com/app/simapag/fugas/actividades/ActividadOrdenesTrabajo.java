package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentosOrdenesTrabajo;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActividadOrdenesTrabajo extends SingleFragmentoActividad {

    @Override
    protected Fragment crearFragmento() {
        return FragmentosOrdenesTrabajo.nuevaInstancia();
    }

    public static Intent nuevaInstancia(Context context){
        Intent intent = new Intent(context, ActividadOrdenesTrabajo.class);
        return intent;
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
