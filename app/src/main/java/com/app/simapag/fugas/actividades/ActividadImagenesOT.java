package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentoImagenesOT;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActividadImagenesOT extends SingleFragmentoActividad {

    @Override
    protected Fragment crearFragmento() {
        return FragmentoImagenesOT.nuevaInstancia();
    }


    public  static Intent nuevaInstancia(Context context){
        Intent intent = new Intent(context,ActividadImagenesOT.class);
        return  intent;
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
