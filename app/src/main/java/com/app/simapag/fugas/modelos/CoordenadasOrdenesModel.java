package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class CoordenadasOrdenesModel {

    @SerializedName("No_Orden_Trabajo")
    private  String No_Orden_Trabajo;
    @SerializedName("Longitud_Trabajo")
    private  double Longitud_Trabajo;
    @SerializedName("Latitud_Trabajo")
    private  double Latitud_Trabajo;

    public String getNo_Orden_Trabajo() {
        return No_Orden_Trabajo;
    }

    public void setNo_Orden_Trabajo(String no_Orden_Trabajo) {
        No_Orden_Trabajo = no_Orden_Trabajo;
    }

    public double getLongitud_Trabajo() {
        return Longitud_Trabajo;
    }

    public void setLongitud_Trabajo(double longitud_Trabajo) {
        Longitud_Trabajo = longitud_Trabajo;
    }

    public double getLatitud_Trabajo() {
        return Latitud_Trabajo;
    }

    public void setLatitud_Trabajo(double latitud_Trabajo) {
        Latitud_Trabajo = latitud_Trabajo;
    }
}
