package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentoAgregarVisitas;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActividadAgregarVisitas extends SingleFragmentoActividad {

    @Override
    protected Fragment crearFragmento() {
        return FragmentoAgregarVisitas.nuevaInstancia();
    }

    public  static Intent nuevaInstancia(Context context){
        Intent intent = new Intent(context,ActividadAgregarVisitas.class);
        return  intent;
    }

    @Override
    public void onBackPressed() {
        return;
    }

}
