package com.app.simapag.fugas.fragmentos;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.simapag.fugas.BuildConfig;
import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadAsignacionOT;
import com.app.simapag.fugas.actividades.ActividadMenu;
import com.app.simapag.fugas.adapter.AutoCompleteCombos;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.modelos.Cls_BusquedaOT_Model;
import com.app.simapag.fugas.modelos.Cls_Combo;
import com.app.simapag.fugas.modelos.OrdenTrabajoViewModel;
import com.app.simapag.fugas.modelos.OrdenesTrabajoPaginado;
import com.app.simapag.fugas.modelos.ParametrosBusquedaOT;
import com.app.simapag.fugas.preferencias.Preferencias;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.singlenton.RepositorioOT;
import com.app.simapag.fugas.util.DatePickerWrapper;
import com.app.simapag.fugas.util.Util;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FragmentoListadoOrdenesRegistradas extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;

    @BindView(R.id.image_view_microfono)
    ImageView micButton;

    @BindView(R.id.spinner_area)
    Spinner SpinnerArea;
    ArrayAdapter arrayAdapterArea;
    List<Cls_Combo> ListaAreas = new ArrayList<>();
    Cls_Combo itemSelectedArea;

    @BindView(R.id.recyclerview_ordenes_trabajo)
    RecyclerView recyclerOrdenesTrabajo;
    LinearLayoutManager linearLayoutManager;

    @BindView(R.id.autoComplete_actividad)
    AutoCompleteTextView autoCompleteTextActividad;

    @BindView(R.id.boton_borrar_autocompletado_actividad)
    Button buttonBorrarAutompleadoActividad;

    List<Cls_Combo> ListaActividad = new ArrayList<>();
    Cls_Combo itemSelectedActividad;

    List<Cls_Combo> ListaDistritosEmpleados = new ArrayList<>();
    Cls_Combo itemEmpleadoSelected;

    @BindView(R.id.text_input_edit_buscar_orden)
    EditText EditTextBuscarOrden;

    DelayedProgressDialog delayedProgressDialog;
    private Unbinder unbinder;

    @BindView(R.id.imagen_button_cerrar_busqueda)
    ImageButton imageButtonCerrarBusqueda;

    @BindView(R.id.linear_layout_busqueda)
    View viewBusqueda;

    @BindView(R.id.chk_fecha_inicio)
    CheckBox checkBoxFechaInicio;

    @BindView(R.id.chk_fecha_final)
    CheckBox checkBoxFechaFina;

    DatePickerWrapper datePickerWrapperFechaInicio;
    DatePickerWrapper datePickerWrapperFechaFinal;

    Boolean blnBanderVistaBusqueda = true;
    String Distrito_id = "";
    String Brigada_id = "";
    String Actividad_id = "";
    String Empleado_Asignado_id="";
    String Estatus = "";
    String Fecha_Inicio = "";
    String Fecha_Fin = "";
    SimpleDateFormat formatFecha = new SimpleDateFormat("yyyy-MM-dd");

    Cls_BusquedaOT_Model ObjBusquedaModel = new Cls_BusquedaOT_Model();
    List<OrdenTrabajoViewModel> ListaOrdenes = new ArrayList<>();
    String EmpleadoID;

    int mTotalElementos = 0;
    int mPagina;
    boolean loading = false;
    int totalItemCount;
    int lastFetchedPage = 1;
    String mQuery = "";
    private  boolean BanderaPermisoMicrofono = false;
    private SpeechRecognizer speechRecognizer;


    public  static  FragmentoListadoOrdenesRegistradas nuevaInstancia(){
        return  new FragmentoListadoOrdenesRegistradas();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);
        EmpleadoID = Preferencias.getEmpleadoID(getContext());
        ObjBusquedaModel.setEmpleado_ID(EmpleadoID);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_listado_ot_regisitradas, container, false);
        unbinder = ButterKnife.bind(this, view);

        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");

        textViewTitulo.setText("Ordenes de Trabajo");

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerOrdenesTrabajo.setLayoutManager(linearLayoutManager);

        datePickerWrapperFechaInicio = new DatePickerWrapper((TextView) view.findViewById(R.id.text_view_fecha_inicio),getActivity());
        datePickerWrapperFechaFinal = new DatePickerWrapper((TextView) view.findViewById(R.id.text_view_fecha_final),getActivity());
        micButton.setEnabled(false);

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getActivity());
        final Intent speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        speechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {

            }

            @Override
            public void onBeginningOfSpeech() {
                EditTextBuscarOrden.setText("");
                // EditTextBuscarOrden.setHint("Escuchando...");
            }

            @Override
            public void onRmsChanged(float v) {

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int i) {

            }

            @Override
            public void onResults(Bundle bundle) {
                micButton.setImageResource(R.drawable.ic_baseline_mic_off_24);
                ArrayList<String> data = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                String resultado = data.get(0).trim();
                resultado= resultado.replaceAll("\\s+","");
                EditTextBuscarOrden.setText(resultado);
            }

            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });

        micButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (BanderaPermisoMicrofono) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        speechRecognizer.stopListening();
                    }

                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        micButton.setImageResource(R.drawable.ic_baseline_mic_24);
                        speechRecognizer.startListening(speechRecognizerIntent);
                    }

                }
                return false;
            }
        });

        view.findViewById(R.id.linear_layout_principal).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                View focusedView = getActivity().getCurrentFocus();
                /*
                 * If no view is focused, an NPE will be thrown
                 *
                 * Maxim Dmitriev
                 */
                if (focusedView != null) {
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }


                return true;
            }
        });


        buttonBorrarAutompleadoActividad.setVisibility(View.GONE);
        autoCompleteTextActividad.setImeOptions(EditorInfo.IME_ACTION_DONE);
        autoCompleteTextActividad.setRawInputType(InputType.TYPE_CLASS_TEXT);

        autoCompleteTextActividad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                if(s.length() != 0) {
                    buttonBorrarAutompleadoActividad.setVisibility(View.VISIBLE);
                } else {
                    buttonBorrarAutompleadoActividad.setVisibility(View.GONE);
                    itemSelectedActividad = null;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        autoCompleteTextActividad.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                itemSelectedActividad = (Cls_Combo) adapterView.getItemAtPosition(i);
                buttonBorrarAutompleadoActividad.setVisibility(View.VISIBLE);
            }
        });

        recyclerOrdenesTrabajo.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();


                totalItemCount = linearLayoutManager.getItemCount();
                if (!loading) {

                    if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == totalItemCount - 1) {
                        loading = true;
                        if (totalItemCount < mTotalElementos) {
                            mPagina++;
                            obtenerOrdenesTrabajo(mQuery, mPagina, Distrito_id, Brigada_id, Actividad_id,Empleado_Asignado_id,Estatus,Fecha_Inicio,Fecha_Fin);
                        }
                    }
                }
            }
        });


        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        obtenerParametrosBuqueda();
    }

    // Métodos *************************************************************

    public  void  configurarAdapterAutocompletados(){

        if(isAdded()){
            autoCompleteTextActividad.setAdapter(new AutoCompleteCombos(getContext(), ListaActividad));
        }
    }

    public  void configurarAdapter(){

        if(isAdded()){
             recyclerOrdenesTrabajo.setAdapter( new BuscarOrdenesRegistrasAdapter(ListaOrdenes));
        }
    }

    public  void  configurarAdaptadorCombo(){

        if(isAdded()){


            arrayAdapterArea = new ArrayAdapter(getContext(), R.layout.item_spinner, ListaAreas);
            SpinnerArea.setAdapter(arrayAdapterArea);


            int index;

            // seleccionamos un elemento de la brigada
            index = Util.obtenerIndex("00010",ListaAreas);
            if(index != -1)
                SpinnerArea.setSelection(index);

        }
    }

    public  void  obtenerParametrosBuqueda(){

        delayedProgressDialog.show(getParentFragmentManager(), "paginado");
        FactoryPeticiones.getFactoryPeticiones()
                .ObtenerParametrosBusquedaOT(EmpleadoID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ParametrosBusquedaOT>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull ParametrosBusquedaOT parametrosBusquedaOT) {

                        ListaAreas = parametrosBusquedaOT.getListaBrigadas();
                        ListaActividad = parametrosBusquedaOT.getListaFallas();
                        ListaDistritosEmpleados = parametrosBusquedaOT.getListaDistritoEmpleados();
                        configurarAdaptadorCombo();
                        configurarAdapterAutocompletados();
                        obtenerPermisoMicrofono();
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });
    }

    public  void  cerrarVentanaBusqueda(){
        blnBanderVistaBusqueda = false;
        viewBusqueda.setVisibility(View.GONE);
        imageButtonCerrarBusqueda.setImageResource(R.drawable.ic_baseline_arrow_drop_up_12);
    }

    public  void  ejecutarBusqueda(){
        String query;
        query = EditTextBuscarOrden.getText().toString().trim();


        Distrito_id = "";

        Estatus = "REGISTRADA";


        itemSelectedArea = (Cls_Combo) SpinnerArea.getSelectedItem();

        if(itemSelectedArea.getID().equals("-1"))
            Brigada_id = "";
        else
            Brigada_id = itemSelectedArea.getID();


        if(itemSelectedActividad == null)
            Actividad_id = "";
        else
            Actividad_id = itemSelectedActividad.getID();

        if(itemEmpleadoSelected == null)
            Empleado_Asignado_id = "";
        else
            Empleado_Asignado_id = itemEmpleadoSelected.getID();

        if(checkBoxFechaInicio.isChecked())
            Fecha_Inicio = formatFecha.format(datePickerWrapperFechaInicio.getDate());
        else
            Fecha_Inicio = "";

        if(checkBoxFechaFina.isChecked())
            Fecha_Fin = formatFecha.format(datePickerWrapperFechaFinal.getDate());
        else
            Fecha_Fin = "";


        iniciarBusquedaOrdenTrabajo(query, Distrito_id, Brigada_id,Actividad_id, Empleado_Asignado_id,Estatus,Fecha_Inicio,Fecha_Fin);
    }

    public void  iniciarBusquedaOrdenTrabajo(String pQuery, String pDistritro_ID , String pBrigadaID, String pActivdiadID, String pEmpleado_AsingadoID, String pEstatus,String pFechaInicio, String pFechaFin){
        mQuery = pQuery;
        lastFetchedPage=1;
        mPagina = 1;
        loading = false;

        obtenerOrdenesTrabajo(mQuery,mPagina,pDistritro_ID,pBrigadaID,pActivdiadID,pEmpleado_AsingadoID, pEstatus, pFechaInicio,pFechaFin);
    }

    // peticiones ********************************************************

    public void  obtenerOrdenesTrabajo(String pQuery, int pPagina, String pDistritro_ID , String pBrigadaID, String pActivdiadID, String pEmpleadoAsignadoID, String pEstatus, String pFechaInicio, String pFechaFin){

        ObjBusquedaModel.setQuery(pQuery);
        ObjBusquedaModel.setDistrito_ID(pDistritro_ID);
        ObjBusquedaModel.setBrigada_ID(pBrigadaID);
        ObjBusquedaModel.setFalla_ID(pActivdiadID);
        ObjBusquedaModel.setPagina(pPagina);
        ObjBusquedaModel.setEmpleado_Asignado_ID(pEmpleadoAsignadoID);
        ObjBusquedaModel.setEstatus(pEstatus);
        ObjBusquedaModel.setFecha_Inicio(pFechaInicio);
        ObjBusquedaModel.setFecha_Fin(pFechaFin);

        Gson gson = new Gson();
        String json = gson.toJson(ObjBusquedaModel);
        Log.d("DEBUG",json);

        delayedProgressDialog.show(getParentFragmentManager(), "paginado");

        FactoryPeticiones.getFactoryPeticiones()
                .obtenerOrdenesTrabajo(ObjBusquedaModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<OrdenesTrabajoPaginado>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull OrdenesTrabajoPaginado ordenesTrabajoPaginado) {

                        if (lastFetchedPage == 1){
                            ListaOrdenes = ordenesTrabajoPaginado.getListaOrdenes();
                            mTotalElementos = ordenesTrabajoPaginado.getTotalElementos();
                            configurarAdapter();
                            if(ordenesTrabajoPaginado.getListaOrdenes().size() == 0){
                                Util.mostarAlerta2(getActivity(),"No se encontró información");
                            }

                        }else {
                            ListaOrdenes.addAll(ordenesTrabajoPaginado.getListaOrdenes());
                            mTotalElementos = ordenesTrabajoPaginado.getTotalElementos();
                            recyclerOrdenesTrabajo.getAdapter().notifyDataSetChanged();
                        }
                        lastFetchedPage ++;
                        loading = false;

                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        loading = true;
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });


    }


    // eventos *************************************************************

    @OnClick(R.id.boton_borrar_autocompletado_actividad)
    public void  onEventoBorrarAutompleadoFallas(){
        autoCompleteTextActividad.setText("");
        buttonBorrarAutompleadoActividad.setVisibility(View.GONE);
        itemSelectedActividad = null;
    }

    @OnClick(R.id.imagen_button_cerrar_busqueda)
    public void eventoCerrarBusqueda(){


        if(blnBanderVistaBusqueda){
            Util.hideKeyboard(getActivity());
            blnBanderVistaBusqueda = false;
            viewBusqueda.setVisibility(View.GONE);
            imageButtonCerrarBusqueda.setImageResource(R.drawable.ic_baseline_arrow_drop_up_12);
        }else {
            blnBanderVistaBusqueda = true;
            viewBusqueda.setVisibility(View.VISIBLE);
            imageButtonCerrarBusqueda.setImageResource(R.drawable.ic_baseline_arrow_drop_down_12);
        }
    }


    // sobre escribr métodos ***********************************************

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_buscar, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.menu_item_buscar:
                Util.hideKeyboard(getActivity());
                cerrarVentanaBusqueda();
                ejecutarBusqueda();
                return  true;

            case android.R.id.home:
                Intent intent1 = ActividadMenu.nuevaInstancia(getContext());
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        speechRecognizer.destroy();
        RepositorioOT.get().clearInstancia();
    }


    // para el microfono métodos y permisos

    private  void openSettings(){
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    private  void  obtenerPermisoMicrofono(){

        Dexter.withContext(getActivity())
                .withPermission(Manifest.permission.RECORD_AUDIO)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                        BanderaPermisoMicrofono = true;
                        micButton.setEnabled(true);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if(response.isPermanentlyDenied()){
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    // clases anidadas********************************************************************


    public  class  BuscarOrdenesRegistrasAdapter extends RecyclerView.Adapter<BuscarOrdenesRegistradasHolder>{

        List<OrdenTrabajoViewModel> listaOrdenes;

        public BuscarOrdenesRegistrasAdapter(List<OrdenTrabajoViewModel> listaOrdenes) {
            this.listaOrdenes = listaOrdenes;
        }

        @NonNull
        @Override
        public BuscarOrdenesRegistradasHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_ordenes_trabajo, parent, false);
            return new BuscarOrdenesRegistradasHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull BuscarOrdenesRegistradasHolder holder, int position) {
            OrdenTrabajoViewModel ordenTrabajoViewModel = listaOrdenes.get(position);
            holder.enlazarDatos(ordenTrabajoViewModel);
        }

        @Override
        public int getItemCount() {
            return listaOrdenes.size();
        }
    }

    public class  BuscarOrdenesRegistradasHolder  extends  RecyclerView.ViewHolder implements  View.OnClickListener{

        @BindView(R.id.text_view_ordenes_colonia)
        TextView textViewColonia;

        @BindView(R.id.text_view_ordenes_domicilio)
        TextView textViewDomicilio;

        @BindView(R.id.text_view_ordenes_falla)
        TextView textViewFalla;

        @BindView(R.id.text_view_ordenes_folio)
        TextView textViewFolio;

        @BindView(R.id.text_view_fecha_ordenes_registro)
        TextView textViewFechaRegistro;

        @BindView(R.id.text_view_ordenes_no_cuenta)
        TextView textViewCuenta;

        @BindView(R.id.text_view_ordenes_rpu)
        TextView textViewRPU;

        @BindView(R.id.text_view_ordenes_usuario)
        TextView textViewUsuario;

        @BindView(R.id.text_view_ordenes_estatus)
        TextView textViewEstatus;

        @BindView(R.id.layout_datos_ordenes_cuenta)
        View viewDatosCuenta;



        OrdenTrabajoViewModel ordenTrabajoViewModel;

        public BuscarOrdenesRegistradasHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        public  void  enlazarDatos(OrdenTrabajoViewModel model){
            ordenTrabajoViewModel = model;

            textViewFolio.setText("Folio: " + model.getFolio());
            textViewFechaRegistro.setText(model.getFecha_Elaboro());
            textViewFalla.setText(model.getFalla());
            textViewColonia.setText(model.getColonia());
            textViewDomicilio.setText(model.getDomicilio());
            textViewEstatus.setText(model.getEstatus());

            if(model.getRPU().trim().length() == 0){
                viewDatosCuenta.setVisibility(View.GONE);
            } else {
                viewDatosCuenta.setVisibility(View.VISIBLE);
            }

            textViewCuenta.setText("No.Cuenta: " + model.getNo_Cuenta());
            textViewRPU.setText("RPU:" + model.getRPU());
            textViewUsuario.setText(model.getUsuario());



        }

        @Override
        public void onClick(View view) {

            RepositorioOT.get()
                    .setObjTrabajViewModel(ordenTrabajoViewModel);

            Intent intent = ActividadAsignacionOT.nuevaInstancia(view.getContext());
            view.getContext().startActivity(intent);
        }
    }
}
