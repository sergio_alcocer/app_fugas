package com.app.simapag.fugas.util;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DatePickerWrapper implements View.OnClickListener, View.OnFocusChangeListener, DatePickerDialog.OnDateSetListener {

    private SimpleDateFormat mDateFormat =  new SimpleDateFormat("dd/MM/yyyy");
    private TextView mTextViewDisplay;
    private  DatePickerDialog mDialog = null;
    private Date mDateCurrentDate = null;
    private  Activity mActivity;

    public DatePickerWrapper(TextView textViewDisplay, Activity activity) {
        mTextViewDisplay = textViewDisplay;
        mTextViewDisplay.setFocusable(true);
        mTextViewDisplay.setClickable(true);
        mTextViewDisplay.setOnClickListener(this);
        mTextViewDisplay.setOnFocusChangeListener(this);
        mActivity = activity;
        this.setDate(new Date());

    }

    @Override
    public void onClick(View view) {
        openDatePickerDialog();
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if(b){
            Util.hideKeyboard(mActivity);
            openDatePickerDialog();
        }
    }



    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {

        Calendar calendar = new GregorianCalendar(year, month, dayOfMonth);

        setDate(calendar.getTime());

    }


    public  void  setDate(Date date){

        if(date == null){
            throw new IllegalArgumentException("date may not be null");
        }

        this.mDateCurrentDate = (Date) date.clone();
        this.mTextViewDisplay.setText(mDateFormat.format(mDateCurrentDate));


        if(this.mDialog != null){
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(mDateCurrentDate);

            this.mDialog.updateDate( calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        }
    }

    public  Date getDate(){
        return mDateCurrentDate;
    }


    public  void  openDatePickerDialog(){

        if(mDialog == null){
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(getDate());
            mDialog = new DatePickerDialog(
                    mTextViewDisplay.getContext(),
                    this,
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)

            );
        }
        mDialog.show();

    }

    public void  bloquearCajas(){
        mTextViewDisplay.setEnabled(false);
    }

}
