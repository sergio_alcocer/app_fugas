package com.app.simapag.fugas.fragmentos;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.simapag.fugas.BuildConfig;
import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadListadoVisitasInvestigacion;
import com.app.simapag.fugas.actividades.ActividadMenu;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.modelos.Cls_Busqueda_Orden_Investigacion;
import com.app.simapag.fugas.modelos.Cls_Combo;
import com.app.simapag.fugas.modelos.OrdenInvestigacionPaginado;
import com.app.simapag.fugas.modelos.OrdenesInvestigacionViewModel;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.singlenton.RepositorioOT;
import com.app.simapag.fugas.singlenton.RepositorioVisita;
import com.app.simapag.fugas.util.DatePickerWrapper;
import com.app.simapag.fugas.util.Util;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FragmentoOrdenesInvestigacion extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;


    @BindView(R.id.text_input_edit_buscar_orden_investigacion)
    EditText EditTextBuscarOrden;

    @BindView(R.id.imagen_button_cerrar_busqueda)
    ImageButton imageButtonCerrarBusqueda;

    @BindView(R.id.image_view_microfono)
    ImageView micButton;


    @BindView(R.id.linear_layout_busqueda)
    View viewBusqueda;


    @BindView(R.id.spinner_estatus_orden_investigacion)
    Spinner SpinnerEstatus;
    ArrayAdapter arrayAdapterEstatus;
    List<Cls_Combo> ListaEstatus = new ArrayList<>();
    Cls_Combo itemSelectedEstatus;

    @BindView(R.id.spinner_aprobada_orden_investigacion)
    Spinner SpinnerAprobada;
    ArrayAdapter arrayAdapterAprobada;
    List<Cls_Combo> ListaAprobada = new ArrayList<>();
    Cls_Combo itemSelectedAprobada;

    Boolean blnBanderVistaBusqueda = true;

    @BindView(R.id.chk_fecha_inicio)
    CheckBox checkBoxFechaInicio;

    @BindView(R.id.chk_fecha_final)
    CheckBox checkBoxFechaFina;

    private  boolean BanderaPermisoMicrofono = false;
    private SpeechRecognizer speechRecognizer;

    DelayedProgressDialog delayedProgressDialog;
    private Unbinder unbinder;

    @BindView(R.id.recyclerview_ordenes_investigacion)
    RecyclerView recyclerInvestigacion;
    LinearLayoutManager linearLayoutManager;

    int mTotalElementos = 0;
    int mPagina;
    boolean loading = false;
    int totalItemCount;
    int lastFetchedPage = 1;
    String mQuery = "";
    String Estatus = "";
    String Apribado = "";
    String Fecha_Inicio = "";
    String Fecha_Fin = "";
    SimpleDateFormat formatFecha = new SimpleDateFormat("yyyy-MM-dd");
    List<OrdenesInvestigacionViewModel> ListaOrdenes = new ArrayList<>();

    Cls_Busqueda_Orden_Investigacion cls_busqueda_orden_investigacion = new Cls_Busqueda_Orden_Investigacion();
    DatePickerWrapper datePickerWrapperFechaInicio;
    DatePickerWrapper datePickerWrapperFechaFinal;


    public static  FragmentoOrdenesInvestigacion nuevaInstancia(){

        return new FragmentoOrdenesInvestigacion();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);
        ListaEstatus = Util.Obtener_Estatus_Orden_Investigacion();
        ListaAprobada = Util.Obtener_Item_Aprobacion_Orden_Investigacion();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_ordenes_investigacion, container, false);
        unbinder = ButterKnife.bind(this, view);

        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");

        textViewTitulo.setText("Ordenes de Investigación");

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerInvestigacion.setLayoutManager(linearLayoutManager);

        datePickerWrapperFechaInicio = new DatePickerWrapper((TextView) view.findViewById(R.id.text_view_fecha_inicio),getActivity());
        datePickerWrapperFechaFinal = new DatePickerWrapper((TextView) view.findViewById(R.id.text_view_fecha_final),getActivity());
        micButton.setEnabled(false);

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getActivity());
        final Intent speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());


        speechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {

            }

            @Override
            public void onBeginningOfSpeech() {
                EditTextBuscarOrden.setText("");

            }

            @Override
            public void onRmsChanged(float v) {

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int i) {

            }

            @Override
            public void onResults(Bundle bundle) {
                micButton.setImageResource(R.drawable.ic_baseline_mic_off_24);
                ArrayList<String> data = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                String resultado = data.get(0).trim();
                resultado= resultado.replaceAll("\\s+","");
                EditTextBuscarOrden.setText(resultado);
            }

            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });

        micButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (BanderaPermisoMicrofono) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        speechRecognizer.stopListening();
                    }

                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        micButton.setImageResource(R.drawable.ic_baseline_mic_24);
                        speechRecognizer.startListening(speechRecognizerIntent);
                    }

                }
                return false;
            }
        });

        view.findViewById(R.id.linear_layout_principal).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                View focusedView = getActivity().getCurrentFocus();
                /*
                 * If no view is focused, an NPE will be thrown
                 *
                 * Maxim Dmitriev
                 */
                if (focusedView != null) {
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }


                return true;
            }
        });

        recyclerInvestigacion.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();


                totalItemCount = linearLayoutManager.getItemCount();
                if (!loading) {

                    if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == totalItemCount - 1) {
                        loading = true;
                        if (totalItemCount < mTotalElementos) {
                            mPagina++;
                            obtenerOrdenesInvestigacion(mQuery,Estatus,Apribado,Fecha_Inicio,Fecha_Fin, mPagina);
                        }
                    }
                }
            }
        });


        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        llenarCombos();
        obtenerPermisoMicrofono();
    }

    // métodos *****************************

    public void configurarAdapter(){

        if(isAdded()){
             recyclerInvestigacion.setAdapter(new BuscarOrdenesInvestegacionAdaptar(ListaOrdenes));
        }
    }

    public void  llenarCombos(){

        if(isAdded()){
            arrayAdapterEstatus = new ArrayAdapter(getContext(),R.layout.item_spinner,ListaEstatus);
            SpinnerEstatus.setAdapter(arrayAdapterEstatus);

            arrayAdapterAprobada = new ArrayAdapter(getContext(), R.layout.item_spinner, ListaAprobada);
            SpinnerAprobada.setAdapter(arrayAdapterAprobada);
        }
    }

    public  void  cerrarVentanaBusqueda(){
        blnBanderVistaBusqueda = false;
        viewBusqueda.setVisibility(View.GONE);
        imageButtonCerrarBusqueda.setImageResource(R.drawable.ic_baseline_arrow_drop_up_12);
    }

    public void  ejecutarBusqueda(){
        String query;

        query = EditTextBuscarOrden.getText().toString().trim();

        itemSelectedEstatus = (Cls_Combo) SpinnerEstatus.getSelectedItem();

        if(itemSelectedEstatus.getID().equals("-1"))
            Estatus = "";
        else
            Estatus = itemSelectedEstatus.getID();

        itemSelectedAprobada = (Cls_Combo) SpinnerAprobada.getSelectedItem();

        if(itemSelectedAprobada.getID().equals("-1"))
            Apribado = "";
        else
            Apribado = itemSelectedAprobada.getID();

        if(checkBoxFechaInicio.isChecked())
            Fecha_Inicio = formatFecha.format(datePickerWrapperFechaInicio.getDate());
        else
            Fecha_Inicio = "";

        if(checkBoxFechaFina.isChecked())
            Fecha_Fin = formatFecha.format(datePickerWrapperFechaFinal.getDate());
        else
            Fecha_Fin = "";

        iniciarBusquedaOrdenesInvestigacion(query,Estatus,Apribado,Fecha_Inicio,Fecha_Fin);
    }

    public void  iniciarBusquedaOrdenesInvestigacion(String pQuery, String pEstatus, String pAprobado,String pFechaInicio, String pFechaFin){
        mQuery = pQuery;
        lastFetchedPage=1;
        mPagina = 1;
        loading = false;

        obtenerOrdenesInvestigacion(pQuery,pEstatus,pAprobado,pFechaInicio,pFechaFin,mPagina);
    }

    public  void obtenerOrdenesInvestigacion(String pQuery, String pEstatus, String pAprobado,String pFechaInicio, String pFechaFin, int pPagina){

        cls_busqueda_orden_investigacion.setAprobada(pAprobado);
        cls_busqueda_orden_investigacion.setEstatus(pEstatus);
        cls_busqueda_orden_investigacion.setFecha_Fin(pFechaFin);
        cls_busqueda_orden_investigacion.setFecha_Inicio(pFechaInicio);
        cls_busqueda_orden_investigacion.setRPU(pQuery);
        cls_busqueda_orden_investigacion.setPagina(pPagina);

        delayedProgressDialog.show(getParentFragmentManager(), "paginado");

        FactoryPeticiones.getFactoryPeticiones()
                .obtenerOrdenInvestigacion(cls_busqueda_orden_investigacion)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<OrdenInvestigacionPaginado>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull OrdenInvestigacionPaginado ordenInvestigacionPaginado) {

                        if(lastFetchedPage == 1){
                             ListaOrdenes = ordenInvestigacionPaginado.getListaOrdenesInvestigacion();
                             mTotalElementos = ordenInvestigacionPaginado.getTotalElementos();
                             configurarAdapter();
                             if(ordenInvestigacionPaginado.getListaOrdenesInvestigacion().size() == 0){
                                 Util.mostarAlerta2(getActivity(),"No se encontró información");
                             }

                        }else {
                             ListaOrdenes.addAll(ordenInvestigacionPaginado.getListaOrdenesInvestigacion());
                             mTotalElementos = ordenInvestigacionPaginado.getTotalElementos();
                             recyclerInvestigacion.getAdapter().notifyDataSetChanged();
                        }

                        lastFetchedPage ++;
                        loading = false;
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        loading = true;
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });
    }

    // eventos ******************************

    @OnClick(R.id.imagen_button_cerrar_busqueda)
    public void eventoCerrarBusqueda(){

        if(blnBanderVistaBusqueda){
            Util.hideKeyboard(getActivity());
            blnBanderVistaBusqueda = false;
            viewBusqueda.setVisibility(View.GONE);
            imageButtonCerrarBusqueda.setImageResource(R.drawable.ic_baseline_arrow_drop_up_12);
        }else {
            blnBanderVistaBusqueda = true;
            viewBusqueda.setVisibility(View.VISIBLE);
            imageButtonCerrarBusqueda.setImageResource(R.drawable.ic_baseline_arrow_drop_down_12);
        }
    }



    // peticiones ***************************


    //  permiso para el microfono

    private  void openSettings(){
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    private  void  obtenerPermisoMicrofono(){

        Dexter.withContext(getActivity())
                .withPermission(Manifest.permission.RECORD_AUDIO)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                        BanderaPermisoMicrofono = true;
                        micButton.setEnabled(true);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if(response.isPermanentlyDenied()){
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    // sobre escribir métodos *******************************

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_buscar, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.menu_item_buscar:
                Util.hideKeyboard(getActivity());
                cerrarVentanaBusqueda();
                ejecutarBusqueda();
                return  true;

            case android.R.id.home:
                Intent intent1 = ActividadMenu.nuevaInstancia(getContext());
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        speechRecognizer.destroy();
        RepositorioVisita.get().clearInstancia();

    }

    // clases anidadas ****************************************



    public class BuscarOrdenesInvestegacionAdaptar extends RecyclerView.Adapter<BuscarOrdenesInvestigacionHolder>{

        List<OrdenesInvestigacionViewModel> listaOrdenes;

        public BuscarOrdenesInvestegacionAdaptar(List<OrdenesInvestigacionViewModel> listaOrdenes) {
            this.listaOrdenes = listaOrdenes;
        }

        @NonNull
        @Override
        public BuscarOrdenesInvestigacionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_orden_investigacion, parent, false);
            return new BuscarOrdenesInvestigacionHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull BuscarOrdenesInvestigacionHolder holder, int position) {
            OrdenesInvestigacionViewModel ordenesInvestigacionViewModel = listaOrdenes.get(position);
            holder.enlzarDatos(ordenesInvestigacionViewModel);
        }

        @Override
        public int getItemCount() {
            return listaOrdenes.size();
        }
    }


    public class BuscarOrdenesInvestigacionHolder  extends RecyclerView.ViewHolder implements  View.OnClickListener{

        @BindView(R.id.text_view_investigacion_actividad)
        TextView textViewActivida;
        @BindView(R.id.text_view_investigacion_aprobada)
        TextView textViewAprobada;
        @BindView(R.id.text_view_investigacion_colonia)
        TextView textViewColonia;
        @BindView(R.id.text_view_investigacion_comentario)
        TextView textViewComentario;
        @BindView(R.id.text_view_investigacion_domicilio)
        TextView textViewDomicilio;
        @BindView(R.id.text_view_investigacion_estatus)
        TextView textViewEstatus;
        @BindView(R.id.text_view_investigacion_rpu)
        TextView textViewRPU;
        @BindView(R.id.text_view_investigacion_tarifa)
        TextView textViewTarifa;
        @BindView(R.id.text_view_investigacion_ultima_visita)
        TextView textViewUltimaVisita;
        @BindView(R.id.text_view_investigacion_usuario)
        TextView textViewUsuario;
        @BindView(R.id.text_view_fecha_investigacion_fecha)
        TextView textViewFecha;



        OrdenesInvestigacionViewModel ordenesInvestigacion;

        public BuscarOrdenesInvestigacionHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        public void  enlzarDatos(OrdenesInvestigacionViewModel ordenesInvestigacionViewModel){
            ordenesInvestigacion = ordenesInvestigacionViewModel;
            textViewActivida.setText(ordenesInvestigacionViewModel.getActividad());
            textViewAprobada.setText("Aprobada: " + ordenesInvestigacionViewModel.getAutorizada());
            textViewColonia.setText(ordenesInvestigacionViewModel.getColonia());

            String domicilio = ordenesInvestigacionViewModel.getCalle();

            if(ordenesInvestigacionViewModel.getNo_exterior().length() > 0)
                domicilio += " No. Ext: " + ordenesInvestigacionViewModel.getNo_exterior();

            if(ordenesInvestigacionViewModel.getNo_interior().length() > 0)
                domicilio += " No. Int: " + ordenesInvestigacionViewModel.getNo_interior();

            textViewEstatus.setText(ordenesInvestigacionViewModel.getEstatus());
            textViewRPU.setText("RPU: " + ordenesInvestigacionViewModel.getRpu());
            textViewUltimaVisita.setText(ordenesInvestigacionViewModel.getUltima_visita());
            textViewComentario.setText(ordenesInvestigacionViewModel.getComentario());
            textViewTarifa.setText(ordenesInvestigacionViewModel.getTarifa());
            textViewFecha.setText("Fecha: " +ordenesInvestigacionViewModel.getFecha_solicito());
            textViewUsuario.setText(ordenesInvestigacionViewModel.getUsuario());
            textViewDomicilio.setText(domicilio);
        }


        @Override
        public void onClick(View view) {

            RepositorioVisita.get()
                    .setOrdenesInvestigacionViewModel(ordenesInvestigacion);

            Intent intent = ActividadListadoVisitasInvestigacion.nuevaInstancia(view.getContext());
            view.getContext().startActivity(intent);
        }
    }

}
