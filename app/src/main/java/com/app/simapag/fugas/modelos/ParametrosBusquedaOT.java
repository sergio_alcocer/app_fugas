package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ParametrosBusquedaOT {

    @SerializedName("ListaBrigadas")
    private List<Cls_Combo> ListaBrigadas;
    @SerializedName("ListaDistrito")
    private List<Cls_Combo> ListaDistrito;
    @SerializedName("ListaFallas")
    private List<Cls_Combo> ListaFallas;
    @SerializedName("ListaDistritoEmpleados")
    private  List<Cls_Combo> ListaDistritoEmpleados;
    @SerializedName("ListaEstatus")
    private  List<Cls_Combo> ListaEstatus;

    public List<Cls_Combo> getListaBrigadas() {
        return ListaBrigadas;
    }

    public void setListaBrigadas(List<Cls_Combo> listaBrigadas) {
        ListaBrigadas = listaBrigadas;
    }

    public List<Cls_Combo> getListaDistrito() {
        return ListaDistrito;
    }

    public void setListaDistrito(List<Cls_Combo> listaDistrito) {
        ListaDistrito = listaDistrito;
    }

    public List<Cls_Combo> getListaFallas() {
        return ListaFallas;
    }

    public void setListaFallas(List<Cls_Combo> listaFallas) {
        ListaFallas = listaFallas;
    }

    public List<Cls_Combo> getListaDistritoEmpleados() {
        return ListaDistritoEmpleados;
    }

    public void setListaDistritoEmpleados(List<Cls_Combo> listaDistritoEmpleados) {
        ListaDistritoEmpleados = listaDistritoEmpleados;
    }

    public List<Cls_Combo> getListaEstatus() {
        return ListaEstatus;
    }

    public void setListaEstatus(List<Cls_Combo> listaEstatus) {
        ListaEstatus = listaEstatus;
    }
}
