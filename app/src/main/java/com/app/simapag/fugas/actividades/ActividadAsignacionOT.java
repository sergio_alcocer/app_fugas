package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentoAsignacionOT;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActividadAsignacionOT extends SingleFragmentoActividad {

    @Override
    protected Fragment crearFragmento() {
        return FragmentoAsignacionOT.nuevaInstancia();
    }

    public static Intent nuevaInstancia(Context context){
        Intent intent = new Intent(context,ActividadAsignacionOT.class);
        return  intent;
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
