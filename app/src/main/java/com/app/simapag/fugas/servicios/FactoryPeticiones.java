package com.app.simapag.fugas.servicios;

import com.app.simapag.fugas.modelos.BusquedaPredioModel;
import com.app.simapag.fugas.modelos.Cls_BusquedaOT_Model;
import com.app.simapag.fugas.modelos.Cls_Busqueda_Orden_Investigacion;
import com.app.simapag.fugas.modelos.Cls_Combo;
import com.app.simapag.fugas.modelos.CoordenadasOrdenesModel;
import com.app.simapag.fugas.modelos.FotoOTModel;
import com.app.simapag.fugas.modelos.LoginModel;
import com.app.simapag.fugas.modelos.MaterialesViewModel;
import com.app.simapag.fugas.modelos.MensajeModel;
import com.app.simapag.fugas.modelos.OrdenCreateModel;
import com.app.simapag.fugas.modelos.OrdenInvestigacionPaginado;
import com.app.simapag.fugas.modelos.OrdenOTUpdate;
import com.app.simapag.fugas.modelos.OrdenesMaterialViewModel;
import com.app.simapag.fugas.modelos.OrdenesTrabajoPaginado;
import com.app.simapag.fugas.modelos.OrderAsingadaModel;
import com.app.simapag.fugas.modelos.ParametroCrearOT;
import com.app.simapag.fugas.modelos.ParametroTrabajoRealizado;
import com.app.simapag.fugas.modelos.ParametrosBusquedaOT;
import com.app.simapag.fugas.modelos.ParametrosVisitasInvestigacion;
import com.app.simapag.fugas.modelos.PrediosPaginado;
import com.app.simapag.fugas.modelos.PrediosViewModel;
import com.app.simapag.fugas.modelos.VisitaInvestigacionModel;
import com.app.simapag.fugas.modelos.VisitasViewModel;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FactoryPeticiones {

    //http://192.168.1.69/wsfuga/swagger/index.html
   // public  static  String BASE_URL = "http://192.168.1.70/wsfuga/api/";
    public  static  String BASE_URL = "http://192.168.1.69/wsfuga/api/";
   //public  static  String BASE_URL = "http://187.174.176.232:8081/wsfugas/api/";
    private  static  FactoryPeticiones sFactoryPeticiones;
    private  InterfacePeticiones interfacePeticiones;

    public FactoryPeticiones(){

        final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(3, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2,TimeUnit.MINUTES)
                .build();

        final Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        interfacePeticiones = retrofit.create(InterfacePeticiones.class);
    }

    public  static  FactoryPeticiones getFactoryPeticiones(){

        if (sFactoryPeticiones == null){
            sFactoryPeticiones = new FactoryPeticiones();
        }

        return  sFactoryPeticiones;
    }

    public Observable<MensajeModel> verificarUsuario(LoginModel loginModel){
         return  interfacePeticiones.verificarUsuario(loginModel);
    }

    public  Observable<OrdenesTrabajoPaginado> obtenerOrdenesTrabajo(Cls_BusquedaOT_Model ModelBusqueda){
        return  interfacePeticiones.obtenerOrdenesTrabajo(ModelBusqueda);
    }

    public Observable<ParametrosBusquedaOT> ObtenerParametrosBusquedaOT(String empleadoID){
        return  interfacePeticiones.ObtenerParametrosBusquedaOT(empleadoID);
    }

    public  Observable<List<Cls_Combo>> ObtenerBrigadasPorDistritos(String distrito_id){
        return  interfacePeticiones.ObtenerBrigadasPorDistritos(distrito_id);
    }

    public   Observable<MensajeModel> agregarVisita(VisitasViewModel visitasViewModel){
        return  interfacePeticiones.agregarVisita(visitasViewModel);
    }

    public  Observable<MensajeModel> eliminarVisita(String pNoVisitaTrabajo,String pUsuario){
        return  interfacePeticiones.eliminarVisita(pNoVisitaTrabajo, pUsuario);
    }

    public   Observable<List<VisitasViewModel>> obtenerVisita(String pNoOrdenTrabajo){
        return  interfacePeticiones.obtenerVisita(pNoOrdenTrabajo);
    }

    public  Observable<List<MaterialesViewModel>> obtenerMateriales(){
        return interfacePeticiones.obtenerMateriales();
    }


    public  Observable<List<OrdenesMaterialViewModel>> obtenerMaterialOrdenes(String NoOrdenTrabajo){
        return  interfacePeticiones.obtenerMaterialOrdenes(NoOrdenTrabajo);
    }

    public  Observable<MensajeModel> agregarMaterialOrden(OrdenesMaterialViewModel ordenesMaterialViewModel){
        return  interfacePeticiones.agregarMaterialOrden(ordenesMaterialViewModel);
    }

    public  Observable<MensajeModel> eliminarMaterialOrden(String Ordenes_Trabajo_Detalles_Materiales_ID){
        return  interfacePeticiones.eliminarMaterialOrden(Ordenes_Trabajo_Detalles_Materiales_ID);
    }

    public Observable<MensajeModel>  actualizarCoordenadas( CoordenadasOrdenesModel coordenadasOrdenesModel){
        return  interfacePeticiones.actualizarCoordenadas(coordenadasOrdenesModel);
    }

    public  Observable<CoordenadasOrdenesModel> obtenerCoordenadasTrabajo(String No_Orden_Trabajo){
        return  interfacePeticiones.obtenerCoordenadasTrabajo(No_Orden_Trabajo);
    }

    public  Observable<ParametroTrabajoRealizado> obtenerParametrosTrabajoRealizado(String Empleado_ID,String No_Orden_Trabajo){
        return  interfacePeticiones.obtenerParametrosTrabajoRealizado(Empleado_ID, No_Orden_Trabajo);
    }

    public  Observable< List<Cls_Combo>> ObtenerEmpleadosBrigada(String Brigada_ID){
        return  interfacePeticiones.ObtenerEmpleadosBrigada(Brigada_ID);
    }

    public  Observable<MensajeModel> modificarOrdenTrabajo(OrdenOTUpdate ordenOTUpdate){
        return  interfacePeticiones.modificarOrdenTrabajo(ordenOTUpdate);
    }

    public Call<MensajeModel> subirImagen(FotoOTModel fotoOTModel){
         return  interfacePeticiones.subirImagen(fotoOTModel);
    }

    public  Observable<List<FotoOTModel>> obtenerFotos(String No_Orden_Trabajo){
        return  interfacePeticiones.obtenerFotos(No_Orden_Trabajo);
    }

    public Observable<ParametroCrearOT> obtenerParametrosCrearOT(){
        return interfacePeticiones.obtenerParametrosCrearOT();
    }

    public  Observable<List<Cls_Combo>> obtenerCallesPorColonia(String Colonia_ID){
        return  interfacePeticiones.obtenerCallesPorColonia(Colonia_ID);
    }

    public  Observable<PrediosPaginado> obtenerPredios(BusquedaPredioModel busquedaPredioModel){
        return  interfacePeticiones.obtenerPredios(busquedaPredioModel);
    }

    public Observable<MensajeModel> guardarOrdenTrabajo(OrdenCreateModel ordenCreateModel){
        return  interfacePeticiones.guardarOrdenTrabajo(ordenCreateModel);
    }

    public  Observable<MensajeModel> asignarOrdenesTrabajo(List<OrderAsingadaModel> listaAsignacion){
       return  interfacePeticiones.asignarOrdenesTrabajo(listaAsignacion);
    }

    public  Observable<OrdenInvestigacionPaginado> obtenerOrdenInvestigacion(Cls_Busqueda_Orden_Investigacion cls_busqueda_orden_investigacion){
        return  interfacePeticiones.obtenerOrdenInvestigacion(cls_busqueda_orden_investigacion);
    }

    public Observable<List<VisitaInvestigacionModel>> ObtenerVisitasOrdenInvestigacion(String No_Solicitud){
        return  interfacePeticiones.ObtenerVisitasOrdenInvestigacion(No_Solicitud);
    }

   public   Observable<ParametrosVisitasInvestigacion> ObtenerParametrosVisitasInvestigacion(String No_Solicitud){
        return  interfacePeticiones.ObtenerParametrosVisitasInvestigacion(No_Solicitud);
   }
}
