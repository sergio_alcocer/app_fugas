package com.app.simapag.fugas.dialogos;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.app.simapag.fugas.R;

public class DialogoPregunta extends DialogFragment {

    static  final  String ARG_TITULO = "mensaje_titulo";
    static  final  String ARG_Mensaje = "mensaje_dialogo";
    static  final  String ARG_ID = "identificacion_afectar";
    AlertDialog alertDialog;
    TextView textViewMensaje;

    String Titulo;
    String Mensaje;
    String Identificador;

  public static DialogoPregunta nuevaInstancia(String pIdentificador, String pMensaje , String pTitulo){
      Bundle arg = new Bundle();
      arg.putSerializable(ARG_ID, pIdentificador);
      arg.putSerializable(ARG_Mensaje, pMensaje);
      arg.putSerializable(ARG_TITULO, pTitulo);

      DialogoPregunta dialogoPregunta = new DialogoPregunta();
      dialogoPregunta.setArguments(arg);

      return  dialogoPregunta;
  }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Titulo = (String) getArguments().getString(ARG_TITULO);
        Mensaje = (String) getArguments().getString(ARG_Mensaje);
        Identificador = (String) getArguments().getString(ARG_ID);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View vista = LayoutInflater.from(getActivity()).inflate(R.layout.dialogo_pregunta_si_no, null);

        textViewMensaje = (TextView) vista.findViewById(R.id.text_view_dialogo_mensaje);
        textViewMensaje.setText(Mensaje);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setView(vista)
                .setTitle(Titulo)
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                })
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RespuestaListener respuestaListener = (RespuestaListener) getTargetFragment();
                        respuestaListener.aceptarRespuestaSI(Identificador);
                        dismiss();
                    }
                });
        alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);

        return  alertDialog;

    }

   // clases anidadas
   public interface RespuestaListener {
       void  aceptarRespuestaSI(String identificador);
   }
}
