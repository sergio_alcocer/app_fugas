package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentoListadoOrdenesRegistradas;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActividadListadoOrdenesRegistradas extends SingleFragmentoActividad {

    @Override
    protected Fragment crearFragmento() {
        return FragmentoListadoOrdenesRegistradas.nuevaInstancia();
    }

    public  static Intent nuevaInstancia(Context context){
        Intent intent = new Intent(context,ActividadListadoOrdenesRegistradas.class);
        return  intent;
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
