package com.app.simapag.fugas.fragmentos;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadOrdenesInvestigacion;
import com.app.simapag.fugas.modelos.Cls_Menu;
import com.app.simapag.fugas.modelos.OrdenTrabajoViewModel;
import com.app.simapag.fugas.modelos.OrdenesInvestigacionViewModel;
import com.app.simapag.fugas.singlenton.RepositorioVisita;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FragmentoMenuVisitas extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;

    @BindView(R.id.recyclerview_menu)
    RecyclerView recyclerMenu;
    LinearLayoutManager linearLayoutManager;

     OrdenesInvestigacionViewModel ordenesInvestigacionViewModel;

    List<Cls_Menu> ListaMenu = new ArrayList<>();
    Cls_Menu ObjMenu;

    private boolean blnSepuedeRegresar = false;
    private Unbinder unbinder;

    public  static  FragmentoMenuVisitas NuevaInstancia(){
         FragmentoMenuVisitas fragmentoMenuVisitas = new FragmentoMenuVisitas();
         return  fragmentoMenuVisitas;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        hacerMenu();
        ordenesInvestigacionViewModel = RepositorioVisita.get().getOrdenesInvestigacionViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_menu, container, false);
        unbinder = ButterKnife.bind(this, view);

        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerMenu.setLayoutManager(linearLayoutManager);


        textViewTitulo.setText("RPU:  " + ordenesInvestigacionViewModel.getRpu());

        configurarAdaptador();
        return  view;
    }


    public void configurarAdaptador(){

        if (isAdded()){
            recyclerMenu.setAdapter(new MenuVisitasAdapter(ListaMenu));
        }

    }

    public boolean isBlnSepuedeRegresar() {
        return blnSepuedeRegresar;
    }


    public void hacerMenu(){

        ObjMenu = new Cls_Menu();
        ObjMenu.setID(1);
        ObjMenu.setNombre("Información Solicitud");
        ListaMenu.add(ObjMenu);

        ObjMenu = new Cls_Menu();
        ObjMenu.setID(2);
        ObjMenu.setNombre("Visitas");
        ListaMenu.add(ObjMenu);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case  android.R.id.home:
                Intent intent = ActividadOrdenesInvestigacion.nuevaInstancia(getContext());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return  true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    // clases anidadas

    public class  MenuVisitasAdapter extends  RecyclerView.Adapter<MenuVisitasHolder>{

        List<Cls_Menu> listaMenu;

        public MenuVisitasAdapter(List<Cls_Menu> listaMenu) {
            this.listaMenu = listaMenu;
        }

        @NonNull
        @Override
        public MenuVisitasHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_menu, parent, false);
            return new MenuVisitasHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MenuVisitasHolder holder, int position) {
           Cls_Menu menu = listaMenu.get(position);
           holder.enlazarDatos(menu);
        }

        @Override
        public int getItemCount() {
            return listaMenu.size();
        }
    }

    public class MenuVisitasHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.image_view_icono)
        ImageView imageView;

        @BindView(R.id.text_view_nombre_menu)
        TextView textViewNombreMenu;

        Cls_Menu clsMenu;

        public MenuVisitasHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        public void enlazarDatos(Cls_Menu model){
            clsMenu =model;
            textViewNombreMenu.setText(model.getNombre());

            switch (model.getID()){
                case 1:
                    imageView.setImageResource(R.drawable.ic_baseline_info_60);
                    break;
                case 2:
                    imageView.setImageResource(R.drawable.ic_baseline_groups_60);
                    break;
                case 3:
                    imageView.setImageResource(R.drawable.ic_baseline_groups_60);
                    break;
                case 4:
                    imageView.setImageResource(R.drawable.ic_baseline_location_on_60);
                    break;
                case 5:
                    imageView.setImageResource(R.drawable.ic_baseline_image_60);
                    break;
                case 6:
                    imageView.setImageResource(R.drawable.ic_baseline_hardware_60);
                    break;

            }
        }

        @Override
        public void onClick(View view) {

        }
    }
}
