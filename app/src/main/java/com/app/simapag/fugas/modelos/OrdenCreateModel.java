package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class OrdenCreateModel {

    @SerializedName("Predio_ID")
    private  String Predio_ID;
    @SerializedName("Tipo_Falla_ID")
    private  String Tipo_Falla_ID;
    @SerializedName("Empleado_ID")
    private  String Empleado_ID;
    @SerializedName("Nombre_Empleado")
    private  String Nombre_Empleado;
    @SerializedName("Colonia_ID")
    private String Colonia_ID;
    @SerializedName("Calle_ID")
    private  String Calle_ID;
    @SerializedName("Calle_Referencia_ID")
    private  String Calle_Referencia_ID;
    @SerializedName("Calle_Referencia_ID2")
    private String Calle_Referencia_ID2;
    @SerializedName("Domicilio_Referencia")
    private  String Domicilio_Referencia;
    @SerializedName("Referencia")
    private  String Referencia;
    @SerializedName("Reporto")
    private  String Reporto;
    @SerializedName("Observaciones")
    private  String Observaciones;
    @SerializedName("Numero")
    private  String  Numero;
    @SerializedName("No_Cuenta")
    private  String No_Cuenta;
    @SerializedName("No_Medidor")
    private  String No_Medidor;
    @SerializedName("Telefono")
    private  String Telefono;

    public String getPredio_ID() {
        return Predio_ID;
    }

    public void setPredio_ID(String predio_ID) {
        Predio_ID = predio_ID;
    }

    public String getTipo_Falla_ID() {
        return Tipo_Falla_ID;
    }

    public void setTipo_Falla_ID(String tipo_Falla_ID) {
        Tipo_Falla_ID = tipo_Falla_ID;
    }

    public String getEmpleado_ID() {
        return Empleado_ID;
    }

    public void setEmpleado_ID(String empleado_ID) {
        Empleado_ID = empleado_ID;
    }

    public String getNombre_Empleado() {
        return Nombre_Empleado;
    }

    public void setNombre_Empleado(String nombre_Empleado) {
        Nombre_Empleado = nombre_Empleado;
    }

    public String getColonia_ID() {
        return Colonia_ID;
    }

    public void setColonia_ID(String colonia_ID) {
        Colonia_ID = colonia_ID;
    }

    public String getCalle_ID() {
        return Calle_ID;
    }

    public void setCalle_ID(String calle_ID) {
        Calle_ID = calle_ID;
    }

    public String getCalle_Referencia_ID() {
        return Calle_Referencia_ID;
    }

    public void setCalle_Referencia_ID(String calle_Referencia_ID) {
        Calle_Referencia_ID = calle_Referencia_ID;
    }

    public String getCalle_Referencia_ID2() {
        return Calle_Referencia_ID2;
    }

    public void setCalle_Referencia_ID2(String calle_Referencia_ID2) {
        Calle_Referencia_ID2 = calle_Referencia_ID2;
    }

    public String getDomicilio_Referencia() {
        return Domicilio_Referencia;
    }

    public void setDomicilio_Referencia(String domicilio_Referencia) {
        Domicilio_Referencia = domicilio_Referencia;
    }

    public String getReferencia() {
        return Referencia;
    }

    public void setReferencia(String referencia) {
        Referencia = referencia;
    }

    public String getReporto() {
        return Reporto;
    }

    public void setReporto(String reporto) {
        Reporto = reporto;
    }

    public String getObservaciones() {
        return Observaciones;
    }

    public void setObservaciones(String observaciones) {
        Observaciones = observaciones;
    }

    public String getNumero() {
        return Numero;
    }

    public void setNumero(String numero) {
        Numero = numero;
    }

    public String getNo_Cuenta() {
        return No_Cuenta;
    }

    public void setNo_Cuenta(String no_Cuenta) {
        No_Cuenta = no_Cuenta;
    }

    public String getNo_Medidor() {
        return No_Medidor;
    }

    public void setNo_Medidor(String no_Medidor) {
        No_Medidor = no_Medidor;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }
}
