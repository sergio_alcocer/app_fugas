package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentoVisitaInvestigacion;
import com.app.simapag.fugas.parciable.VisitaInvestigacionParciable;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActividadVisitaInvestigacion extends SingleFragmentoActividad {


    public static final String EXTRA_VISITA_INVESTIGACION = "com.app.simapag.visita.investigacion.edicion";

    @Override
    protected Fragment crearFragmento() {
        VisitaInvestigacionParciable visitaInvestigacionParciable = (VisitaInvestigacionParciable) getIntent().getParcelableExtra(EXTRA_VISITA_INVESTIGACION);
        FragmentoVisitaInvestigacion fragmentoVisitaInvestigacion =FragmentoVisitaInvestigacion.nuevaInstancia(visitaInvestigacionParciable);
        return fragmentoVisitaInvestigacion;
    }


    public static Intent nuevaInstancia(Context context, VisitaInvestigacionParciable visitaInvestigacionParciable){
         Intent intent = new Intent(context,ActividadVisitaInvestigacion.class);
          intent.putExtra(EXTRA_VISITA_INVESTIGACION, visitaInvestigacionParciable);
         return  intent;
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
