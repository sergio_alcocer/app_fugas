package com.app.simapag.fugas.fragmentos;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;

import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadMenu;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.modelos.LoginModel;
import com.app.simapag.fugas.modelos.MensajeModel;
import com.app.simapag.fugas.preferencias.Preferencias;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.util.Util;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FragmentoLogin extends Fragment {

    @BindView(R.id.text_input_layout_usuario)
    TextInputLayout textInputLayoutUsuario;

    @BindView(R.id.text_input_layout_password)
    TextInputLayout textInputLayoutPassword;

    @BindView(R.id.text_input_edit_usuario)
    TextInputEditText textInputEditTextUsuario;

    @BindView(R.id.text_input_edit_password)
    TextInputEditText textInputEditTextPassword;

    DelayedProgressDialog delayedProgressDialog;

    private  String Token="";
    private LoginModel ObjUsuario = new LoginModel();
    private Unbinder unbinder;

    public static FragmentoLogin crearFragmentoLogin(){
        return new FragmentoLogin();
   }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                Token = instanceIdResult.getToken();
                Log.e("newToken", Token);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_login, container, false);
        unbinder = ButterKnife.bind(this, view);

        view.findViewById(R.id.linear_layout_principal).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                View focusedView = getActivity().getCurrentFocus();
                /*
                 * If no view is focused, an NPE will be thrown
                 *
                 * Maxim Dmitriev
                 */
                if (focusedView != null) {
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }


                return true;
            }
        });

        textInputEditTextUsuario.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if(textInputEditTextUsuario.getText().toString().trim().length()>0){
                    textInputLayoutUsuario.setError(null);
                }
                return false;
            }
        });

        textInputEditTextPassword.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if(textInputEditTextPassword.getText().toString().trim().length()>0){
                    textInputLayoutPassword.setError(null);
                }

                return false;
            }
        });



        return view;
    }

    // métodos

    private  boolean validarInterfaz(){
        boolean respuesta = true;

        ObjUsuario.setToken(Token);


        if (textInputEditTextUsuario.getText().toString().trim().length() == 0){
            textInputLayoutUsuario.setError("Se requiere un nombre usuario");
            respuesta =  false;
        }else {
            textInputLayoutUsuario.setError(null);
            ObjUsuario.setUsuario(textInputEditTextUsuario.getText().toString().trim());
        }

        if(textInputEditTextPassword.getText().toString().trim().length() == 0){
            textInputLayoutPassword.setError("Se requiere una contraseña");
            respuesta = false;
        }else {
            textInputLayoutPassword.setError(null);
            ObjUsuario.setPassword(textInputEditTextPassword.getText().toString().trim());
        }

        if (Token.length() == 0){
            Util.mostarAlerta2(getActivity(),"No se generó token intente de nuevo");
            respuesta = false;
        }

        return  respuesta;
    }

    public  void guardarEmpleadoPreferencias(MensajeModel mensajeModel){
        Preferencias.setEmpleadoID( getContext(), mensajeModel.getDato2());
        Preferencias.setNombreUsuario(getContext(), mensajeModel.getDato1());
        Preferencias.setNombreRol(getContext(), mensajeModel.getDato3());
    }

    public void verificarUsuario(LoginModel loginModel){

        delayedProgressDialog.show(getParentFragmentManager(),"logon");

        FactoryPeticiones.getFactoryPeticiones()
                .verificarUsuario(loginModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<MensajeModel>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull MensajeModel mensajeModel) {

                            if(mensajeModel.getEstatus().equals("bien")){
                                    guardarEmpleadoPreferencias(mensajeModel);
                                Intent intent = ActividadMenu.nuevaInstancia(getContext());
                                startActivity(intent);
                            }else {
                                Util.mostarAlerta2(getActivity(), mensajeModel.getEstatus());
                            }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });

    }

    // eventos

    @OnClick(R.id.button_ingresar_login)
    public void  eventoLogin(){

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                Token = instanceIdResult.getToken();
                Log.e("newToken", Token);
            }
        });

        if(validarInterfaz() == false){
            return;
        }


        verificarUsuario(ObjUsuario);
    }

    // sobre escribir clases

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
    // clases anidadas


}
