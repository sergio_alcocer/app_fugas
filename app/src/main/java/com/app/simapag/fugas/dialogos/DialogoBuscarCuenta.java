package com.app.simapag.fugas.dialogos;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.simapag.fugas.BuildConfig;
import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadMenu;
import com.app.simapag.fugas.modelos.BusquedaPredioModel;
import com.app.simapag.fugas.modelos.PrediosPaginado;
import com.app.simapag.fugas.modelos.PrediosViewModel;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.util.Util;
import com.google.android.material.textfield.TextInputEditText;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DialogoBuscarCuenta extends DialogFragment {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;

    @BindView(R.id.image_view_microfono)
    ImageView micButton;


    @BindView(R.id.radio_buscar_rpu)
    RadioButton radioButtonRPU;

    @BindView(R.id.radio_buscar_no_cuenta)
    RadioButton radioButtonNoCuenta;

    @BindView(R.id.radio_buscar_no_medidor)
    RadioButton radioButtonNoMedidor;

    @BindView(R.id.imagen_button_cerrar_busqueda)
    ImageButton imageButtonCerrarBusqueda;

    @BindView(R.id.linear_layout_busqueda)
    View viewBusqueda;

    @BindView(R.id.text_input_edit_buscar_predio)
    TextInputEditText textInputEditTextQuery;

    @BindView(R.id.text_input_edit_buscar_predio_usuario)
    TextInputEditText textInputEditTextNombreUsuario;

    @BindView(R.id.recyclerview_predios)
    RecyclerView recyclerPredios;
    LinearLayoutManager linearLayoutManager;

    List<PrediosViewModel> ListaPredios = new ArrayList<>();
    String RPU="";
    String No_Medidor="";
    String No_Cuenta="";
    String NombreUsuario="";
    int mTotalElementos = 0;
    int mPagina;
    boolean loading = false;
    int totalItemCount;
    int lastFetchedPage = 1;

    DelayedProgressDialog delayedProgressDialog;
    BusquedaPredioModel ObjBusquedaPredioModel = new BusquedaPredioModel();

    Boolean blnBanderVistaBusqueda = true;
    private Unbinder unbinder;

    private  boolean BanderaPermisoMicrofono = false;
    private SpeechRecognizer speechRecognizer;


    public  static DialogoBuscarCuenta nuevaInstancia(){
         DialogoBuscarCuenta dialogoBuscarCuenta = new DialogoBuscarCuenta();
         dialogoBuscarCuenta.setCancelable(false);
         return dialogoBuscarCuenta;
    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_busqueda_cuenta, container, false);
        unbinder = ButterKnife.bind(this, view);

        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);

        textViewTitulo.setText("Buscar Cuenta");

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerPredios.setLayoutManager(linearLayoutManager);

        toolbar.inflateMenu(R.menu.menu_buscar);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_cancel_24);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()){
                    case  R.id.menu_item_buscar:
                        hideSoftKeyboard();
                        cerrarVentanaBusqueda();
                        ejecutarBusqueda();
                        break;
                }

                return false;
            }
        });

        recyclerPredios.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                totalItemCount = linearLayoutManager.getItemCount();
                if (!loading) {

                    if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == totalItemCount - 1) {
                        loading = true;
                        if (totalItemCount < mTotalElementos) {
                            mPagina++;
                            obtenerPredios(RPU,No_Cuenta,No_Medidor,NombreUsuario, mPagina);
                        }
                    }
                }
            }
        });

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(getActivity());
        final Intent speechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());


        speechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {

            }

            @Override
            public void onBeginningOfSpeech() {
                textInputEditTextQuery.setText("");
            }

            @Override
            public void onRmsChanged(float v) {

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int i) {

            }

            @Override
            public void onResults(Bundle bundle) {
                ArrayList<String> data = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                String resultado = data.get(0).trim();
                resultado= resultado.replaceAll("\\s+","");
                textInputEditTextQuery.setText(resultado);

            }

            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });

        micButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (BanderaPermisoMicrofono) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        speechRecognizer.stopListening();
                    }

                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        micButton.setImageResource(R.drawable.ic_baseline_mic_24);
                        speechRecognizer.startListening(speechRecognizerIntent);
                    }

                }
                return false;
            }
        });

        obtenerPermisoMicrofono();
        return  view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);


        // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);
    }

    @Override
    public int getTheme() {
        return R.style.DialogTheme;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    // métodos *****************************************************************
    public void hideSoftKeyboard() {
        try {
            View windowToken = getDialog().getWindow().getDecorView().getRootView();
            InputMethodManager imm = (InputMethodManager) getDialog().getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow( windowToken.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception ex) {

        }
    }

    public  void  cerrarVentanaBusqueda(){
        blnBanderVistaBusqueda = false;
        viewBusqueda.setVisibility(View.GONE);
        imageButtonCerrarBusqueda.setImageResource(R.drawable.ic_baseline_arrow_drop_up_12);
    }


    public  void  configurarAdapter(){

        if(isAdded()){
            recyclerPredios.setAdapter(new PredioAdapter(ListaPredios));
        }
    }


    public  void  ejecutarBusqueda(){

        if(radioButtonNoCuenta.isChecked())
            No_Cuenta = textInputEditTextQuery.getText().toString().trim();
        else
            No_Cuenta = "";

        if(radioButtonNoMedidor.isChecked())
            No_Medidor = textInputEditTextQuery.getText().toString().trim();
        else
            No_Medidor = "";

        if(radioButtonRPU.isChecked())
            RPU = textInputEditTextQuery.getText().toString().trim();
        else
            RPU= "";

        NombreUsuario = textInputEditTextNombreUsuario.getText().toString().trim();

        iniciarBusquedaPredios(RPU,No_Cuenta,No_Medidor, NombreUsuario);


    }

    public void   iniciarBusquedaPredios(String pRPU, String pNoCuenta, String pNoMedidor, String pNombreUsuario){
        lastFetchedPage=1;
        mPagina = 1;
        loading = false;
        obtenerPredios(pRPU,pNoCuenta,pNoMedidor,pNombreUsuario,mPagina);
    }

    // eventos *****************************************************************

    @OnClick(R.id.imagen_button_cerrar_busqueda)
    public void eventoCerrarBusqueda(){


        if(blnBanderVistaBusqueda){
            Util.hideKeyboard(getActivity());
            blnBanderVistaBusqueda = false;
            viewBusqueda.setVisibility(View.GONE);
            imageButtonCerrarBusqueda.setImageResource(R.drawable.ic_baseline_arrow_drop_up_12);
        }else {
            blnBanderVistaBusqueda = true;
            viewBusqueda.setVisibility(View.VISIBLE);
            imageButtonCerrarBusqueda.setImageResource(R.drawable.ic_baseline_arrow_drop_down_12);
        }
    }

    // permisos ****************************************************************

    private  void openSettings(){
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    private  void  obtenerPermisoMicrofono(){

        Dexter.withContext(getActivity())
                .withPermission(Manifest.permission.RECORD_AUDIO)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                        BanderaPermisoMicrofono = true;
                        micButton.setEnabled(true);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if(response.isPermanentlyDenied()){
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    // peticiones **************************************************************

    public  void obtenerPredios(String pRPU, String pNoCuenta, String pNoMedidor, String pNombreUsuario, int pPagina){
        ObjBusquedaPredioModel.setRPU(pRPU);
        ObjBusquedaPredioModel.setNo_Cuenta(pNoCuenta);
        ObjBusquedaPredioModel.setNo_Medidor(pNoMedidor);
        ObjBusquedaPredioModel.setNombreUsuario(pNombreUsuario);
        ObjBusquedaPredioModel.setPagina(pPagina);

        delayedProgressDialog.show(getParentFragmentManager(), "paginado");

        FactoryPeticiones.getFactoryPeticiones()
                .obtenerPredios(ObjBusquedaPredioModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PrediosPaginado>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull PrediosPaginado prediosPaginado) {

                        if (lastFetchedPage == 1){
                             ListaPredios = prediosPaginado.getListaPredios();
                             mTotalElementos = prediosPaginado.getTotalElementos();
                             configurarAdapter();
                             if(prediosPaginado.getListaPredios().size()== 0){
                                 Util.mostarAlerta2(getActivity(),"No se encontró información");
                             }
                        }else {
                            ListaPredios.addAll(prediosPaginado.getListaPredios());
                            mTotalElementos = prediosPaginado.getTotalElementos();
                            recyclerPredios.getAdapter().notifyDataSetChanged();
                        }
                        lastFetchedPage ++;
                        loading = false;
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        loading = true;
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });


    }

    // clases anidades **********************************************************

    public  interface BusquedaCuentaListener{

        void  selectedCuenta(PrediosViewModel prediosViewModel);
    }

   public  class  PredioAdapter extends RecyclerView.Adapter<PredioHolder>{

        List<PrediosViewModel> modelList;

       public PredioAdapter(List<PrediosViewModel> modelList) {
           this.modelList = modelList;
       }

       @NonNull
       @Override
       public PredioHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
           LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
           View view = layoutInflater.inflate(R.layout.item_predio, parent, false);
           return new PredioHolder(view);
       }

       @Override
       public void onBindViewHolder(@NonNull PredioHolder holder, int position) {
          PrediosViewModel prediosViewModel = modelList.get(position);
          holder.enlzarDatos(prediosViewModel);

          holder.buttonSeleccionar.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  BusquedaCuentaListener busquedaCuentaListener = (BusquedaCuentaListener) getTargetFragment();
                  busquedaCuentaListener.selectedCuenta(prediosViewModel);
                  dismiss();
              }
          });
       }

       @Override
       public int getItemCount() {
           return modelList.size();
       }
   }

    public  class  PredioHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.text_view_predio_colonia)
        TextView textViewColonia;

        @BindView(R.id.text_view_predio_domicilio)
        TextView textViewDomicilio;

        @BindView(R.id.text_view_predio_no_cuenta)
        TextView textViewNoCuenta;

        @BindView(R.id.text_view_predio_no_medidor)
        TextView textViewNoMedidor;

        @BindView(R.id.text_view_predio_rpu)
        TextView textViewRPU;

        @BindView(R.id.text_view_predio_usuario)
        TextView textViewUsuario;

        @BindView(R.id.button_predios_seleccionar)
        Button buttonSeleccionar;

        PrediosViewModel prediosViewModel;

        String domicilio;

        public PredioHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        public  void enlzarDatos(PrediosViewModel model){
            prediosViewModel = model;

            domicilio = model.getTipo() + " " + model.getCalle() + " No.Ext: " + model.getNumero_Exterior();

            textViewColonia.setText(model.getColonia());
            textViewNoMedidor.setText("No. Medidor: " +model.getNo_Medidor());
            textViewRPU.setText("RPU: " +model.getRPU());
            textViewNoCuenta.setText("No.Cuenta: " + model.getNo_Cuenta());
            textViewUsuario.setText(model.getApellido_Paterno() + " " + model.getApellido_Materno() + " " + model.getNombre());
            textViewDomicilio.setText(domicilio);
        }

        @Override
        public void onClick(View view) {

        }
    }

}
