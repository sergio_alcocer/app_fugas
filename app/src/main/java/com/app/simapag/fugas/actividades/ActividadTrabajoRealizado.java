package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentoTrabajoRealizado;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActividadTrabajoRealizado extends SingleFragmentoActividad {

    @Override
    protected Fragment crearFragmento() {
        return FragmentoTrabajoRealizado.nuevaInstancia();
    }

    public static Intent nuevaInstancia(Context context){
        Intent intent = new Intent(context,ActividadTrabajoRealizado.class);
        return  intent;
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
