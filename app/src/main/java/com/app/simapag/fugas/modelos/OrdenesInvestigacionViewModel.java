package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class OrdenesInvestigacionViewModel {

    @SerializedName("rpu")
    private String rpu;
    @SerializedName("folio")
    private double folio;
    @SerializedName("estatus")
    private String estatus;
    @SerializedName("fecha_solicito")
    private String fecha_solicito;
    @SerializedName("colonia")
    private String colonia;
    @SerializedName("calle")
    private String calle;
    @SerializedName("no_exterior")
    private String no_exterior;
    @SerializedName("no_interior")
    private String no_interior;
    @SerializedName("manzana")
    private String manzana;
    @SerializedName("lote")
    private String lote;
    @SerializedName("usuario")
    private String usuario;
    @SerializedName("tarifa")
    private String tarifa;
    @SerializedName("actividad")
    private String actividad;
    @SerializedName("comentario")
    private String comentario;
    @SerializedName("ultima_visita")
    private String ultima_visita;
    @SerializedName("no_solicitud")
    private String no_solicitud;
    @SerializedName("predio_id")
    private String predio_id;
    @SerializedName("fosa_septica")
    private String fosa_septica;
    @SerializedName("nombre_solicito")
    private String nombre_solicito;
    @SerializedName("predio_especial")
    private String predio_especial;
    @SerializedName("autorizada")
    private String autorizada;
    @SerializedName("tipo_provisional")
    private String tipo_provisional;
    @SerializedName("toma")
    private String toma;
    @SerializedName("descarga")
    private String descarga;
    @SerializedName("ampliacion")
    private String ampliacion;
    @SerializedName("Autorizar_Hidraulica")
    private String Autorizar_Hidraulica;
    @SerializedName("Autorizar_Alcantarillado")
    private String Autorizar_Alcantarillado;
    @SerializedName("Autorizar_Distribucion")
    private String Autorizar_Distribucion;

    public String getRpu() {
        return rpu;
    }

    public void setRpu(String rpu) {
        this.rpu = rpu;
    }

    public double getFolio() {
        return folio;
    }

    public void setFolio(double folio) {
        this.folio = folio;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getFecha_solicito() {
        return fecha_solicito;
    }

    public void setFecha_solicito(String fecha_solicito) {
        this.fecha_solicito = fecha_solicito;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNo_exterior() {
        return no_exterior;
    }

    public void setNo_exterior(String no_exterior) {
        this.no_exterior = no_exterior;
    }

    public String getNo_interior() {
        return no_interior;
    }

    public void setNo_interior(String no_interior) {
        this.no_interior = no_interior;
    }

    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTarifa() {
        return tarifa;
    }

    public void setTarifa(String tarifa) {
        this.tarifa = tarifa;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getUltima_visita() {
        return ultima_visita;
    }

    public void setUltima_visita(String ultima_visita) {
        this.ultima_visita = ultima_visita;
    }

    public String getNo_solicitud() {
        return no_solicitud;
    }

    public void setNo_solicitud(String no_solicitud) {
        this.no_solicitud = no_solicitud;
    }

    public String getPredio_id() {
        return predio_id;
    }

    public void setPredio_id(String predio_id) {
        this.predio_id = predio_id;
    }

    public String getFosa_septica() {
        return fosa_septica;
    }

    public void setFosa_septica(String fosa_septica) {
        this.fosa_septica = fosa_septica;
    }

    public String getNombre_solicito() {
        return nombre_solicito;
    }

    public void setNombre_solicito(String nombre_solicito) {
        this.nombre_solicito = nombre_solicito;
    }

    public String getPredio_especial() {
        return predio_especial;
    }

    public void setPredio_especial(String predio_especial) {
        this.predio_especial = predio_especial;
    }

    public String getAutorizada() {
        return autorizada;
    }

    public void setAutorizada(String autorizada) {
        this.autorizada = autorizada;
    }

    public String getTipo_provisional() {
        return tipo_provisional;
    }

    public void setTipo_provisional(String tipo_provisional) {
        this.tipo_provisional = tipo_provisional;
    }

    public String getToma() {
        return toma;
    }

    public void setToma(String toma) {
        this.toma = toma;
    }

    public String getDescarga() {
        return descarga;
    }

    public void setDescarga(String descarga) {
        this.descarga = descarga;
    }

    public String getAmpliacion() {
        return ampliacion;
    }

    public void setAmpliacion(String ampliacion) {
        this.ampliacion = ampliacion;
    }

    public String getAutorizar_Hidraulica() {
        return Autorizar_Hidraulica;
    }

    public void setAutorizar_Hidraulica(String autorizar_Hidraulica) {
        Autorizar_Hidraulica = autorizar_Hidraulica;
    }

    public String getAutorizar_Alcantarillado() {
        return Autorizar_Alcantarillado;
    }

    public void setAutorizar_Alcantarillado(String autorizar_Alcantarillado) {
        Autorizar_Alcantarillado = autorizar_Alcantarillado;
    }

    public String getAutorizar_Distribucion() {
        return Autorizar_Distribucion;
    }

    public void setAutorizar_Distribucion(String autorizar_Distribucion) {
        Autorizar_Distribucion = autorizar_Distribucion;
    }
}
