package com.app.simapag.fugas.actividades;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.app.simapag.fugas.BuildConfig;
import com.app.simapag.fugas.R;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.modelos.CoordenadasOrdenesModel;
import com.app.simapag.fugas.modelos.MensajeModel;
import com.app.simapag.fugas.modelos.OrdenTrabajoViewModel;
import com.app.simapag.fugas.preferencias.Preferencias;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.singlenton.RepositorioOT;
import com.app.simapag.fugas.util.LatLngInterpolator;
import com.app.simapag.fugas.util.MarkerAnimation;
import com.app.simapag.fugas.util.Util;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ActividadUbicacion extends AppCompatActivity   implements OnMapReadyCallback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    public TextView textViewTituloBar;


    @BindView(R.id.text_input_edit_ubicacion_latitud)
    TextInputEditText textInputEditTextLatitud;
    @BindView(R.id.text_input_edit_ubicacion_longitud)
    TextInputEditText textInputEditTextLongitud;

    OrdenTrabajoViewModel ordenTrabajoViewModel;
    CoordenadasOrdenesModel coordenadasOrdenesModel = new CoordenadasOrdenesModel();

    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;
    LocationRequest mLocationRequests = LocationRequest.create();
    private  static  final  long UPDATE_INTERVAL = 10000, FASTER_INTERVAL = 5000 ; // = 5 seconds
    boolean blnPermisos = false;
    double latitud = 0;
    double longitud = 0;
    private  GoogleMap mMapa;
    private Marker currentLocationMarker;
    private Location currentLocation;
    DelayedProgressDialog delayedProgressDialog;
    private boolean blnSepuedeRegresar = false;
    View parentLayout;

    public static Intent nuevaInstancia(Context context){
        Intent intent = new Intent(context, ActividadUbicacion.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.actividad_ubicacion);
        ButterKnife.bind(this);

        parentLayout = findViewById(android.R.id.content);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        setSupportActionBar(toolbar);
        textViewTituloBar = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ordenTrabajoViewModel = RepositorioOT.get().getObjTrabajViewModel();

        textViewTituloBar.setText("Ubicar Trabajo");

        textInputEditTextLatitud.setEnabled(false);
        textInputEditTextLongitud.setEnabled(false);

        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);

        // configuración del callback del GPS
        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                Location location = locationResult.getLastLocation();
                if (location != null){
                    stopGPS();
                    //delayedProgressDialog.cancel();
                    currentLocation = location;
                    latitud = location.getLatitude();
                    longitud = location.getLongitude();

                    textInputEditTextLatitud.setText(String.valueOf(latitud));
                    textInputEditTextLongitud.setText(String.valueOf(longitud));

                    animateCamera(currentLocation);
                    showMarker(currentLocation);

                }
            }
        };


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
       mMapa = googleMap;
        // Posicionar el mapa en una localización y con un nivel de zoom
        //LatLng latLng = new LatLng(36.679582, -5.444791);
       // googleMap.addMarker( new MarkerOptions().position(latLng).title("aquie"));

        mMapa.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                Log.d("System out", "onMarkerDragStart..."+marker.getPosition().latitude+"..."+marker.getPosition().longitude);
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                Log.d("System out", "onMarkerDragEnd..."+marker.getPosition().latitude+"..."+marker.getPosition().longitude);
                mMapa.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
                textInputEditTextLatitud.setText(String.valueOf( Util.round( marker.getPosition().latitude,8)));
                textInputEditTextLongitud.setText(String.valueOf( Util.round( marker.getPosition().longitude,8)));
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                Log.i("System out", "onMarkerDrag...");
            }
        });
        obtenerCoordenadas();
    }


    // peticiones

     public  void  obtenerCoordenadas(){

         delayedProgressDialog.show(getSupportFragmentManager(), "actualizar");

         FactoryPeticiones.getFactoryPeticiones()
                 .obtenerCoordenadasTrabajo(ordenTrabajoViewModel.getNo_Orden_Trabajo())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribeOn(Schedulers.io())
                 .subscribe(new Observer<CoordenadasOrdenesModel>() {
                     @Override
                     public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                     }

                     @Override
                     public void onNext(@io.reactivex.annotations.NonNull CoordenadasOrdenesModel coordenadasOrdenesModel) {
                               visualizarCoordenadas(coordenadasOrdenesModel);
                     }

                     @Override
                     public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                         Util.mostrarAlerta(parentLayout, "Error de conectividad");
                     }

                     @Override
                     public void onComplete() {
                         delayedProgressDialog.cancel();
                     }
                 });
     }

     public  void  actualizarCoordenadas(){

         delayedProgressDialog.show(getSupportFragmentManager(), "actualizar");

         FactoryPeticiones.getFactoryPeticiones()
                 .actualizarCoordenadas(coordenadasOrdenesModel)
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribeOn(Schedulers.io())
                 .subscribe(new Observer<MensajeModel>() {
                     @Override
                     public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                     }

                     @Override
                     public void onNext(@io.reactivex.annotations.NonNull MensajeModel mensajeModel) {

                         if(mensajeModel.getEstatus().equals("bien")){
                             Util.mostrarAlerta(parentLayout, "Proceso Correcto");
                         }else {

                             Util.mostrarAlerta(parentLayout, mensajeModel.getEstatus());
                         }
                     }

                     @Override
                     public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                         Util.mostrarAlerta(parentLayout, "Error de conectividad");
                     }

                     @Override
                     public void onComplete() {
                         delayedProgressDialog.cancel();
                     }
                 });
     }

    // métodos

    public  void  visualizarCoordenadas(CoordenadasOrdenesModel pModel){

        if( pModel.getLatitud_Trabajo() != 0 && pModel.getLongitud_Trabajo() != 0){
            Location location = new Location(LocationManager.GPS_PROVIDER);
            location.setLatitude(pModel.getLatitud_Trabajo());
            location.setLongitude(pModel.getLongitud_Trabajo());

            animateCamera(location);
            showMarker(location);

            textInputEditTextLatitud.setText(String.valueOf(pModel.getLatitud_Trabajo()));
            textInputEditTextLongitud.setText(String.valueOf(pModel.getLongitud_Trabajo()));
        }
    }

    public  boolean validarInterfaz(){
        coordenadasOrdenesModel.setLatitud_Trabajo(Util.convertirDouble(textInputEditTextLatitud.getText().toString()));
        coordenadasOrdenesModel.setLongitud_Trabajo(Util.convertirDouble(textInputEditTextLongitud.getText().toString()));
        coordenadasOrdenesModel.setNo_Orden_Trabajo(ordenTrabajoViewModel.getNo_Orden_Trabajo());


        if( coordenadasOrdenesModel.getLatitud_Trabajo() == 0 || coordenadasOrdenesModel.getLatitud_Trabajo() ==0){
            Util.mostrarAlerta(parentLayout, "Se requieren coordenas geográficas");
            return  false;
        }


        return  true;
    }

    public boolean isBlnSepuedeRegresar() {
        return blnSepuedeRegresar;
    }

    private void animateCamera(@NonNull Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mMapa.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(latLng)));
    }

    private CameraPosition getCameraPositionWithBearing(LatLng latLng) {
        return new CameraPosition.Builder().target(latLng).zoom(16).build();
    }

    private void showMarker(@NonNull Location currentLocation) {
        LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        if (currentLocationMarker == null)
            currentLocationMarker = mMapa.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker()).position(latLng).title("Trabajo").draggable(true));
        else
            MarkerAnimation.animateMarkerToGB(currentLocationMarker, latLng, new LatLngInterpolator.Spherical());
    }

    // eventos
     @OnClick(R.id.button_ubicacion_gps)
    public  void  onObtenerUbicacionGPS(){
          obtenerPermisosGPS();
     }

     @OnClick(R.id.button_ubicacion_guardar)
     public void  onGuardarUbicacion(){

        if(validarInterfaz() == false){
            return;
        }

        actualizarCoordenadas();
     }


     // permisos del gps

    private  void openSettings(){
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    public void obtenerPermisosGPS(){

        Dexter.withContext(this)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {

                if(multiplePermissionsReport.areAllPermissionsGranted()){
                     ejecutarProcesoGPS();
                }else {
                    openSettings();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                permissionToken.continuePermissionRequest();
            }
        }).check();

    }

    public void ejecutarProcesoGPS(){

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationRequests.setInterval(UPDATE_INTERVAL);
        mLocationRequests.setFastestInterval(FASTER_INTERVAL);
        mLocationRequests.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequests);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(locationSettingsRequest);

        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                // All location settings are satisfied. The client can initialize
                // location requests here.
                // ...
                startLocationUpdates();
                blnPermisos = true;
            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                Util.mostrarAlerta(parentLayout, "No se puedo acceder al gps");
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(ActividadUbicacion.this,
                                1);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });


    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        fusedLocationClient.requestLocationUpdates(mLocationRequests,
                locationCallback,
                Looper.getMainLooper());
    }

    public  void  stopGPS(){

        if(locationCallback != null){
            if(fusedLocationClient != null){
                if(blnPermisos){
                    fusedLocationClient.removeLocationUpdates(locationCallback);
                }
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                blnSepuedeRegresar = true;
                this.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {

        if(isBlnSepuedeRegresar()){
            super.onBackPressed();
        }else {
            return;
        }

    }

}
