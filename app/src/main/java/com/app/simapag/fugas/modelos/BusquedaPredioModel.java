package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class BusquedaPredioModel {

    @SerializedName("Pagina")
    private  int Pagina;
    @SerializedName("RPU")
    private  String RPU;
    @SerializedName("No_Cuenta")
    private  String No_Cuenta;
    @SerializedName("No_Medidor")
    private  String No_Medidor;
    @SerializedName("NombreUsuario")
    private  String NombreUsuario;

    public int getPagina() {
        return Pagina;
    }

    public void setPagina(int pagina) {
        Pagina = pagina;
    }

    public String getRPU() {
        return RPU;
    }

    public void setRPU(String RPU) {
        this.RPU = RPU;
    }

    public String getNo_Cuenta() {
        return No_Cuenta;
    }

    public void setNo_Cuenta(String no_Cuenta) {
        No_Cuenta = no_Cuenta;
    }

    public String getNo_Medidor() {
        return No_Medidor;
    }

    public void setNo_Medidor(String no_Medidor) {
        No_Medidor = no_Medidor;
    }

    public String getNombreUsuario() {
        return NombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        NombreUsuario = nombreUsuario;
    }
}
