package com.app.simapag.fugas.modelos;

public class Cls_Menu {

    private  String Nombre;
    private  int ID;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
}
