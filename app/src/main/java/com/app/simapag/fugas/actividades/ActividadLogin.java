package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentoLogin;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActividadLogin extends SingleFragmentoActividad {

    @Override
    protected Fragment crearFragmento() {
        return FragmentoLogin.crearFragmentoLogin();
    }

    public static Intent nuevaInstancia(Context context){
        Intent intent = new Intent(context, ActividadLogin.class);
        return  intent;
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
