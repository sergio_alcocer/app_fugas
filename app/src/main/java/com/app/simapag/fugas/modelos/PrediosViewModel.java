package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class PrediosViewModel {

    @SerializedName("RPU")
    private  String RPU ;
    @SerializedName("Predio_ID")
    private  String Predio_ID;
    @SerializedName("No_Cuenta")
    private  String No_Cuenta;
    @SerializedName("Nombre")
    private  String Nombre;
    @SerializedName("Apellido_Paterno")
    private  String Apellido_Paterno;
    @SerializedName("Apellido_Materno")
    private  String Apellido_Materno;
    @SerializedName("No_Zona")
    private  String No_Zona;
    @SerializedName("Estatus")
    private  String Estatus;
    @SerializedName("Tipo")
    private  String Tipo;
    @SerializedName("Calle")
    private  String Calle;
    @SerializedName("Colonia")
    private  String Colonia;
    @SerializedName("Numero_Exterior")
    private  String Numero_Exterior;
    @SerializedName("Numero_Interior")
    private  String Numero_Interior;
    @SerializedName("Manzana")
    private  String Manzana;
    @SerializedName("No_Medidor")
    private  String No_Medidor;
    @SerializedName("Calle_ID")
    private  String Calle_ID;
    @SerializedName("Colonia_ID")
    private  String Colonia_ID;
    @SerializedName("Calle_Referencia1_ID")
    private  String Calle_Referencia1_ID;
    @SerializedName("Calle_Referencia2_ID")
    private  String Calle_Referencia2_ID;
    @SerializedName("Calle_Referencia")
    private  String Calle_Referencia;
    @SerializedName("Calle_Referencia2")
    private  String Calle_Referencia2;

    public String getRPU() {
        return RPU;
    }

    public void setRPU(String RPU) {
        this.RPU = RPU;
    }

    public String getPredio_ID() {
        return Predio_ID;
    }

    public void setPredio_ID(String predio_ID) {
        Predio_ID = predio_ID;
    }

    public String getNo_Cuenta() {
        return No_Cuenta;
    }

    public void setNo_Cuenta(String no_Cuenta) {
        No_Cuenta = no_Cuenta;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellido_Paterno() {
        return Apellido_Paterno;
    }

    public void setApellido_Paterno(String apellido_Paterno) {
        Apellido_Paterno = apellido_Paterno;
    }

    public String getApellido_Materno() {
        return Apellido_Materno;
    }

    public void setApellido_Materno(String apellido_Materno) {
        Apellido_Materno = apellido_Materno;
    }

    public String getNo_Zona() {
        return No_Zona;
    }

    public void setNo_Zona(String no_Zona) {
        No_Zona = no_Zona;
    }

    public String getEstatus() {
        return Estatus;
    }

    public void setEstatus(String estatus) {
        Estatus = estatus;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        Tipo = tipo;
    }

    public String getCalle() {
        return Calle;
    }

    public void setCalle(String calle) {
        Calle = calle;
    }

    public String getColonia() {
        return Colonia;
    }

    public void setColonia(String colonia) {
        Colonia = colonia;
    }

    public String getNumero_Exterior() {
        return Numero_Exterior;
    }

    public void setNumero_Exterior(String numero_Exterior) {
        Numero_Exterior = numero_Exterior;
    }

    public String getNumero_Interior() {
        return Numero_Interior;
    }

    public void setNumero_Interior(String numero_Interior) {
        Numero_Interior = numero_Interior;
    }

    public String getManzana() {
        return Manzana;
    }

    public void setManzana(String manzana) {
        Manzana = manzana;
    }

    public String getNo_Medidor() {
        return No_Medidor;
    }

    public void setNo_Medidor(String no_Medidor) {
        No_Medidor = no_Medidor;
    }

    public String getCalle_ID() {
        return Calle_ID;
    }

    public void setCalle_ID(String calle_ID) {
        Calle_ID = calle_ID;
    }

    public String getColonia_ID() {
        return Colonia_ID;
    }

    public void setColonia_ID(String colonia_ID) {
        Colonia_ID = colonia_ID;
    }

    public String getCalle_Referencia1_ID() {
        return Calle_Referencia1_ID;
    }

    public void setCalle_Referencia1_ID(String calle_Referencia1_ID) {
        Calle_Referencia1_ID = calle_Referencia1_ID;
    }

    public String getCalle_Referencia2_ID() {
        return Calle_Referencia2_ID;
    }

    public void setCalle_Referencia2_ID(String calle_Referencia2_ID) {
        Calle_Referencia2_ID = calle_Referencia2_ID;
    }

    public String getCalle_Referencia() {
        return Calle_Referencia;
    }

    public void setCalle_Referencia(String calle_Referencia) {
        Calle_Referencia = calle_Referencia;
    }

    public String getCalle_Referencia2() {
        return Calle_Referencia2;
    }

    public void setCalle_Referencia2(String calle_Referencia2) {
        Calle_Referencia2 = calle_Referencia2;
    }
}

