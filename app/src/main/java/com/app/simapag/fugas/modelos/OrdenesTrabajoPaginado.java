package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrdenesTrabajoPaginado {

    @SerializedName("TotalElementos")
    private int TotalElementos;
    @SerializedName("ListaOrdenes")
    private List<OrdenTrabajoViewModel> ListaOrdenes;

    public int getTotalElementos() {
        return TotalElementos;
    }

    public void setTotalElementos(int totalElementos) {
        TotalElementos = totalElementos;
    }

    public List<OrdenTrabajoViewModel> getListaOrdenes() {
        return ListaOrdenes;
    }

    public void setListaOrdenes(List<OrdenTrabajoViewModel> listaOrdenes) {
        ListaOrdenes = listaOrdenes;
    }
}
