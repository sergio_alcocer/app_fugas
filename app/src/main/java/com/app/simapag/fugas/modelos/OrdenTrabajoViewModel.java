package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class OrdenTrabajoViewModel {

    @SerializedName("No_Orden_Trabajo")
    private   String No_Orden_Trabajo;
    @SerializedName("Folio")
    private   String Folio;
    @SerializedName("Estatus")
    private  String  Estatus;
    @SerializedName("Fecha_Elaboro")
    private   String  Fecha_Elaboro;
    @SerializedName("No_Cuenta")
    private  String  No_Cuenta;
    @SerializedName("Colonia")
    private  String Colonia;
    @SerializedName("Tipo_Falla_ID")
    private  String  Tipo_Falla_ID;
    @SerializedName("Falla")
    private  String Falla;
    @SerializedName("No_Suspension")
    private  String No_Suspension;
    @SerializedName("Predio_ID")
    private  String Predio_ID;
    @SerializedName("Clave_Nom")
    private  String Clave_Nom;
    @SerializedName("Usuario")
    private  String Usuario;
    @SerializedName("Domicilio")
    private  String Domicilio;
    @SerializedName("Brigada_ID")
    private String Brigada_ID;
    @SerializedName("Distrito_ID")
    private String Distrito_ID;
    @SerializedName("Distrito")
    private String Distrito;
    @SerializedName("Observaciones")
    private String Observaciones;
    @SerializedName("RPU")
    private  String RPU;
    @SerializedName("No_Medidor")
    private  String No_Medidor;
    @SerializedName("latitud")
    private  double latitud;
    @SerializedName("longitud")
    private  double longitud;
    @SerializedName("Calle_ID")
    private  String Calle_ID;
    @SerializedName("Colonia_ID")
    private  String Colonia_ID;
    @SerializedName("Marca")
    private  String Marca;
    @SerializedName("Ultima_Lectura")
    private  double Ultima_Lectura;
    @SerializedName("Autorizo")
    private  String Autorizo;
    @SerializedName("Fecha_Trabajo")
    private String Fecha_Trabajo;
    @SerializedName("Observaciones_Trabajo_Realizado")
    private String Observaciones_Trabajo_Realizado;
    @SerializedName("Recibio")
    private String Recibio;
    @SerializedName("Reporto")
    private  String Reporto;
    @SerializedName("Material_Fuga_ID")
    private int Material_Fuga_ID;
    @SerializedName("Diametro_Fuga_ID")
    private  int Diametro_Fuga_ID;
    @SerializedName("Fecha_Inicio")
    private  String Fecha_Inicio;
    @SerializedName("Fecha_Termino")
    private  String Fecha_Termino;

    public String getNo_Orden_Trabajo() {
        return No_Orden_Trabajo;
    }

    public void setNo_Orden_Trabajo(String no_Orden_Trabajo) {
        No_Orden_Trabajo = no_Orden_Trabajo;
    }

    public String getFolio() {
        return Folio;
    }

    public void setFolio(String folio) {
        Folio = folio;
    }

    public String getEstatus() {
        return Estatus;
    }

    public void setEstatus(String estatus) {
        Estatus = estatus;
    }

    public String getFecha_Elaboro() {
        return Fecha_Elaboro;
    }

    public void setFecha_Elaboro(String fecha_Elaboro) {
        Fecha_Elaboro = fecha_Elaboro;
    }

    public String getNo_Cuenta() {
        return No_Cuenta;
    }

    public void setNo_Cuenta(String no_Cuenta) {
        No_Cuenta = no_Cuenta;
    }

    public String getColonia() {
        return Colonia;
    }

    public void setColonia(String colonia) {
        Colonia = colonia;
    }

    public String getTipo_Falla_ID() {
        return Tipo_Falla_ID;
    }

    public void setTipo_Falla_ID(String tipo_Falla_ID) {
        Tipo_Falla_ID = tipo_Falla_ID;
    }

    public String getFalla() {
        return Falla;
    }

    public void setFalla(String falla) {
        Falla = falla;
    }

    public String getNo_Suspension() {
        return No_Suspension;
    }

    public void setNo_Suspension(String no_Suspension) {
        No_Suspension = no_Suspension;
    }

    public String getPredio_ID() {
        return Predio_ID;
    }

    public void setPredio_ID(String predio_ID) {
        Predio_ID = predio_ID;
    }

    public String getClave_Nom() {
        return Clave_Nom;
    }

    public void setClave_Nom(String clave_Nom) {
        Clave_Nom = clave_Nom;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        Usuario = usuario;
    }

    public String getDomicilio() {
        return Domicilio;
    }

    public void setDomicilio(String domicilio) {
        Domicilio = domicilio;
    }

    public String getRPU() {
        return RPU;
    }

    public void setRPU(String RPU) {
        this.RPU = RPU;
    }

    public String getNo_Medidor() {
        return No_Medidor;
    }

    public void setNo_Medidor(String no_Medidor) {
        No_Medidor = no_Medidor;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getCalle_ID() {
        return Calle_ID;
    }

    public void setCalle_ID(String calle_ID) {
        Calle_ID = calle_ID;
    }

    public String getColonia_ID() {
        return Colonia_ID;
    }

    public void setColonia_ID(String colonia_ID) {
        Colonia_ID = colonia_ID;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String marca) {
        Marca = marca;
    }

    public double getUltima_Lectura() {
        return Ultima_Lectura;
    }

    public void setUltima_Lectura(double ultima_Lectura) {
        Ultima_Lectura = ultima_Lectura;
    }

    public String getAutorizo() {
        return Autorizo;
    }

    public void setAutorizo(String autorizo) {
        Autorizo = autorizo;
    }

    public String getBrigada_ID() {
        return Brigada_ID;
    }

    public void setBrigada_ID(String brigada_ID) {
        Brigada_ID = brigada_ID;
    }

    public String getDistrito_ID() {
        return Distrito_ID;
    }

    public void setDistrito_ID(String distrito_ID) {
        Distrito_ID = distrito_ID;
    }

    public String getDistrito() {
        return Distrito;
    }

    public void setDistrito(String distrito) {
        Distrito = distrito;
    }

    public String getObservaciones() {
        return Observaciones;
    }

    public void setObservaciones(String observaciones) {
        Observaciones = observaciones;
    }

    public String getFecha_Trabajo() {
        return Fecha_Trabajo;
    }

    public void setFecha_Trabajo(String fecha_Trabajo) {
        Fecha_Trabajo = fecha_Trabajo;
    }

    public String getObservaciones_Trabajo_Realizado() {
        return Observaciones_Trabajo_Realizado;
    }

    public void setObservaciones_Trabajo_Realizado(String observaciones_Trabajo_Realizado) {
        Observaciones_Trabajo_Realizado = observaciones_Trabajo_Realizado;
    }

    public String getRecibio() {
        return Recibio;
    }

    public void setRecibio(String recibio) {
        Recibio = recibio;
    }

    public String getReporto() {
        return Reporto;
    }

    public void setReporto(String reporto) {
        Reporto = reporto;
    }

    public int getMaterial_Fuga_ID() {
        return Material_Fuga_ID;
    }

    public void setMaterial_Fuga_ID(int material_Fuga_ID) {
        Material_Fuga_ID = material_Fuga_ID;
    }

    public int getDiametro_Fuga_ID() {
        return Diametro_Fuga_ID;
    }

    public void setDiametro_Fuga_ID(int diametro_Fuga_ID) {
        Diametro_Fuga_ID = diametro_Fuga_ID;
    }

    public String getFecha_Inicio() {
        return Fecha_Inicio;
    }

    public void setFecha_Inicio(String fecha_Inicio) {
        Fecha_Inicio = fecha_Inicio;
    }

    public String getFecha_Termino() {
        return Fecha_Termino;
    }

    public void setFecha_Termino(String fecha_Termino) {
        Fecha_Termino = fecha_Termino;
    }
}
