package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentoListadoMaterial;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActvidadListadoMaterial extends SingleFragmentoActividad {

    @Override
    protected Fragment crearFragmento() {
        return FragmentoListadoMaterial.nuevaInstancia();
    }

    public static Intent nuevaInstancia(Context context) {
      Intent intent = new Intent(context,ActvidadListadoMaterial.class);
      return  intent;
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
