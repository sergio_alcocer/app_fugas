package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class VisitasViewModel {

    @SerializedName("No_Visita_Trabajo")
    private  String No_Visita_Trabajo;
    @SerializedName("No_Orden_Trabajo")
    private  String No_Orden_Trabajo;
    @SerializedName("Detalles")
    private  String Detalles;
    @SerializedName("Fecha")
    private  String  Fecha;
    @SerializedName("Estatus")
    private  String Estatus;
    @SerializedName("Nombre_Usuario")
    private  String Nombre_Usuario;

    public String getNo_Visita_Trabajo() {
        return No_Visita_Trabajo;
    }

    public void setNo_Visita_Trabajo(String no_Visita_Trabajo) {
        No_Visita_Trabajo = no_Visita_Trabajo;
    }

    public String getNo_Orden_Trabajo() {
        return No_Orden_Trabajo;
    }

    public void setNo_Orden_Trabajo(String no_Orden_Trabajo) {
        No_Orden_Trabajo = no_Orden_Trabajo;
    }

    public String getDetalles() {
        return Detalles;
    }

    public void setDetalles(String detalles) {
        Detalles = detalles;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public String getEstatus() {
        return Estatus;
    }

    public void setEstatus(String estatus) {
        Estatus = estatus;
    }

    public String getNombre_Usuario() {
        return Nombre_Usuario;
    }

    public void setNombre_Usuario(String nombre_Usuario) {
        Nombre_Usuario = nombre_Usuario;
    }
}
