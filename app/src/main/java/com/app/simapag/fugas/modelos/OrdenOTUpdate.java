package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class OrdenOTUpdate {

    @SerializedName("No_Orden_Trabajo")
    private  String No_Orden_Trabajo;
    @SerializedName("Falla_ID")
    private  String Falla_ID;
    @SerializedName("Empleado_Realizo_ID")
    private  String Empleado_Realizo_ID;
    @SerializedName("Comentarios")
    private  String Comentarios;
    @SerializedName("Anomalias")
    private  String Anomalias;
    @SerializedName("Fecha_Trabajo")
    private  String Fecha_Trabajo;
    @SerializedName("Fecha_Inicio_Hora")
    private  String Fecha_Inicio_Hora;
    @SerializedName("Fecha_Fin_Hora")
    private  String Fecha_Fin_Hora;
    @SerializedName("Trabajo_Realizado")
    private  String Trabajo_Realizado;
    @SerializedName("Diametro_Fuga_ID")
    private  String Diametro_Fuga_ID;
    @SerializedName("Material_Fuga_ID")
    private  String Material_Fuga_ID;
    @SerializedName("Nombre_Usuario")
    private  String Nombre_Usuario;
    @SerializedName("BrigadaID")
    private  String BrigadaID;
    @SerializedName("Empleado_ID")
    private  String Empleado_ID;
    @SerializedName("RPU")
    private  String RPU;
    public String getNo_Orden_Trabajo() {
        return No_Orden_Trabajo;
    }

    public void setNo_Orden_Trabajo(String no_Orden_Trabajo) {
        No_Orden_Trabajo = no_Orden_Trabajo;
    }

    public String getFalla_ID() {
        return Falla_ID;
    }

    public void setFalla_ID(String falla_ID) {
        Falla_ID = falla_ID;
    }

    public String getEmpleado_Realizo_ID() {
        return Empleado_Realizo_ID;
    }

    public void setEmpleado_Realizo_ID(String empleado_Realizo_ID) {
        Empleado_Realizo_ID = empleado_Realizo_ID;
    }

    public String getComentarios() {
        return Comentarios;
    }

    public void setComentarios(String comentarios) {
        Comentarios = comentarios;
    }

    public String getAnomalias() {
        return Anomalias;
    }

    public void setAnomalias(String anomalias) {
        Anomalias = anomalias;
    }

    public String getFecha_Trabajo() {
        return Fecha_Trabajo;
    }

    public void setFecha_Trabajo(String fecha_Trabajo) {
        Fecha_Trabajo = fecha_Trabajo;
    }

    public String getFecha_Inicio_Hora() {
        return Fecha_Inicio_Hora;
    }

    public void setFecha_Inicio_Hora(String fecha_Inicio_Hora) {
        Fecha_Inicio_Hora = fecha_Inicio_Hora;
    }

    public String getFecha_Fin_Hora() {
        return Fecha_Fin_Hora;
    }

    public void setFecha_Fin_Hora(String fecha_Fin_Hora) {
        Fecha_Fin_Hora = fecha_Fin_Hora;
    }

    public String getTrabajo_Realizado() {
        return Trabajo_Realizado;
    }

    public void setTrabajo_Realizado(String trabajo_Realizado) {
        Trabajo_Realizado = trabajo_Realizado;
    }

    public String getDiametro_Fuga_ID() {
        return Diametro_Fuga_ID;
    }

    public void setDiametro_Fuga_ID(String diametro_Fuga_ID) {
        Diametro_Fuga_ID = diametro_Fuga_ID;
    }

    public String getMaterial_Fuga_ID() {
        return Material_Fuga_ID;
    }

    public void setMaterial_Fuga_ID(String material_Fuga_ID) {
        Material_Fuga_ID = material_Fuga_ID;
    }

    public String getNombre_Usuario() {
        return Nombre_Usuario;
    }

    public void setNombre_Usuario(String nombre_Usuario) {
        Nombre_Usuario = nombre_Usuario;
    }

    public String getBrigadaID() {
        return BrigadaID;
    }

    public void setBrigadaID(String brigadaID) {
        BrigadaID = brigadaID;
    }

    public String getEmpleado_ID() {
        return Empleado_ID;
    }

    public void setEmpleado_ID(String empleado_ID) {
        Empleado_ID = empleado_ID;
    }

    public String getRPU() {
        return RPU;
    }

    public void setRPU(String RPU) {
        this.RPU = RPU;
    }
}
