package com.app.simapag.fugas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.simapag.fugas.R;
import com.app.simapag.fugas.modelos.Cls_Combo;


import java.util.ArrayList;
import java.util.List;

public class AutoCompleteCombos extends ArrayAdapter<Cls_Combo> {

    private  List<Cls_Combo> comboListFull;

    public AutoCompleteCombos(@NonNull Context context, @NonNull List<Cls_Combo> comboList) {
        super(context, 0, comboList);
        comboListFull = new ArrayList<>(comboList);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return comboFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.material_autocomplete_row ,parent, false);
        }

        TextView textViewNombreMaterial = convertView.findViewById(R.id.text_view_material_nombre);
        Cls_Combo cls_combo = getItem(position);

        if(cls_combo != null){
             textViewNombreMaterial.setText(cls_combo.getName());
        }

        return  convertView;
    }


        private Filter comboFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<Cls_Combo> suggestions = new ArrayList<>();

            if( constraint == null || constraint.length() == 0){
                suggestions.addAll(comboListFull);
            } else {
                String  filterPattern = constraint.toString().toLowerCase().trim();

                for(Cls_Combo item: comboListFull){

                    if(item.getName().toLowerCase().contains(filterPattern)){
                        suggestions.add(item);
                    }
                }
            }

            results.values = suggestions;
            results.count = suggestions.size();

            return  results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults results) {
            clear();
            addAll((List) results.values);
            notifyDataSetChanged();
        }
        @Override
        public CharSequence convertResultToString(Object resultValue) {

            return ((Cls_Combo) resultValue).getName();
        }
    };
}
