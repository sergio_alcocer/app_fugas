package com.app.simapag.fugas.fragmentos;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadListadoOrdenes;
import com.app.simapag.fugas.actividades.ActividadListadoVisitas;
import com.app.simapag.fugas.actividades.ActividadMenuOrdenes;
import com.app.simapag.fugas.actividades.ActvidadListadoMaterial;
import com.app.simapag.fugas.adapter.AutoCompleteCombos;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.modelos.Cls_Combo;
import com.app.simapag.fugas.modelos.MensajeModel;
import com.app.simapag.fugas.modelos.OrdenOTUpdate;
import com.app.simapag.fugas.modelos.OrdenTrabajoViewModel;
import com.app.simapag.fugas.modelos.ParametroTrabajoRealizado;
import com.app.simapag.fugas.preferencias.Preferencias;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.singlenton.RepositorioOT;
import com.app.simapag.fugas.util.DatePickerWrapper;
import com.app.simapag.fugas.util.TimePickerWrapper;
import com.app.simapag.fugas.util.Util;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FragmentoTrabajoRealizado extends Fragment {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;

    @BindView(R.id.spinner_trabajo_realizado_area)
    Spinner SpinnerArea;
    ArrayAdapter arrayAdapterArea;
    List<Cls_Combo> ListaAreas = new ArrayList<>();
    Cls_Combo itemSelectedArea;

    @BindView(R.id.spinner_trabajo_realizado_diametro)
    Spinner SpinnerDiametro;
    ArrayAdapter arrayAdapterDiametro;
    List<Cls_Combo> ListaDiametro = new ArrayList<>();
    Cls_Combo itemSelectedDiametro;

    @BindView(R.id.spinner_trabajo_realizado_material)
    Spinner SpinnerMaterial;
    ArrayAdapter arrayAdapterMaterial;
    List<Cls_Combo> ListaMaterial = new ArrayList<>();
    Cls_Combo itemSelectedMaterial;


    @BindView(R.id.autoComplete_trabajo_realizado_actividad)
    AutoCompleteTextView autoCompleteTextActividad;
    List<Cls_Combo> ListaActividad = new ArrayList<>();
    Cls_Combo itemSelectedActividad;

    @BindView(R.id.boton_borrar_autocompletado_actividad)
    Button buttonBorrarAutompleadoActividad;

    @BindView(R.id.autoComplete_trabajo_realizado_empleado)
    AutoCompleteTextView autoCompleteTextEmpleado;
    List<Cls_Combo> ListaEmpleadoRealizo = new ArrayList<>();
    Cls_Combo itemSelectedEmpleadoRealizo;

    @BindView(R.id.boton_borrar_autocompletado_empleado_realizo)
    Button buttonBorrarAutompleadoEmpleado;


    @BindView(R.id.text_input_layout_trabajo_realizado_comentario)
    TextInputLayout textInputLayoutComentario;

    @BindView(R.id.text_input_edit_trabajo_realizado_comentario)
    TextInputEditText textInputEditTextComentario;

    @BindView(R.id.text_input_layout_trabajo_realizado_anomalia)
    TextInputLayout textInputLayoutAnomalia;

    @BindView(R.id.text_input_edit_trabajo_realizado_anomalia)
    TextInputEditText textInputEditTextAnomalia;


    @BindView(R.id.radio_si_trabajo_realizado)
    RadioButton radioButtonTrabajoRealizadoSi;

    @BindView(R.id.radio_no_trabajo_realizado)
    RadioButton radioButtonTrabajoRealizadoNo;


    DatePickerWrapper datePickerWrapperFechaTrabajo;

    DatePickerWrapper datePickerWrapperFechaInicio;
    DatePickerWrapper datePickerWrapperFechaFin;
    TimePickerWrapper timePickerWrapperHoraInicio;
    TimePickerWrapper timePickerWrapperHoraFin;


    OrdenTrabajoViewModel ordenTrabajoViewModel;
    DelayedProgressDialog delayedProgressDialog;
    private Unbinder unbinder;
    private String Empleado_ID;

    SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm");
    SimpleDateFormat formatFechaHora = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    SimpleDateFormat formatFecha = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat formaFechaMexicana = new SimpleDateFormat("dd/MM/yyyy");


    Date datFechaInicio;
    Date datFechaFin;
    Date datFechaTrabajo;

    int ContadorBusquedaArea = 0;
    OrdenOTUpdate ObjOrdenOTUpdate = new OrdenOTUpdate();

    public static FragmentoTrabajoRealizado nuevaInstancia() {
        return new FragmentoTrabajoRealizado();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);
        setHasOptionsMenu(true);
        ordenTrabajoViewModel = RepositorioOT.get().getObjTrabajViewModel();
        Empleado_ID = Preferencias.getEmpleadoID(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_trabajo_realizado, container, false);
        unbinder = ButterKnife.bind(this, view);
        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");

        textViewTitulo.setText("Folio: " + ordenTrabajoViewModel.getFolio());

        datePickerWrapperFechaTrabajo = new DatePickerWrapper((TextView) view.findViewById(R.id.text_view_trabajo_realizado_fecha),getActivity());
        timePickerWrapperHoraInicio = new TimePickerWrapper((TextView) view.findViewById(R.id.text_view_trabajo_realizado_hora_inicio), getActivity());
        timePickerWrapperHoraFin = new TimePickerWrapper((TextView) view.findViewById(R.id.text_view_trabajo_realizado_hora_fin),getActivity());

        datePickerWrapperFechaFin = new DatePickerWrapper((TextView) view.findViewById(R.id.text_view_trabajo_realizado_fecha_final),getActivity());
        datePickerWrapperFechaInicio = new DatePickerWrapper((TextView) view.findViewById(R.id.text_view_trabajo_realizado_fecha_inicio),getActivity());

        view.findViewById(R.id.linear_layout_principal).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                View focusedView = getActivity().getCurrentFocus();
                /*
                 * If no view is focused, an NPE will be thrown
                 *
                 * Maxim Dmitriev
                 */
                if (focusedView != null) {
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }


                return true;
            }
        });

      /*
        SpinnerArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

               if(ContadorBusquedaArea > 0)
                if(!ListaAreas.get(i).getID().equals("-1")){
                    // buscar los empleados de la áera seleccionada
                     autoCompleteTextEmpleado.setText("");
                     buttonBorrarAutompleadoEmpleado.setVisibility(View.GONE);
                     itemSelectedEmpleadoRealizo = null;
                     obtenerEmpleadosBrigada(ListaAreas.get(i).getID());
                }else {
                    autoCompleteTextEmpleado.setText("");
                    buttonBorrarAutompleadoEmpleado.setVisibility(View.GONE);
                    itemSelectedEmpleadoRealizo = null;
                }else {
                    ContadorBusquedaArea++;
               }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

       */

        textInputEditTextComentario.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        textInputEditTextComentario.setRawInputType(InputType.TYPE_CLASS_TEXT);

        textInputEditTextAnomalia.setImeOptions(EditorInfo.IME_ACTION_DONE);
        textInputEditTextAnomalia.setRawInputType(InputType.TYPE_CLASS_TEXT);

        buttonBorrarAutompleadoEmpleado.setVisibility(View.GONE);
        autoCompleteTextEmpleado.setImeOptions(EditorInfo.IME_ACTION_DONE);
        autoCompleteTextEmpleado.setRawInputType(InputType.TYPE_CLASS_TEXT);

        autoCompleteTextEmpleado.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                if(s.length() != 0) {
                     buttonBorrarAutompleadoEmpleado.setVisibility(View.VISIBLE);
                }else {
                    buttonBorrarAutompleadoEmpleado.setVisibility(View.GONE);
                    itemSelectedEmpleadoRealizo = null;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        autoCompleteTextEmpleado.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                itemSelectedEmpleadoRealizo = (Cls_Combo) adapterView.getItemAtPosition(i);
                buttonBorrarAutompleadoEmpleado.setVisibility(View.VISIBLE);
            }
        });

        buttonBorrarAutompleadoActividad.setVisibility(View.GONE);
        autoCompleteTextActividad.setImeOptions(EditorInfo.IME_ACTION_DONE);
        autoCompleteTextActividad.setRawInputType(InputType.TYPE_CLASS_TEXT);

        autoCompleteTextActividad.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                if(s.length() != 0) {
                    buttonBorrarAutompleadoActividad.setVisibility(View.VISIBLE);
                }else {
                    buttonBorrarAutompleadoActividad.setVisibility(View.GONE);
                    itemSelectedActividad = null;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        autoCompleteTextActividad.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                itemSelectedActividad = (Cls_Combo) adapterView.getItemAtPosition(i);
                buttonBorrarAutompleadoActividad.setVisibility(View.VISIBLE);
            }
        });


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        obtenerParametrosTrabajoRealizado(Empleado_ID, ordenTrabajoViewModel.getNo_Orden_Trabajo());
    }

    // métodos

    public  void  configurarAdpterCombos(){

        if(isAdded()){
             arrayAdapterArea  = new ArrayAdapter(getContext(),R.layout.item_spinner,ListaAreas);
             SpinnerArea.setAdapter(arrayAdapterArea);

             arrayAdapterDiametro = new ArrayAdapter(getContext(), R.layout.item_spinner, ListaDiametro);
             SpinnerDiametro.setAdapter(arrayAdapterDiametro);

             arrayAdapterMaterial = new ArrayAdapter(getContext(), R.layout.item_spinner, ListaMaterial);
             SpinnerMaterial.setAdapter(arrayAdapterMaterial);

             autoCompleteTextActividad.setAdapter(new AutoCompleteCombos(getContext(), ListaActividad));
             autoCompleteTextEmpleado.setAdapter(new AutoCompleteCombos(getContext(), ListaEmpleadoRealizo));

             seleccionarElementos(ordenTrabajoViewModel.getBrigada_ID(), ordenTrabajoViewModel.getTipo_Falla_ID(), ordenTrabajoViewModel.getMaterial_Fuga_ID(), ordenTrabajoViewModel.getDiametro_Fuga_ID());
             bloquearControles();
        }
    }

    public  void  bloquearControles(){

        if(ordenTrabajoViewModel.getEstatus().equals("TERMINADA")){

            radioButtonTrabajoRealizadoSi.setChecked(true);
            radioButtonTrabajoRealizadoSi.setEnabled(false);
            radioButtonTrabajoRealizadoNo.setEnabled(false);

            datePickerWrapperFechaFin.bloquearCajas();
            datePickerWrapperFechaInicio.bloquearCajas();
            datePickerWrapperFechaTrabajo.bloquearCajas();
            timePickerWrapperHoraFin.bloquearCajas();
            timePickerWrapperHoraInicio.bloquearCajas();

            SpinnerDiametro.setEnabled(false);
            SpinnerMaterial.setEnabled(false);
            SpinnerArea.setEnabled(false);
            autoCompleteTextEmpleado.setEnabled(false);
            autoCompleteTextActividad.setEnabled(false);
            buttonBorrarAutompleadoEmpleado.setEnabled(false);
            buttonBorrarAutompleadoActividad.setEnabled(false);

            textInputEditTextComentario.setEnabled(false);
            textInputEditTextAnomalia.setEnabled(false);

        }
    }

    public void  seleccionarElementos(String pBrigdaID, String pFalla_ID, int pMaterialFuga_ID, int pDiametroFuga_ID){
        int index;
        Date datFechaAux;

        index = Util.obtenerIndex(pBrigdaID, ListaAreas);

        if(index != -1){
            itemSelectedArea = ListaAreas.get(index);
            SpinnerArea.setSelection(index);
        }

        index = Util.obtenerIndex(pFalla_ID, ListaActividad);

        if(index != -1){
            itemSelectedActividad = ListaActividad.get(index);
            autoCompleteTextActividad.setText(itemSelectedActividad.getName());
        }


       if(pMaterialFuga_ID > 0){
           index = Util.obtenerIndex( String.valueOf(pMaterialFuga_ID), ListaMaterial);
           if(index != -1){
               itemSelectedMaterial = ListaMaterial.get(index);
               SpinnerMaterial.setSelection(index);
           }
       }

       if(pDiametroFuga_ID >0){
           index = Util.obtenerIndex(String.valueOf(pDiametroFuga_ID), ListaDiametro);
           if(index != -1){
               itemSelectedDiametro = ListaDiametro.get(index);
               SpinnerDiametro.setSelection(index);
           }
       }

        textInputEditTextComentario.setText(ordenTrabajoViewModel.getObservaciones_Trabajo_Realizado().trim());

        if(ordenTrabajoViewModel.getFecha_Trabajo().length()>0){
            try {

                datFechaAux = formaFechaMexicana.parse(ordenTrabajoViewModel.getFecha_Trabajo());
                datePickerWrapperFechaTrabajo.setDate(datFechaAux);

            }catch (Exception ex){

            }
        }


        if(ordenTrabajoViewModel.getFecha_Inicio().length()>0){
            try {

                datFechaAux = formaFechaMexicana.parse(ordenTrabajoViewModel.getFecha_Inicio());
                datePickerWrapperFechaInicio.setDate(datFechaAux);

            }catch (Exception ex){

            }
        }

        if(ordenTrabajoViewModel.getFecha_Termino().length() >0){
            try {
                  datFechaAux = formaFechaMexicana.parse(ordenTrabajoViewModel.getFecha_Termino());
                  datePickerWrapperFechaFin.setDate(datFechaAux);
            }catch (Exception ex){

            }

        }

    }


    public  boolean  validarInterfaz(){
      int horaIntervalo;
      int minutoIntervalo;

        ObjOrdenOTUpdate.setRPU(ordenTrabajoViewModel.getRPU().trim());
        ObjOrdenOTUpdate.setNo_Orden_Trabajo(ordenTrabajoViewModel.getNo_Orden_Trabajo());
        ObjOrdenOTUpdate.setNombre_Usuario(Preferencias.getNombreUsuario(getContext()));
        ObjOrdenOTUpdate.setComentarios(textInputEditTextComentario.getText().toString().trim());
        ObjOrdenOTUpdate.setAnomalias(textInputEditTextAnomalia.getText().toString().trim());
        ObjOrdenOTUpdate.setEmpleado_Realizo_ID("-1");
        ObjOrdenOTUpdate.setEmpleado_ID(Preferencias.getEmpleadoID(getContext()));

        horaIntervalo = Util.obtenerHora(timePickerWrapperHoraInicio.getTime());
        minutoIntervalo = Util.obtenerMinuto(timePickerWrapperHoraInicio.getTime());
        datFechaInicio = Util.configurarHorasMinutosAFecha(datePickerWrapperFechaInicio.getDate(), horaIntervalo,minutoIntervalo);
        ObjOrdenOTUpdate.setFecha_Inicio_Hora(formatFechaHora.format(datFechaInicio));

        horaIntervalo = Util.obtenerHora(timePickerWrapperHoraFin.getTime());
        minutoIntervalo = Util.obtenerMinuto(timePickerWrapperHoraFin.getTime());
        datFechaFin = Util.configurarHorasMinutosAFecha( datePickerWrapperFechaFin.getDate(), horaIntervalo, minutoIntervalo);
        ObjOrdenOTUpdate.setFecha_Fin_Hora(formatFechaHora.format(datFechaFin));

        datFechaTrabajo = datePickerWrapperFechaTrabajo.getDate();
        ObjOrdenOTUpdate.setFecha_Trabajo( formatFecha.format(datePickerWrapperFechaTrabajo.getDate()));

        if(radioButtonTrabajoRealizadoSi.isChecked())
            ObjOrdenOTUpdate.setTrabajo_Realizado("SI");
        else
            ObjOrdenOTUpdate.setTrabajo_Realizado("NO");

        itemSelectedArea = (Cls_Combo) SpinnerArea.getSelectedItem();

        if(itemSelectedArea.getID().equals("-1")){
            Util.mostarAlerta2(getActivity(), "Seleciona una Área");
            return  false;
        }

        ObjOrdenOTUpdate.setBrigadaID(itemSelectedArea.getID());

        if(itemSelectedActividad == null){
             Util.mostarAlerta2(getActivity(), "Seleciona una Actividad");
            return  false;
        }

        if( ObjOrdenOTUpdate.getTrabajo_Realizado().equals("SI")){

            if(itemSelectedEmpleadoRealizo == null){
                Util.mostarAlerta2(getActivity(), "Selecciona un empleado");
                return  false;
            }else {
                 ObjOrdenOTUpdate.setEmpleado_Realizo_ID(itemSelectedEmpleadoRealizo.getID());
            }


            int comparacionFechaEjecucionInicio = datFechaTrabajo.compareTo(datePickerWrapperFechaInicio.getDate());

            if(comparacionFechaEjecucionInicio == 1){
                Util.mostarAlerta2(getActivity(), "La fecha de inicio no puede ser menor a la fecha de ejecución");
                return false;
            }

            int comparacionFechaEjecucionFin = datFechaTrabajo.compareTo(datePickerWrapperFechaFin.getDate());

            if (comparacionFechaEjecucionFin == 1){
                Util.mostarAlerta2(getActivity(), "La fecha final no puede ser menor a la fecha de ejecución");
                return false;
            }

           int Comparacion = datFechaInicio.compareTo(datFechaFin);

            if(Comparacion == 1){
                Util.mostarAlerta2(getActivity(), "La fecha final no puede ser menor a la fecha inicio");
                return false;
            }


        }else {
            if(itemSelectedEmpleadoRealizo != null)
                ObjOrdenOTUpdate.setEmpleado_Realizo_ID(itemSelectedEmpleadoRealizo.getID());
        }

        if(ObjOrdenOTUpdate.getComentarios().length() < 2){
            Util.mostarAlerta2(getActivity(), "Inidique un comentario");
            return  false;
        }

        if(Util.tieneCarateresPermitidos(ObjOrdenOTUpdate.getComentarios()) == false){
            textInputLayoutComentario.setError("Contiene caracteres no permitidos");
            Util.mostarAlerta2(getActivity(),"Permitidos letras,números y estos caracteres: " + Util.CARACTERES_PERMITIDOS);

            return  false;
        }

        textInputLayoutComentario.setError(null);

       if(ObjOrdenOTUpdate.getAnomalias().length() > 0){
           if(Util.tieneCarateresPermitidos(ObjOrdenOTUpdate.getAnomalias()) == false){
               textInputLayoutAnomalia.setError("Contiene caracteres no permitidos");
               Util.mostarAlerta2(getActivity(),"Permitidos letras,números y estos caracteres: " + Util.CARACTERES_PERMITIDOS);

               return  false;
           }

       }
        textInputLayoutAnomalia.setError(null);

        ObjOrdenOTUpdate.setFalla_ID(itemSelectedActividad.getID());

        itemSelectedDiametro = (Cls_Combo) SpinnerDiametro.getSelectedItem();
        ObjOrdenOTUpdate.setDiametro_Fuga_ID(itemSelectedDiametro.getID());

        itemSelectedMaterial = (Cls_Combo) SpinnerMaterial.getSelectedItem();
        ObjOrdenOTUpdate.setMaterial_Fuga_ID(itemSelectedMaterial.getID());


        return  true;
    }

    public  void  mostrarAlerta(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Proceso Correcto");
        builder.setTitle("Trabajao Realizado");
        builder.setCancelable(false);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RepositorioOT.get().clearInstancia();
                Intent intent = ActividadListadoOrdenes.nuevaInstancia(getContext());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public  void  guardarOT(){

        if(ordenTrabajoViewModel.getEstatus().equals("TERMINADA")){
            Util.mostarAlerta2(getActivity(), "La orden no se puede modificar esta terminada");
            return;
        }

        if(validarInterfaz() == false){
            return;
        }
        Gson gson = new Gson();
        String json = gson.toJson(ObjOrdenOTUpdate);
        Log.d("DEBUG", json);

        modificarOrden(ObjOrdenOTUpdate);
    }

    // peticiones

    public  void modificarOrden(OrdenOTUpdate ordenOTUpdate){
        delayedProgressDialog.show(getParentFragmentManager(), "paginado");
        FactoryPeticiones.getFactoryPeticiones()
                .modificarOrdenTrabajo(ordenOTUpdate)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<MensajeModel>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull MensajeModel mensajeModel) {

                        if(mensajeModel.getEstatus().equals("bien")){
                              mostrarAlerta();
                        }else{
                            Util.mostarAlerta2(getActivity(), mensajeModel.getEstatus());
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });
    }

    /**
     * Esta consulta obtiene los trabajadores que pertencencen a la brigada seleccionada
     * @param pBrigadaID
     */
    public  void  obtenerEmpleadosBrigada(String pBrigadaID){

        delayedProgressDialog.show(getParentFragmentManager(), "paginado");
        FactoryPeticiones.getFactoryPeticiones()
                .ObtenerEmpleadosBrigada(pBrigadaID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Cls_Combo>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<Cls_Combo> cls_combos) {
                        ListaEmpleadoRealizo = cls_combos;
                        autoCompleteTextEmpleado.setAdapter(new AutoCompleteCombos(getContext(), ListaEmpleadoRealizo));
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });


    }

    /**
     * Obtiene los parametros para llenar los combos de la pantalla
     * @param pEmpleadoID
     * @param pNoOrdenTrabajo
     */
    public  void obtenerParametrosTrabajoRealizado(String pEmpleadoID, String pNoOrdenTrabajo){

        delayedProgressDialog.show(getParentFragmentManager(), "paginado");
        FactoryPeticiones.getFactoryPeticiones()
                .obtenerParametrosTrabajoRealizado(pEmpleadoID, pNoOrdenTrabajo)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ParametroTrabajoRealizado>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull ParametroTrabajoRealizado parametroTrabajoRealizado) {

                       // ListaAreas.add(Util.obtenerElementoInicial());
                      //  ListaAreas.addAll(parametroTrabajoRealizado.getListaAreas());
                        ListaAreas = parametroTrabajoRealizado.getListaAreas();

                        ListaDiametro.add(Util.obtenerElementoInicial());
                        ListaDiametro.addAll(parametroTrabajoRealizado.getListaDiametroFuga());

                        ListaMaterial.add(Util.obtenerElementoInicial());
                        ListaMaterial.addAll(parametroTrabajoRealizado.getListaMaterialesFuga());

                        ListaActividad = parametroTrabajoRealizado.getListaFallas();
                        ListaEmpleadoRealizo = parametroTrabajoRealizado.getListaEmpleadosDistritos();

                        configurarAdpterCombos();


                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });
    }

    // eventos de botones

    @OnClick(R.id.boton_borrar_autocompletado_actividad)
    public  void  onBorrarActividad(){
        autoCompleteTextActividad.setText("");
        buttonBorrarAutompleadoActividad.setVisibility(View.GONE);
        itemSelectedActividad = null;
    }

    @OnClick(R.id.boton_borrar_autocompletado_empleado_realizo)
    public  void  onBorrarEmpleadoRealizo(){

        autoCompleteTextEmpleado.setText("");
        buttonBorrarAutompleadoEmpleado.setVisibility(View.GONE);
        itemSelectedEmpleadoRealizo = null;
    }

    // sobre escribír métodos

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_guardar, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.menu_item_aceptar:
                guardarOT();
                return  true;
            case android.R.id.home:
                Intent intent = ActividadMenuOrdenes.nuevaInstancia(getContext());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
