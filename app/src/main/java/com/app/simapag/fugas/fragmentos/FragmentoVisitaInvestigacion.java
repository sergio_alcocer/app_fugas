package com.app.simapag.fugas.fragmentos;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadListadoVisitas;
import com.app.simapag.fugas.actividades.ActividadListadoVisitasInvestigacion;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.modelos.Cls_Combo;
import com.app.simapag.fugas.modelos.OrdenesInvestigacionViewModel;
import com.app.simapag.fugas.modelos.ParametrosVisitasInvestigacion;
import com.app.simapag.fugas.parciable.VisitaInvestigacionParciable;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.singlenton.RepositorioVisita;
import com.app.simapag.fugas.util.DatePickerWrapper;
import com.app.simapag.fugas.util.TimePickerWrapper;
import com.app.simapag.fugas.util.Util;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FragmentoVisitaInvestigacion extends Fragment {
    private static  final  String ARG_OBJECTO_VISITA_INVESTIGACION="objecto_visita_investigacion";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;

    @BindView(R.id.text_view_titulo_pantalla)
    TextView textViewTituloPantalla;

    @BindView(R.id.spinner_visita_investigacion_toma)
    Spinner SpinnerToma;
    ArrayAdapter arrayAdapterToma;
    List<Cls_Combo> ListaToma = new ArrayList<>();
    Cls_Combo itemSelectedToma;

    @BindView(R.id.spinner_visita_investigacion_descarga)
    Spinner SpinnerDescarga;
    ArrayAdapter arrayAdapterDescarga;
    List<Cls_Combo> ListaDescarga = new ArrayList<>();
    Cls_Combo itemSelectedDescarga;


    @BindView(R.id.spinner_visita_investigacion_aprobada)
    Spinner SpinnerAprobada;
    ArrayAdapter arrayAdapterAprobada;
    List<Cls_Combo> ListaAprobada = new ArrayList<>();
    Cls_Combo itemSelectedAprobada;

    @BindView(R.id.spinner_visita_investigacion_ampliacion)
    Spinner SpinnerAmpliacion;
    ArrayAdapter arrayAdapterAmpliacion;
    List<Cls_Combo> ListaAmpliacion = new ArrayList<>();
    Cls_Combo itemSelectedAmpliacion;


    @BindView(R.id.spinner_visita_investigacion_atencion_usuario)
    Spinner SpinnerAtencionUsuario;
    ArrayAdapter arrayAdapterAtencionUsuario;
    List<Cls_Combo> ListaAtencionUsuario = new ArrayList<>();
    Cls_Combo itemSelectedAtencionUsuario;


    @BindView(R.id.text_input_layout_text_visita_investigacion_observacion)
    TextInputLayout textInputLayoutObservacion;

    @BindView(R.id.text_input_edit_visita_investigacion_observacion)
    TextInputEditText textInputEditTextObservacion;

    DatePickerWrapper datePickerWrapperFecha;
    TimePickerWrapper timePickerWrapperHora;

    @BindView(R.id.button_visita_investigacion_llenar_observacion)
    Button buttonLlenarObservacion;

    VisitaInvestigacionParciable visitaInvestigacionParciable;
    OrdenesInvestigacionViewModel ordenesInvestigacionViewModel;
    DelayedProgressDialog delayedProgressDialog;
    private Unbinder unbinder;

    public static FragmentoVisitaInvestigacion nuevaInstancia(VisitaInvestigacionParciable visitaInvestigacionParciable){
        Bundle arg = new Bundle();
        arg.putParcelable(ARG_OBJECTO_VISITA_INVESTIGACION, visitaInvestigacionParciable);
        FragmentoVisitaInvestigacion fragmentoVisitaInvestigacion = new FragmentoVisitaInvestigacion();
        fragmentoVisitaInvestigacion.setArguments(arg);
        return  fragmentoVisitaInvestigacion;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);
        setHasOptionsMenu(true);
        visitaInvestigacionParciable = (VisitaInvestigacionParciable) getArguments().getParcelable(ARG_OBJECTO_VISITA_INVESTIGACION);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_visita_investigacion, container, false);
        unbinder = ButterKnife.bind(this, view);
        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");

        textViewTitulo.setText("RPU: " + RepositorioVisita.get().getOrdenesInvestigacionViewModel().getRpu());

        if(visitaInvestigacionParciable.getNo_Visita() == 0)
          textViewTituloPantalla.setText("Nueva Visita");
        else
            textViewTituloPantalla.setText("Editar Visita");

        datePickerWrapperFecha = new DatePickerWrapper((TextView) view.findViewById(R.id.text_view_visita_investigacion_fecha_visita),getActivity());
        timePickerWrapperHora = new TimePickerWrapper((TextView) view.findViewById(R.id.text_view_visita_investigacion_hora_visita), getActivity());

        textInputEditTextObservacion.setImeOptions(EditorInfo.IME_ACTION_DONE);
        textInputEditTextObservacion.setRawInputType(InputType.TYPE_CLASS_TEXT);

        view.findViewById(R.id.linear_layout_principal).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                View focusedView = getActivity().getCurrentFocus();
                /*
                 * If no view is focused, an NPE will be thrown
                 *
                 * Maxim Dmitriev
                 */
                if (focusedView != null) {
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                }


                return true;
            }
        });

        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        obtenerParametros(visitaInvestigacionParciable.getNo_Solicitud());
    }

    // métodos

    public void llenarCombos(){

        if(isAdded()){
            arrayAdapterToma = new ArrayAdapter(getContext(),R.layout.item_spinner,ListaToma);
            SpinnerToma.setAdapter(arrayAdapterToma);

            arrayAdapterDescarga = new ArrayAdapter(getContext(),R.layout.item_spinner, ListaDescarga);
            SpinnerDescarga.setAdapter(arrayAdapterDescarga);

            arrayAdapterAmpliacion = new ArrayAdapter(getContext(), R.layout.item_spinner, ListaAmpliacion);
            SpinnerAmpliacion.setAdapter(arrayAdapterAmpliacion);

            arrayAdapterAprobada = new ArrayAdapter(getContext(), R.layout.item_spinner, ListaAprobada);
            SpinnerAprobada.setAdapter(arrayAdapterAprobada);

            arrayAdapterAtencionUsuario = new ArrayAdapter(getContext(), R.layout.item_spinner, ListaAtencionUsuario);
            SpinnerAtencionUsuario.setAdapter(arrayAdapterAtencionUsuario);
        }

    }

    // peticiones
    public  void obtenerParametros(String No_Solicitud){

        delayedProgressDialog.show(getParentFragmentManager(), "paginado");
        FactoryPeticiones.getFactoryPeticiones()
                .ObtenerParametrosVisitasInvestigacion(No_Solicitud)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ParametrosVisitasInvestigacion>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull ParametrosVisitasInvestigacion parametrosVisitasInvestigacion) {

                        ordenesInvestigacionViewModel = parametrosVisitasInvestigacion.getOrdenesInvestigacionViewModel();
                        ListaToma.add(Util.obtenerElementoInicial());
                        ListaToma.addAll(parametrosVisitasInvestigacion.getListaTomas());
                        ListaDescarga.add(Util.obtenerElementoInicial());
                        ListaDescarga.addAll(parametrosVisitasInvestigacion.getListaTipoDescarga());
                        ListaAprobada = Util.Obtener_Item_SI_NO();
                        ListaAmpliacion = Util.Obtener_Item_SI_NO();
                        ListaAtencionUsuario = Util.Obtener_Item_SI_NO();

                        llenarCombos();
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });
    }

    // sobre escribir métodos
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_guardar, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){

            case R.id.menu_item_aceptar:
                return true;
            case android.R.id.home:
                Intent intent = ActividadListadoVisitasInvestigacion.nuevaInstancia(getContext());
                startActivity(intent);
                return  true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
