package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentoInformacionOT;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActividadInformacionOT  extends SingleFragmentoActividad {

    @Override
    protected Fragment crearFragmento() {
        return FragmentoInformacionOT.nuevaInstancia();
    }

    public  static Intent nuevaInstancia(Context context){
        Intent intent = new Intent(context, ActividadInformacionOT.class);
        return  intent;
    }

    @Override
    public void onBackPressed() {
        return;
    }

}
