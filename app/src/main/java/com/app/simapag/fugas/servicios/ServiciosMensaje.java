package com.app.simapag.fugas.servicios;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.app.simapag.fugas.actividades.ActividadLogin;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.app.simapag.fugas.R;
import java.util.Random;

public class ServiciosMensaje extends FirebaseMessagingService {


    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {

        if(remoteMessage.getNotification() != null){
            visualizarNotificacion(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
        }
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }

    private  void visualizarNotificacion(String pTitulo, String pMensaje){

        Intent intent;

        intent = ActividadLogin.nuevaInstancia(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,PendingIntent.FLAG_ONE_SHOT);
        Uri notificacionSonido = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        String notificacionCanalID = "canal_fuga";
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (pTitulo == null){
            pTitulo = "";
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){

            NotificationChannel notificationChannel = new NotificationChannel(notificacionCanalID,"Notificación",NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription("Aplicación de Fuga SIMAPAG");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationChannel.setShowBadge(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, notificacionCanalID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis())
                .setTicker("Mensajes")
                .setContentTitle(pTitulo)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(pMensaje))
                .setContentText(pMensaje)
                .setAutoCancel(true)
                .setSound(notificacionSonido)
                .setContentIntent(pendingIntent);


        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;

        notificationManager.notify(m,builder.build());
    }

}
