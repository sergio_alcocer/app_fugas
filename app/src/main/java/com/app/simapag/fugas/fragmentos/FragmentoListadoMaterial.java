package com.app.simapag.fugas.fragmentos;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.simapag.fugas.R;
import com.app.simapag.fugas.actividades.ActividadMateriales;
import com.app.simapag.fugas.actividades.ActividadMenuOrdenes;
import com.app.simapag.fugas.dialogos.DelayedProgressDialog;
import com.app.simapag.fugas.dialogos.DialogoPregunta;
import com.app.simapag.fugas.modelos.MensajeModel;
import com.app.simapag.fugas.modelos.OrdenTrabajoViewModel;
import com.app.simapag.fugas.modelos.OrdenesMaterialViewModel;
import com.app.simapag.fugas.servicios.FactoryPeticiones;
import com.app.simapag.fugas.singlenton.RepositorioOT;
import com.app.simapag.fugas.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FragmentoListadoMaterial extends Fragment implements DialogoPregunta.RespuestaListener {


    private  static final int REQUEST_CODIGO = 1;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    TextView textViewTitulo;

    @BindView(R.id.text_view_titulo_pantalla)
    TextView textViewTituloPantalla;

    @BindView(R.id.recyclerview_material_orden)
    RecyclerView recyclerOrden;
    LinearLayoutManager linearLayoutManager;

    OrdenTrabajoViewModel ordenTrabajoViewModel;


    List<OrdenesMaterialViewModel> ListaOrdenesMaterial = new ArrayList<>();
    DelayedProgressDialog delayedProgressDialog;
    private Unbinder unbinder;

    public static  FragmentoListadoMaterial nuevaInstancia(){
        return  new FragmentoListadoMaterial();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        delayedProgressDialog = new DelayedProgressDialog();
        delayedProgressDialog.setCancelable(false);
        setHasOptionsMenu(true);
        ordenTrabajoViewModel = RepositorioOT.get().getObjTrabajViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_listado_materiales, container, false);
        unbinder = ButterKnife.bind(this, view);

        textViewTitulo = (TextView) toolbar.findViewById(R.id.toolbar_titulo);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerOrden.setLayoutManager(linearLayoutManager);

        textViewTitulo.setText("Folio: " + ordenTrabajoViewModel.getFolio());
        textViewTituloPantalla.setText("Listado Material");

        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        obtenerListadoMaterialOrden(ordenTrabajoViewModel.getNo_Orden_Trabajo());
    }

    // métodos

    public  void configurarAdapter(){

        if(isAdded()){
            recyclerOrden.setAdapter( new ListadoMaterialOrdenAdapter(ListaOrdenesMaterial));
        }
    }

    public  void  mostrarAlerta(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Proceso Correcto");
        builder.setTitle("Visitas");
        builder.setCancelable(false);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                obtenerListadoMaterialOrden(ordenTrabajoViewModel.getNo_Orden_Trabajo());

            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    // eventos

    // peticiones


    public  void eliminarMaterialEnOrden(String Ordenes_Trabajo_Detalles_Materiales_ID){
        delayedProgressDialog.show(getParentFragmentManager(), "paginado");
        FactoryPeticiones.getFactoryPeticiones()
                .eliminarMaterialOrden(Ordenes_Trabajo_Detalles_Materiales_ID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<MensajeModel>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull MensajeModel mensajeModel) {

                        if(mensajeModel.getEstatus().equals("bien")){
                            mostrarAlerta();
                        }else {
                            Util.mostarAlerta2(getActivity(), " " + mensajeModel.getEstatus());
                        }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });

    }

    public  void  obtenerListadoMaterialOrden(String pNoOrdenTrabajo){

        delayedProgressDialog.show(getParentFragmentManager(), "paginado");
        FactoryPeticiones.getFactoryPeticiones()
                .obtenerMaterialOrdenes(pNoOrdenTrabajo)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<OrdenesMaterialViewModel>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull List<OrdenesMaterialViewModel> ordenesMaterialViewModels) {
                          ListaOrdenesMaterial = ordenesMaterialViewModels;
                          configurarAdapter();
                          if(ordenesMaterialViewModels.size() == 0){
                              Util.mostarAlerta2(getActivity(), "No hay materiales Agregados");
                          }
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        delayedProgressDialog.cancel();
                        Util.mostarAlerta2(getActivity(), "Error de conectividad");
                    }

                    @Override
                    public void onComplete() {
                        delayedProgressDialog.cancel();
                    }
                });
    }

    // sobre escribír clases


    @Override
    public void aceptarRespuestaSI(String identificador) {
        eliminarMaterialEnOrden(identificador);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_mas, menu);
        MenuCompat.setGroupDividerEnabled(menu, true);
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.menu_item_agregar:

                Intent intentNuevoMaterial = ActividadMateriales.nuevaInstancia(getContext());
                  startActivity(intentNuevoMaterial);
                return  true;
            case android.R.id.home:
                Intent intent = ActividadMenuOrdenes.nuevaInstancia(getContext());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    // clases anidadas

    public  class  ListadoMaterialOrdenAdapter  extends RecyclerView.Adapter<ListadoMaterialOrdenHolder>{

        List<OrdenesMaterialViewModel> lista;

        public ListadoMaterialOrdenAdapter(List<OrdenesMaterialViewModel> lista) {
            this.lista = lista;
        }

        @NonNull
        @Override
        public ListadoMaterialOrdenHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_material_orden, parent, false);
            return new ListadoMaterialOrdenHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ListadoMaterialOrdenHolder holder, int position) {
             OrdenesMaterialViewModel viewModel = lista.get(position);
             holder.enlazarDatos(viewModel);

             holder.buttonEliminar.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     String mensaje = " " + viewModel.getNombre_Material();
                     DialogoPregunta dialogoPregunta = DialogoPregunta.nuevaInstancia(viewModel.getOrdenes_Trabajo_Detalles_Materiales_ID(),mensaje, "Material");
                     dialogoPregunta.setTargetFragment(FragmentoListadoMaterial.this,REQUEST_CODIGO);
                     dialogoPregunta.show(getParentFragmentManager(),"codigo");
                 }
             });
        }

        @Override
        public int getItemCount() {
            return lista.size();
        }
    }

    public class ListadoMaterialOrdenHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{


        OrdenesMaterialViewModel materialViewModel;

        @BindView(R.id.text_view_material_nombre)
        TextView textViewNombre;

        @BindView(R.id.text_view_material_cantidad)
        TextView textViewCantidad;

        @BindView(R.id.text_view_material_unidad)
        TextView textViewUnidad;

        @BindView(R.id.text_view_material_costo)
        TextView textViewCosto;

        @BindView(R.id.text_view_material_costo_total)
        TextView textViewCostoTotal;

        @BindView(R.id.button_material_eliminar)
        Button buttonEliminar;

        public ListadoMaterialOrdenHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
        }

        public void enlazarDatos(OrdenesMaterialViewModel model){
            materialViewModel = model;
            textViewNombre.setText(materialViewModel.getNombre_Material());
            textViewCantidad.setText("Cantidad: " + String.valueOf(materialViewModel.getCantidad()));
            textViewUnidad.setText("Unidad: " + materialViewModel.getUnidad());
            textViewCosto.setText("Costo Unidad: " + String.valueOf(materialViewModel.getCostoPorUnidad()));
            textViewCostoTotal.setText("Costo Total: " + String.valueOf(materialViewModel.getCostoTotal()));
        }


        @Override
        public void onClick(View view) {

        }
    }
}
