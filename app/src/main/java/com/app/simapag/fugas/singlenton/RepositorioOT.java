package com.app.simapag.fugas.singlenton;

import com.app.simapag.fugas.modelos.OrdenTrabajoViewModel;

public class RepositorioOT {

    private static RepositorioOT sRepoOT;
    private OrdenTrabajoViewModel ObjTrabajViewModel;


    public static RepositorioOT get(){

        if(sRepoOT == null){
              sRepoOT = new RepositorioOT();
        }
        return  sRepoOT;
    }

    public OrdenTrabajoViewModel getObjTrabajViewModel() {
        return ObjTrabajViewModel;
    }

    public void setObjTrabajViewModel(OrdenTrabajoViewModel objTrabajViewModel) {
        ObjTrabajViewModel = objTrabajViewModel;
    }

    public void clearInstancia(){ sRepoOT = null;}
}
