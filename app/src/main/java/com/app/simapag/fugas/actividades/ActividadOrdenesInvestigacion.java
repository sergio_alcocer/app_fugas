package com.app.simapag.fugas.actividades;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.app.simapag.fugas.fragmentos.FragmentoOrdenesInvestigacion;
import com.app.simapag.fugas.util.SingleFragmentoActividad;

public class ActividadOrdenesInvestigacion extends SingleFragmentoActividad {


    @Override
    protected Fragment crearFragmento() {
        return FragmentoOrdenesInvestigacion.nuevaInstancia();
    }

    public  static Intent nuevaInstancia(Context context){
        Intent intent = new Intent(context, ActividadOrdenesInvestigacion.class);
        return  intent;
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
