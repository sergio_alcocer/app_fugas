package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class VisitaInvestigacionModel {

    @SerializedName("No_Visita")
    private int No_Visita;
    @SerializedName("No_Solicitud")
    private  String No_Solicitud;
    @SerializedName("Fecha_Visita")
    private  String Fecha_Visita;
    @SerializedName("Hora_Visita")
    private  String Hora_Visita;
    @SerializedName("Observaciones")
    private  String Observaciones;
    @SerializedName("Usuario")
    private  String Usuario;

    public int getNo_Visita() {
        return No_Visita;
    }

    public void setNo_Visita(int no_Visita) {
        No_Visita = no_Visita;
    }

    public String getNo_Solicitud() {
        return No_Solicitud;
    }

    public void setNo_Solicitud(String no_Solicitud) {
        No_Solicitud = no_Solicitud;
    }

    public String getFecha_Visita() {
        return Fecha_Visita;
    }

    public void setFecha_Visita(String fecha_Visita) {
        Fecha_Visita = fecha_Visita;
    }

    public String getHora_Visita() {
        return Hora_Visita;
    }

    public void setHora_Visita(String hora_Visita) {
        Hora_Visita = hora_Visita;
    }

    public String getObservaciones() {
        return Observaciones;
    }

    public void setObservaciones(String observaciones) {
        Observaciones = observaciones;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String usuario) {
        Usuario = usuario;
    }
}
