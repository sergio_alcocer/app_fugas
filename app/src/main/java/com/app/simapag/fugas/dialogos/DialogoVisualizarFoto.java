package com.app.simapag.fugas.dialogos;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.app.simapag.fugas.R;
import com.github.chrisbanes.photoview.PhotoView;

public class DialogoVisualizarFoto extends DialogFragment {

    static  final  String ARG_ARRAYBITES = "arreglobitesfoto";
    static  final  String ARG_TITULO = "titulo";

    PhotoView photoViewFoto;
    String titulo;
    Bitmap bitmap;
    AlertDialog alertDialog;


    public  static  DialogoVisualizarFoto nuevaInstancia(byte[] byteArray, String titulo){
        Bundle arg = new Bundle();
        arg.putByteArray(ARG_ARRAYBITES, byteArray);
        arg.putSerializable(ARG_TITULO, titulo);
        DialogoVisualizarFoto dialogoVisualizarFoto = new DialogoVisualizarFoto();
        dialogoVisualizarFoto.setArguments(arg);
        return dialogoVisualizarFoto;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View vista = LayoutInflater.from(getActivity()).inflate(R.layout.fragmento_dialogo_foto, null);
        photoViewFoto = (PhotoView) vista.findViewById(R.id.photoview_imagen);
        byte[] bytesArray =  getArguments().getByteArray(ARG_ARRAYBITES);
        bitmap = BitmapFactory.decodeByteArray(bytesArray,0,bytesArray.length);
        titulo = (String) getArguments().getSerializable(ARG_TITULO);

        photoViewFoto.setImageBitmap(bitmap);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setView(vista)
                .setTitle(titulo)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                });
        alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);

        return  alertDialog;
    }
}
