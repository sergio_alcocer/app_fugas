package com.app.simapag.fugas.modelos;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Cls_Combo {

    @SerializedName("ID")
    private  String ID ;
    @SerializedName("Name")
    private  String Name;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @NonNull
    @Override
    public String toString() {
        return this.Name;
    }
}
