package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class MaterialesViewModel {

    @SerializedName("Producto_ID")
    private  String Producto_ID;
    @SerializedName("Unidad")
    private  String  Unidad;
    @SerializedName("Clave")
    private  String Clave;
    @SerializedName("Nombre")
    private  String  Nombre;
    @SerializedName("Costo")
    private  double Costo;

    public String getProducto_ID() {
        return Producto_ID;
    }

    public void setProducto_ID(String producto_ID) {
        Producto_ID = producto_ID;
    }

    public String getUnidad() {
        return Unidad;
    }

    public void setUnidad(String unidad) {
        Unidad = unidad;
    }

    public String getClave() {
        return Clave;
    }

    public void setClave(String clave) {
        Clave = clave;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public double getCosto() {
        return Costo;
    }

    public void setCosto(double costo) {
        Costo = costo;
    }
}
