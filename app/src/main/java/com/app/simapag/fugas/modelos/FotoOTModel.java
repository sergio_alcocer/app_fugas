package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

public class FotoOTModel {

    @SerializedName("Nombre_Foto")
    private String Nombre_Foto;
    @SerializedName("Cadena_Foto")
    private  String Cadena_Foto;
    @SerializedName("RPU")
    private  String RPU;
    @SerializedName("No_Orden_Trabajo")
    private  String No_Orden_Trabajo;
    @SerializedName("Nombre_Usuario")
    private  String Nombre_Usuario;
    @SerializedName("URL_Imagen")
    private String URL_Imagen;
    @SerializedName("Descripcion")
    private  String Descripcion;

    public String getNombre_Foto() {
        return Nombre_Foto;
    }

    public void setNombre_Foto(String nombre_Foto) {
        Nombre_Foto = nombre_Foto;
    }

    public String getCadena_Foto() {
        return Cadena_Foto;
    }

    public void setCadena_Foto(String cadena_Foto) {
        Cadena_Foto = cadena_Foto;
    }

    public String getRPU() {
        return RPU;
    }

    public void setRPU(String RPU) {
        this.RPU = RPU;
    }

    public String getNo_Orden_Trabajo() {
        return No_Orden_Trabajo;
    }

    public void setNo_Orden_Trabajo(String no_Orden_Trabajo) {
        No_Orden_Trabajo = no_Orden_Trabajo;
    }

    public String getNombre_Usuario() {
        return Nombre_Usuario;
    }

    public void setNombre_Usuario(String nombre_Usuario) {
        Nombre_Usuario = nombre_Usuario;
    }

    public String getURL_Imagen() {
        return URL_Imagen;
    }

    public void setURL_Imagen(String URL_Imagen) {
        this.URL_Imagen = URL_Imagen;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
