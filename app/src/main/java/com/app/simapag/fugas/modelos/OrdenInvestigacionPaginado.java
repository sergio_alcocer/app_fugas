package com.app.simapag.fugas.modelos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrdenInvestigacionPaginado {

    @SerializedName("TotalElementos")
    private int TotalElementos ;
    @SerializedName("ListaOrdenesInvestigacion")
    private List<OrdenesInvestigacionViewModel> ListaOrdenesInvestigacion;

    public int getTotalElementos() {
        return TotalElementos;
    }

    public void setTotalElementos(int totalElementos) {
        TotalElementos = totalElementos;
    }

    public List<OrdenesInvestigacionViewModel> getListaOrdenesInvestigacion() {
        return ListaOrdenesInvestigacion;
    }

    public void setListaOrdenesInvestigacion(List<OrdenesInvestigacionViewModel> listaOrdenesInvestigacion) {
        ListaOrdenesInvestigacion = listaOrdenesInvestigacion;
    }
}
